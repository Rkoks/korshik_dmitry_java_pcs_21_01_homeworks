package ru.rkoks.controllers.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.domains.exceptions.RegistrationFailedException;
import ru.rkoks.domains.forms.AuthForm;
import ru.rkoks.domains.forms.RegisterForm;
import ru.rkoks.domains.responses.LoginResponse;
import ru.rkoks.domains.responses.LogoutResponse;
import ru.rkoks.domains.responses.RegisterResponse;
import ru.rkoks.security.details.Account;
import ru.rkoks.services.AccountService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/auth")
public class AuthController {
    private final AccountService accountService;

    @PostMapping("login")
    public ResponseEntity login(@RequestBody @Valid AuthForm authForm) {
        try {
            LoginResponse response = accountService.loginAccount(authForm);

            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION,
                            "Bearer " + response.getToken())
                    .body(response);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

    //Зарегистрировать нового пользователя может только Админ
    @RolesAllowed(Account.Role.ADMIN)
    @PostMapping("register")
    public ResponseEntity register(@RequestBody @Valid RegisterForm form) {

        try {
            RegisterResponse response = accountService.registerAccount(form);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(response);
        } catch (RegistrationFailedException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("logout")
    public ResponseEntity logout(@RequestHeader Map<String, String> headers) {
        LogoutResponse response = accountService.logoutAccount(headers);
        return ResponseEntity
                .status(response.getStatus())
                .body(response.getMessage());

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException e) {
        Map<String, String> errors = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return errors;
    }
}
