package ru.rkoks.domains.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.security.details.Account;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResponse {
    private Long id;
    private String email;
    private String token;

    public static LoginResponse from(Account account) {
        return builder()
                .id(account.getId())
                .email(account.getEmail())
                .build();
    }
}
