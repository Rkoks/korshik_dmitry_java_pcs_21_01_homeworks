package ru.rkoks.domains.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.security.details.Account;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterResponse {
    private Long id;
    private String email;
    private String message;

    public static RegisterResponse from(Account account) {
        return builder()
                .id(account.getId())
                .email(account.getEmail())
                .message("Вам было отправлено письмо со ссылкой для подтверждения " +
                        "email адреса на " + account.getEmail())
                .build();
    }

}
