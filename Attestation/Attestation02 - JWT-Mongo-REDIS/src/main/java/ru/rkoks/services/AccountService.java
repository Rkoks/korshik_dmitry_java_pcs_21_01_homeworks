package ru.rkoks.services;

import org.springframework.security.core.AuthenticationException;
import ru.rkoks.domains.exceptions.RegistrationFailedException;
import ru.rkoks.domains.forms.AuthForm;
import ru.rkoks.domains.forms.RegisterForm;
import ru.rkoks.domains.responses.LoginResponse;
import ru.rkoks.domains.responses.LogoutResponse;
import ru.rkoks.domains.responses.RegisterResponse;

import java.util.Map;


public interface AccountService {
    LoginResponse loginAccount(AuthForm form) throws AuthenticationException;

    RegisterResponse registerAccount(RegisterForm form) throws RegistrationFailedException;

    LogoutResponse logoutAccount(Map<String, String> headers);
}
