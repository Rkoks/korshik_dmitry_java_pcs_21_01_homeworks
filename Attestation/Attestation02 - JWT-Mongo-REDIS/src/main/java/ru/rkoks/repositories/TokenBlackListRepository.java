package ru.rkoks.repositories;

public interface TokenBlackListRepository {
    void save(String token);

    boolean exists(String token);
}
