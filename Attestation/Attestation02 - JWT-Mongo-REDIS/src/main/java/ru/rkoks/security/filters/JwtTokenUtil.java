package ru.rkoks.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.rkoks.security.details.Account;

import java.util.Date;

@Slf4j
@Component
public class JwtTokenUtil {
    private final String jwtSecret = "MyVerySecretCode";
    private final String jwtIssuer = "rkoks.ru";

    public String generateAccessToken(Account account) {
        return JWT.create()
                .withSubject(account.getId().toString())
                .withClaim("email", account.getEmail())
                .withClaim("role", account.getRole())
                .withClaim("state", account.getState().name())
                .withIssuer(jwtIssuer)
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() +
                        7 * 24 * 60 * 60 * 1000))
                .sign(Algorithm.HMAC256(jwtSecret));
    }

    public UserDetails getUserDetails(String token) {
        DecodedJWT decodedJWT = JWT.decode(token);
        return Account.builder()
                .id(getUserId(decodedJWT))
                .email(getUsername(decodedJWT))
                .password(null)
                .role(getRole(decodedJWT))
                .state(Account.State.valueOf(getState(decodedJWT)))
                .build();

    }

    public Long getUserId(DecodedJWT token) {
        return Long.parseLong(token.getSubject());
    }

    public String getUsername(DecodedJWT token) {
        return token.getClaim("email").asString();
    }

    public String getState(DecodedJWT token) {
        return token.getClaim("state").asString();
    }

    public String getRole(DecodedJWT token) {
        return token.getClaim("role").asString();
    }

    public boolean validate(String token) {

        try {
            JWT.require(Algorithm.HMAC256(jwtSecret)).build().verify(token);
            return true;
        } catch (SignatureVerificationException e) {
            log.error("Invalid JWT signature - {}", e.getMessage());
        } catch (TokenExpiredException e) {
            log.error("Expired JWT token - {}", e.getMessage());
        } catch (InvalidClaimException e) {
            log.error("Invalid claims' value - {}", e.getMessage());
        } catch (JWTVerificationException e) {
            log.error("Invalid JWT token - {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("Invalid algorithm - {}", e.getMessage());
        }
        return false;
    }





}
