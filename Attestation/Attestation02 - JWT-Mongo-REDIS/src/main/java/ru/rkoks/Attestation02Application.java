package ru.rkoks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation02Application {

    public static void main(String[] args) {
        SpringApplication.run(Attestation02Application.class, args);
    }

}
