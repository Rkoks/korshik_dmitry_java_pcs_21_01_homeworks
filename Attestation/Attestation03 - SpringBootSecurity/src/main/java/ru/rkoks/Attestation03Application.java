package ru.rkoks;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SecurityScheme(name = "swaggerSecure", scheme = "bearer", in = SecuritySchemeIn.HEADER, type = SecuritySchemeType.HTTP)
public class Attestation03Application {

    public static void main(String[] args) {
        SpringApplication.run(Attestation03Application.class, args);
    }

}
