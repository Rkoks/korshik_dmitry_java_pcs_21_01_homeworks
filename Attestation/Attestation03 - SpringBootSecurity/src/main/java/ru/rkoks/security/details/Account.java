package ru.rkoks.security.details;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

/**
 * Модель аккаунта для работы Spring Security и хранения в БД
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Account implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;
    private String password;
    private String role;
    @Enumerated(value = EnumType.STRING)
    private State state;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !state.equals(State.DELETED);
    }

    @Override
    public boolean isAccountNonLocked() {
        return !state.equals(State.BANNED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return state.equals(State.CONFIRMED);
    }

    public static class Role {
        /**
         * Имеет права <code>CREATOR</code> и дополнительные на управление
         * пользователями и системой викторин, доступ ко всей возможной статистике
         */
        public static final String ADMIN = "ADMIN";
        /**
         * Имеет права на участие (игру) в существующих викторинах
         */
        public static final String USER = "USER";
    }

    public enum State {
        /**
         * Зарегистрирован, не имеет доступа к сайту,
         * нужно подтвердить Email
         */
        NOT_CONFIRMED,
        /**
         * Email подтверждён, имеет доступ к профилю и календарю мероприятий,
         * нужно заполнить профиль
         */
        CONFIRMED,
        /**
         * Удалился сам
         */
        DELETED,
        /**
         * Забанен Админом
         */
        BANNED
    }
}
