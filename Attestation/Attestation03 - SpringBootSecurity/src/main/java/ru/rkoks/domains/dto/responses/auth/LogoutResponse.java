package ru.rkoks.domains.dto.responses.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Модель ответа пользователю при его выходе из системы
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogoutResponse {
    private HttpStatus status;
    private String message;
}
