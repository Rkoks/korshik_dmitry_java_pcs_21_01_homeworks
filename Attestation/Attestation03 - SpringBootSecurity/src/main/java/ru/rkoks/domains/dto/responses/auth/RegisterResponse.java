package ru.rkoks.domains.dto.responses.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.security.details.Account;

/**
 * Модель ответа пользователю при его регистрации
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterResponse {
    private String email;
    private String message;

    public static RegisterResponse from(Account account) {
        return builder()
                .email(account.getEmail())
                .message("Успешная регистрация!")
                .build();
    }

}
