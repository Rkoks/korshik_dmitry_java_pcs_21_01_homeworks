package ru.rkoks.domains.exceptions;

/**
 * Исключение используемое при регистрации пользователя
 */
public class RegistrationFailedException extends RuntimeException {

    public RegistrationFailedException(String message) {
        super(message);
    }

}