package ru.rkoks.domains.dto.responses.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.security.details.Account;

/**
 * Модель ответа пользователю при его аутентификации
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResponse {
    private String email;
    private String token;

    public static LoginResponse from(Account account) {
        return builder()
                .email(account.getEmail())
                .build();
    }
}
