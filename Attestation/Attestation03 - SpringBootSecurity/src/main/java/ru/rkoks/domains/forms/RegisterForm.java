package ru.rkoks.domains.forms;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * Форма регистрации
 */
@Data
public class RegisterForm {
    @NotBlank
    @Pattern(regexp = "(\\w|\\.|-)+@(\\w+\\.)+\\w+", message = "eMail должен содержать @ и .")
    private String email;
    @NotBlank
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}")
    private String password;
}
