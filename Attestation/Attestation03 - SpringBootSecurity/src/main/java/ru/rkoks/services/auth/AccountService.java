package ru.rkoks.services.auth;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import ru.rkoks.domains.dto.responses.auth.LoginResponse;
import ru.rkoks.domains.dto.responses.auth.LogoutResponse;
import ru.rkoks.domains.dto.responses.auth.RegisterResponse;
import ru.rkoks.domains.exceptions.RegistrationFailedException;
import ru.rkoks.domains.forms.AuthForm;
import ru.rkoks.domains.forms.RegisterForm;

import java.util.Map;

/**
 * Интерфейс сервиса по работе с учётными записями
 */
public interface AccountService {
    /**
     * Реализует аутентификацию пользователя по вводимым данным
     *
     * @param form форма отправляемая пользователей с логином/email и паролем
     * @return ответ пользователю о результатах аутентификации
     * @throws AuthenticationException
     */
    LoginResponse loginAccount(AuthForm form) throws AuthenticationException;

    /**
     * Реализует регистрацию нового пользователя по вводимым данным
     *
     * @param form форма отправляемая пользователей с логином/email и паролем
     * @return ответ пользователю о результатах регистрации
     * @throws RegistrationFailedException
     */
    RegisterResponse registerAccount(RegisterForm form) throws RegistrationFailedException;

    /**
     * Реализует выход пользователя из системы (логаут)
     *
     * @param headers набор заголовков с JWT-токеном
     * @return ответ пользователю о результатах логаута
     */
    LogoutResponse logoutAccount(Map<String, String> headers);

    ResponseEntity getUsers();
}
