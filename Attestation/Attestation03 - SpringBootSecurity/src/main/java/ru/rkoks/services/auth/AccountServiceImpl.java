package ru.rkoks.services.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.dto.responses.auth.LoginResponse;
import ru.rkoks.domains.dto.responses.auth.LogoutResponse;
import ru.rkoks.domains.dto.responses.auth.RegisterResponse;
import ru.rkoks.domains.exceptions.RegistrationFailedException;
import ru.rkoks.domains.forms.AuthForm;
import ru.rkoks.domains.forms.RegisterForm;
import ru.rkoks.repositories.auth.AccountsRepository;
import ru.rkoks.repositories.auth.TokenBlacklistRepository;
import ru.rkoks.security.details.Account;
import ru.rkoks.security.filters.JwtTokenUtil;

import java.util.Map;

@RequiredArgsConstructor
@Service
public class AccountServiceImpl implements AccountService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final AccountsRepository accountsRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenBlacklistRepository blackListRepository;

    @Override
    public LoginResponse loginAccount(AuthForm form) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            form.getEmail(), form.getPassword()
                    ));

            Account account = (Account) authentication.getPrincipal();

            LoginResponse response = LoginResponse.from(account);
            response.setToken(jwtTokenUtil.generateAccessToken(account));
            return response;
        } catch (AuthenticationException e) {
            throw e;
        }
    }

    @Override
    public RegisterResponse registerAccount(RegisterForm form) throws RegistrationFailedException {
        Account account = Account.builder()
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.CONFIRMED)
                .build();

        if (accountsRepository.existsByEmail(account.getEmail())) {
            throw new RegistrationFailedException("Такой email уже зарегистрирован");
        }

        accountsRepository.save(account);

        RegisterResponse response = RegisterResponse.from(account);
        return response;

    }

    @Override
    public LogoutResponse logoutAccount(Map<String, String> headers) {
        if (headers.containsKey(HttpHeaders.AUTHORIZATION.toLowerCase())) {
            String token = headers.get(HttpHeaders.AUTHORIZATION.toLowerCase());
            if (token != null && token.startsWith("Bearer ")) {
                token = token.split(" ")[1].trim();
                blackListRepository.saveToBlacklist(token);
                SecurityContextHolder.getContext();
                return LogoutResponse.builder()
                        .status(HttpStatus.ACCEPTED)
                        .message("Успешный выход из системы")
                        .build();
            }

            return LogoutResponse.builder()
                    .status(HttpStatus.NOT_ACCEPTABLE)
                    .message("Неверный формат токена")
                    .build();

        }

        return LogoutResponse.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message("Отсутствует заголовок авторизации")
                .build();
    }

    @Override
    public ResponseEntity getUsers() {

        return ResponseEntity.ok(accountsRepository.findAll());
    }


}
