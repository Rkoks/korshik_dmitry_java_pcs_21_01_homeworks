package ru.rkoks.repositories.auth;

public interface TokenBlacklistRepository {
    void saveToBlacklist(String token);

    boolean existsInBlacklist(String token);
}
