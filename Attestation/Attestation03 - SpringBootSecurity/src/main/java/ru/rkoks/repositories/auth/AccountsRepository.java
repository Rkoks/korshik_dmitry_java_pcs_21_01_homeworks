package ru.rkoks.repositories.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rkoks.security.details.Account;

import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);

    boolean existsByEmail(String email);

}
