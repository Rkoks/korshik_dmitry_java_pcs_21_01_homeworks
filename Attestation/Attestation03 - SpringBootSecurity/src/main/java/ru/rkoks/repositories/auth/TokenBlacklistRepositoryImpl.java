package ru.rkoks.repositories.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class TokenBlacklistRepositoryImpl implements TokenBlacklistRepository {

    private final RedisTemplate<String, String> redisTemplate;
    private final String BLACKLIST_PREFIX = "BL_";

    @Override
    public void saveToBlacklist(String token) {
        redisTemplate.opsForValue().set(BLACKLIST_PREFIX + token, "");
    }

    @Override
    public boolean existsInBlacklist(String token) {
        Boolean hasToken = redisTemplate.hasKey(BLACKLIST_PREFIX + token);
        return hasToken != null && hasToken;
    }
}
