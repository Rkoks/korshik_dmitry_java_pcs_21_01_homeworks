insert into account (email, password, role, state)
values ('admin@mail.ru',
        '$2a$10$FetbvmZ/HVltVKuDWV8y6eDlsqiPA/GASWBnwq4Se5YosN15b3.CS',
        'ADMIN',
        'CONFIRMED');