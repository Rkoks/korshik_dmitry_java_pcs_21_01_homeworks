package ru.rkoks.controllers.editor;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.dto.requests.editor.QuizRequestDto;
import ru.rkoks.domains.models.main.Preview;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.utils.ModelView;
import ru.rkoks.domains.utils.RequestView;
import ru.rkoks.security.details.Account;
import ru.rkoks.security.details.UserInfo;
import ru.rkoks.security.filters.UserInformationFilter;
import ru.rkoks.services.editor.QuizModifierService;
import ru.rkoks.services.editor.QuizStatelessService;

import javax.annotation.security.RolesAllowed;
import javax.servlet.annotation.MultipartConfig;
import java.util.List;

@RestController
@RequestMapping("api/editor")
@RequiredArgsConstructor
@RolesAllowed({Account.Role.ADMIN, Account.Role.CREATOR})
@SecurityRequirement(name = "swaggerSecure")
@MultipartConfig
@Tag(name = "Редактор", description = "Позволяет создавать и редактировать викторины")
public class EditorController {
    private final QuizStatelessService quizStatelessService;
    private final QuizModifierService quizModifierService;

    //Получить все викторины - только для админа
    @Operation(tags = {"Редактор"}, summary = "Просмотреть все существующие на платформе викторины",
    description = "Доступно только пользователям с уровнем доступа Admin")
    @GetMapping("quiz/all")
    @RolesAllowed(Account.Role.ADMIN)
    public @JsonView(ModelView.AdminView.class)
    List<Quiz> getAllQuizzes() {
        return quizStatelessService.getAllQuizzes();
    }

    //Получить все свои викторины
    @Operation(summary = "Просмотреть все созданные вами викторины")
    @GetMapping("quiz")
    public @JsonView(ModelView.CreatorView.class)
    List<Quiz> getMineQuizzes(
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        return quizStatelessService.getUserQuizzes(userInfo);
    }

    //Получить конкретную викторину
    @Operation(summary = "Просмотреть викторину по её ID")
    @GetMapping("quiz/{quiz-id}")
    public @JsonView(ModelView.CreatorView.class)
    Quiz getQuizById(
            @PathVariable(value = "quiz-id") String quizId,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        return quizStatelessService.getQuiz(quizId, userInfo);
    }

    //Создать новую викторину
    @Operation(summary = "Создать новую викторину")
    @PostMapping(value = "quiz")
    @ResponseStatus(HttpStatus.CREATED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz createQuiz(
            @JsonView(RequestView.MainWithQuestionView.class)
            @RequestBody QuizRequestDto quizDto,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        quizDto.setCreator(userInfo);
        return quizStatelessService.addQuiz(quizDto);
    }

    //Скрыть или показать викторину
    @Operation(summary = "Сделать викторину доступной или недоступной для участников")
    @PostMapping(value = "quiz/{quiz-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz changeHideStateOfQuiz(
            @PathVariable("quiz-id") String quizId,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        return quizModifierService.changeHiddenState(quizId, userInfo);
    }

    //Обновить некоторые поля в викторине
    @Operation(summary = "Обновить информацию о викторине")
    @PutMapping("quiz/{quiz-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz updateQuiz(
            @PathVariable("quiz-id") String quizId,
            @Parameter(description = "Вводить значения необходимо только в те поля, которые нужно обновить")
            @RequestBody @JsonView(RequestView.MainView.class) QuizRequestDto quiz,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        quiz.setCreator(userInfo);
        return quizModifierService.updateQuiz(quizId, quiz);
    }

    //TODO Это ненормальная загрузка изображений к викторине, удалить метод
    @Hidden
    @PutMapping(value = "quiz/{quiz-id}/preview", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz addImageToPreview(
            @PathVariable("quiz-id") String quizId,
            @RequestPart("previewImage") MultipartFile previewImage,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo
    ){
        return quizModifierService.addImageToPreview(quizId, previewImage, userInfo);
    }



    //Добавить вопросы в викторину
    @Operation(summary = "Добавить вопросы в викторину")
    @PutMapping("quiz/{quiz-id}/questions")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz pushQuestionsToQuiz(
            @PathVariable("quiz-id") String quizId,
            @Parameter(description = "Можно вводить сразу несколько вопросов")
            @RequestBody @JsonView(RequestView.WithQuestionsView.class) QuizRequestDto quiz,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        quiz.setCreator(userInfo);
        return quizModifierService.pushQuestions(quizId, quiz);

    }

    //Удалить викторину
    @Operation(summary = "Удалить викторину")
    @DeleteMapping("quiz/{quiz-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz deleteQuiz(
            @PathVariable("quiz-id") String quizId,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        return quizModifierService.deleteQuiz(quizId, userInfo);
    }

    //Удалить вопросы из викторины
    @Operation(summary = "Удалить вопросы из викторины по их ID")
    @DeleteMapping("quiz/{quiz-id}/questions")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @JsonView(ModelView.CreatorView.class)
    Quiz deleteQuestionsFromQuiz(
            @PathVariable("quiz-id") String quizId,
            @RequestParam List<String> questionsIds,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo) {
        return quizModifierService.deleteQuestions(quizId, questionsIds, userInfo);
    }

}
