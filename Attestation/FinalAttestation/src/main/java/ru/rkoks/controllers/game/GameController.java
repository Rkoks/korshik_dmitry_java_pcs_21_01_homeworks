package ru.rkoks.controllers.game;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.domains.dto.requests.game.UserAnswerDto;
import ru.rkoks.domains.dto.responses.game.QuizResponseDto;
import ru.rkoks.security.details.UserInfo;
import ru.rkoks.security.filters.UserInformationFilter;
import ru.rkoks.services.game.GameService;

import javax.servlet.annotation.MultipartConfig;
import java.util.List;

@RestController
@RequestMapping("api/game")
@RequiredArgsConstructor
@SecurityRequirement(name = "swaggerSecure")
@MultipartConfig
@Tag(name = "Викторины", description = "Интерфейс для участников")
public class GameController {
    private final GameService gameService;

    @Operation(summary = "Просмотреть викторины доступные для участия")
    @GetMapping("quiz")
    public List<QuizResponseDto> showQuizzesByState(
            @RequestParam(value = "status", required = false) String state,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo
    ) {
        return gameService.showQuizzes(state);
    }

    @Operation(summary = "Участвовать в викторине", description = "Участие происходит в 4 этапа: 1) Записаться 2) " +
            "Начать викторину 3) Ответить на все вопросы 4) Ознакомиться с результатами")
    @PostMapping(value = "quiz/{quiz-id}")
    public QuizResponseDto subscribeToQuiz(
            @RequestParam(value = "quiz-id", required = true) String quizId,
            @Parameter(description = "Тело запроса необходимо только для 3го этапа. Поле \"repeatQuestion\" " +
                    "должно быть true только при необходимости показать вопрос, на котором остановился пользователь," +
                    " в остальных случаях оно должно быть false или отсутствовать.")
            @RequestBody(required = false) UserAnswerDto userAnswer,
            @RequestAttribute(name = UserInformationFilter.USER_INFO_ATTR_NAME) UserInfo userInfo
    ) {
        userAnswer.setQuizId(quizId);
        userAnswer.setPlayer(userInfo);
        return gameService.playQuiz(userAnswer);

    }
}
