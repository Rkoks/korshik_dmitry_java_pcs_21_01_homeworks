package ru.rkoks.controllers.auth;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.domains.exceptions.RegistrationFailedException;
import ru.rkoks.domains.forms.AuthForm;
import ru.rkoks.domains.forms.RegisterForm;
import ru.rkoks.domains.dto.responses.auth.LoginResponse;
import ru.rkoks.domains.dto.responses.auth.LogoutResponse;
import ru.rkoks.domains.dto.responses.auth.RegisterResponse;
import ru.rkoks.services.auth.AccountService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/auth")
@Tag(name = "Авторизация", description = "Работа пользователя с аккаунтом")
public class AuthController {
    private final AccountService accountService;

    @Operation(summary = "Войти в систему под своей учётной записью")
    @PostMapping("login")
    public ResponseEntity login(@RequestBody @Valid AuthForm authForm) {
        try {
            LoginResponse response = accountService.loginAccount(authForm);

            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION,
                            "Bearer " + response.getToken())
                    .body(response);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }


    @Operation(summary = "Зарегистрироваться в системе")
    @PostMapping("register")
    public ResponseEntity register(@RequestBody @Valid RegisterForm form) {

        try {
            RegisterResponse response = accountService.registerAccount(form);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(response);
        } catch (RegistrationFailedException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }

    @Operation(summary = "Выйти из системы")
    @GetMapping("logout")
    @PreAuthorize("isAuthenticated()")
    @SecurityRequirement(name = "swaggerSecure")
    public ResponseEntity logout(@RequestHeader Map<String, String> headers) {
        LogoutResponse response = accountService.logoutAccount(headers);
        return ResponseEntity
                .status(response.getStatus())
                .body(response.getMessage());

    }

    //TODO Спрятать ендпоинт от прямого использования в Swagger
    @Hidden
    @GetMapping("email")
    @PreAuthorize("permitAll()")
    public ResponseEntity confirmEmail(@RequestParam("token") String token) {
        return accountService.confirmEmail(token);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException e) {
        Map<String, String> errors = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return errors;
    }
}
