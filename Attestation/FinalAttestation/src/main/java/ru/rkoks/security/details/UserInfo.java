package ru.rkoks.security.details;

import lombok.Builder;
import lombok.Getter;

/**
 * Модель аккаунта для работы сервисов
 */
@Getter
@Builder
public class UserInfo {
    private Long id;
    private String role;
    private Account.State state;
}
