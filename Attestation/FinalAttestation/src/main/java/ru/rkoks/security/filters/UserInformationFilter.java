package ru.rkoks.security.filters;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.rkoks.security.details.UserInfo;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Фильтр, достающий информацию из JWT-токена в случае успешной аутентификации и прокидывающий её
 * в контроллеры для дальнейшей работы сервисов
 */
@Component
@RequiredArgsConstructor
public class UserInformationFilter extends OncePerRequestFilter {
    private final JwtTokenUtil jwtTokenUtil;
    private final Set<String> freeUrls = Set.of("/swagger", "/swagger-ui", "/v3/api-docs", "/api/auth");
    public static final String USER_INFO_ATTR_NAME = "UserInfo";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) { // если произведена аутентификация
            String tokenHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
            String token = tokenHeader.split(" ")[1].trim();

            UserInfo userInfo = UserInfo.builder()
                    .id(jwtTokenUtil.getUserId(token))
                    .role(jwtTokenUtil.getRole(token))
                    .state(jwtTokenUtil.getState(token))
                    .build();

            request.setAttribute(USER_INFO_ATTR_NAME, userInfo);
        }

        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path = request.getRequestURI();
        return freeUrls.stream().anyMatch(path::startsWith);
    }


}
