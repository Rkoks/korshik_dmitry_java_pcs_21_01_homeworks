package ru.rkoks.services.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.dto.requests.game.UserAnswerDto;
import ru.rkoks.domains.dto.responses.game.PreviewResponseDto;
import ru.rkoks.domains.dto.responses.game.QuestionResponseDto;
import ru.rkoks.domains.dto.responses.game.QuizResponseDto;
import ru.rkoks.domains.models.main.Participators;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.models.statistics.QuizStatistic;
import ru.rkoks.domains.models.statistics.UserAnswerTry;
import ru.rkoks.repositories.jpa.QuizzesRepository;
import ru.rkoks.services.editor.FileStorageService;
import ru.rkoks.services.editor.ParticipatorsService;
import ru.rkoks.services.editor.QuestionService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GameServiceImpl implements GameService {
    private final QuizzesRepository quizzesRepository;
    private final ParticipatorsService participatorsService;
    private final StatisticService statisticService;
    private final QuestionService questionService;
    //TODO Если не использую файлы для отображения, то нужен ли этот сервис?
    private final FileStorageService fileStorageService;

    @Override
    public QuizResponseDto getFullPreviewInfo(Quiz quiz, QuizResponseDto result) {
        if (result == null) {
            result = QuizResponseDto.builder().build();
        }
        result.setQuizId(quiz.get_id());
        result.setPreview(PreviewResponseDto.from(quiz.getPreview()));
        result.setBeginDate(quiz.getBeginDate());
        result.setFinishDate(quiz.getFinishDate());
        return result;
    }

    @Override
    public QuizResponseDto playQuiz(UserAnswerDto userAnswer) {
        QuizResponseDto result = QuizResponseDto.builder().build();
        Optional<QuizStatistic> optionalStatistic = statisticService.getStatistic(userAnswer);
        Optional<Quiz> optionalQuiz = quizzesRepository.findBy_idAndIsDeletedFalse(userAnswer.getQuizId());


        Quiz quiz;
        if (optionalQuiz.isPresent()) { //Проверяем правильность ввода идентификатора викторины
            quiz = optionalQuiz.get();
        } else {
            result.setMessage("Неправильный идентификатор викторины");
            return result;
        }

        QuizStatistic statistic;
        if (optionalStatistic.isEmpty()) {//Участник не регистрировался на викторине
            if (quiz.getState() == Quiz.State.AWAIT || quiz.getState() == Quiz.State.ACTIVE) { //Доступна ли викторина для записи на участие
                subscribe(quiz, userAnswer, result);
            } else {
                result.setMessage("Викторина не доступна для записи");
            }
        } else { //Статистика создана
            statistic = optionalStatistic.get();
            if (quiz.getState() == Quiz.State.FINISH || quiz.getState() == Quiz.State.HIDDEN) { //Если викторина уже закончена или отменена
                getResult(statistic, result);
            } else if (quiz.getState() == Quiz.State.ACTIVE) { //Если викторина активна
                if (statistic.getState() == QuizStatistic.State.SUBSCRIBED) { //Если игрок на неё подписан
                    start(statistic, quiz, result);
                } else if (statistic.getState() == QuizStatistic.State.ANSWERED) { //Если игрок её уже проходит
                    if (userAnswer.getRepeatQuestion() != null && userAnswer.getRepeatQuestion()) { //Если игрок забыл вопрос
                        repeatQuestion(statistic, result);
                    } else {
                        checkAnswer(userAnswer, statistic, quiz.getParticipators(), result);
                    }
                } else { //Если игрок уже прошёл эту викторину
                    getResult(statistic, result);
                }
            } else {//Если викторина ещё не началась
                getFullPreviewInfo(quiz, result);
                result.setMessage("Викторина ещё не началась");
            }
        }

        return result;
    }

    /**
     * Реализует напоминание по текущему вопросу, если пользователь забыл или захотел прочитать его ещё раз
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @param result    объект отображения необходимой информации по текущему этапу викторины
     * @return ответ пользователю <code>result</code>
     */
    private QuizResponseDto repeatQuestion(QuizStatistic statistic, QuizResponseDto result) {
        Question nextQuestion = questionService.getById(statistic.getNextQuestionId());

        result.setQuizId(statistic.getQuizId());
        result.setState(statistic.getState());
        result.setQuestion(QuestionResponseDto.from(nextQuestion));
        result.setMessage("Вы остановились на данном вопросе");
        return result;
    }

    @Override
    public List<QuizResponseDto> showQuizzes(String state) {
        List<Quiz.State> states;
        if (state == null || state.isEmpty() || state.equalsIgnoreCase("all")) {
            states = Arrays.asList(Quiz.State.ACTIVE, Quiz.State.AWAIT);
        } else {
            state = state.toUpperCase();
            if (state.equals(Quiz.State.ACTIVE.name()) || state.equals(Quiz.State.AWAIT.name())) {
                states = Collections.singletonList(Quiz.State.valueOf(state));
            } else {
                return Collections.singletonList(QuizResponseDto.builder()
                        .message("Статус может быть 'Active' или 'Await'").build());
            }
        }

        List<QuizResponseDto> quizList = QuizResponseDto.mapForShowing(quizzesRepository
                .findAllByStateInAndIsDeletedFalse(states));

        //TODO нужно ли это раз нет нормальной загрузки файлов?
//            quizList.forEach(quiz -> {
//                PreviewResponseDto preview = quiz.getPreview();
//                preview.setFile(fileStorageService.loadFile(preview.getImageFilename()));
//            });

        return quizList;
    }

    /**
     * Реализует подписывание пользователя на указанную викторину для дальнейшего участия в ней
     *
     * @param quiz       викторина, в которой пользователь желает принять участие
     * @param userAnswer запрос от пользователя с указанием необходимой информации для текущего этапа прохождения
     * @param result     объект отображения необходимой информации по текущему этапу викторины
     * @return ответ пользователю <code>result</code>
     */
    private QuizResponseDto subscribe(Quiz quiz, UserAnswerDto userAnswer, QuizResponseDto result) {
        if (participatorsService.notInWillingList(quiz.getParticipators(), userAnswer.getPlayer().getId())) {
            participatorsService.addToWillingList(quiz.getParticipators(), userAnswer.getPlayer().getId());

            QuizStatistic statistic = statisticService.createStatisticBySubscribe(userAnswer, quiz);
            statisticService.saveStatistic(statistic);

            getFullPreviewInfo(quiz, result);
            result.setState(statistic.getState());
            result.setMessage("Вы успешно записались на викторину");
            //TODO Если нет нормальной работы с файлами, нужна ли эта строчка?
//            result.getPreview().setFile(fileStorageService.loadFile(result.getPreview().getImageFilename()));
        } else {
            result.setMessage("Вы уже записывались на эту викторину");
        }

        return result;
    }

    /**
     * Реализует начало участия в викторине
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @param quiz      викторина, в которой пользователь начинает отвечать на вопросы
     * @param result    объект отображения необходимой информации по текущему этапу викторины
     * @return ответ пользователю <code>result</code>
     */
    private QuizResponseDto start(QuizStatistic statistic, Quiz quiz, QuizResponseDto result) {
        if (participatorsService.notInBeginList(quiz.getParticipators(), statistic.getPlayerId())) {
            participatorsService.addToBeginList(quiz.getParticipators(), statistic.getPlayerId());

            statisticService.updateStatisticByStart(statistic, quiz);
            statistic.setState(QuizStatistic.State.ANSWERED);
            Question nextQuestion = getNextQuestion(statistic);
            statisticService.saveStatistic(statistic);

            result.setQuizId(statistic.getQuizId());
            result.setState(statistic.getState());
            result.setQuestion(QuestionResponseDto.from(nextQuestion));
            result.setMessage("Вы приступили к выполнению заданий");
        } else {
            result.setMessage("Вы уже начали прохождение этой викторины");
        }
        return result;
    }

    /**
     * Проверяет ответ пользователя на правильность, обновляет статистику и отдаёт следующий вопрос (при наличии)
     * или переходит к результатам
     *
     * @param userAnswer    запрос от пользователя с указанием необходимой информации для текущего этапа прохождения
     * @param statistic     объект хранящий статистику по текущей для пользователя викторине
     * @param participators объект, хранящий списки участников по этапам
     * @param result        объект отображения необходимой информации по текущему этапу викторины
     * @return ответ пользователю <code>result</code>
     */
    private QuizResponseDto checkAnswer(UserAnswerDto userAnswer, QuizStatistic statistic, Participators participators, QuizResponseDto result) {
        if (userAnswer.getQuestionId().equals(statistic.getNextQuestionId())) { //Игрок отвечает на нужный вопрос
            UserAnswerTry answerForSave = UserAnswerTry.builder()
                    .answer(userAnswer.getAnswer())
                    .questionId(userAnswer.getQuestionId())
                    .build();
            statistic.getAnswerTries().add(answerForSave);

            statistic.setCurrentPoints(statistic.getCurrentPoints() +
                    statisticService.checkUserAnswer(userAnswer));

            result.setMessage("Ваш ответ принят");
            if (statistic.getQuestionsWithoutAnswerIdList().size() == 0) { //Закончились вопросы
                if (participatorsService.notInFinishedList(participators, statistic.getPlayerId())) {
                    participatorsService.addToFinishedList(participators, statistic.getPlayerId());
                    statistic.setState(QuizStatistic.State.FINISHED);
                    getResult(statistic, result);
                    result.setMessage("Вы закончили прохождение викторины. Ваши результаты:");
                } else {
                    result.setMessage("Вы уже прошли эту викторину");
                }
            } else {//Если не закончились, то берём следующий вопрос
                Question nextQuestion = getNextQuestion(statistic);
                result.setQuizId(statistic.getQuizId());
                result.setQuestion(QuestionResponseDto.from(nextQuestion));
            }

            statisticService.saveStatistic(statistic);
        } else {
            result.setMessage("Неверный идентификатор вопроса");
        }

        return result;
    }

    /**
     * Реализует отображение результатов викторины
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @param result    объект отображения необходимой информации по текущему этапу викторины
     * @return ответ пользователю <code>result</code>
     */
    private QuizResponseDto getResult(QuizStatistic statistic, QuizResponseDto result) {
        result.setQuizId(statistic.getQuizId());
        result.setState(QuizStatistic.State.FINISHED);
        result.setMaxPoints(statistic.getMaxPoints());
        result.setResultPoints(statistic.getCurrentPoints());
        result.setPointsToPass(result.getMaxPoints() * statistic.getPercentsToPass() / 100);
        result.setMessage("Ваши результаты:");
        return result;
    }

    /**
     * Достаёт следующий вопрос викторины
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @return возвращает объект вопроса
     */
    private Question getNextQuestion(QuizStatistic statistic) {
        String nextQuestionId = statistic.getQuestionsWithoutAnswerIdList().get(0);
        statistic.setNextQuestionId(nextQuestionId);
        statistic.getQuestionsWithoutAnswerIdList().remove(nextQuestionId);

        Question nextQuestion = questionService.getById(nextQuestionId);

        return nextQuestion;
    }
}
