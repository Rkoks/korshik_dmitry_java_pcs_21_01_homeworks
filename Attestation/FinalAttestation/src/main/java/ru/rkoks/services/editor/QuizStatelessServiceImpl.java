package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.dto.requests.editor.QuizRequestDto;
import ru.rkoks.domains.models.main.Participators;
import ru.rkoks.domains.models.main.Preview;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.repositories.jpa.QuizzesRepository;
import ru.rkoks.security.details.UserInfo;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuizStatelessServiceImpl implements QuizStatelessService {
    private final QuizzesRepository quizzesRepository;
    private final PreviewService previewService;
    private final QuestionService questionService;
    private final ParticipatorsService participatorsService;
    private final QuizModifierService quizModifierService;

    @Override
    public List<Quiz> getAllQuizzes() {
        return quizzesRepository.findAll();
    }

    @Override
    public List<Quiz> getUserQuizzes(UserInfo userInfo) {
        return quizzesRepository.findAllByCreatorIdAndIsDeletedFalse(userInfo.getId());
    }

    @Override
    public Quiz getQuiz(String quizId, UserInfo userInfo) {
        //TODO Добавить свои исключения и ответы пользователю
        return quizzesRepository.findBy_idAndCreatorId(quizId, userInfo.getId())
                .orElseThrow(() -> new RuntimeException("Несуществующая викторина"));
    }

    @Override
    public Quiz addQuiz(QuizRequestDto quizDto) {
        UserInfo creator = quizDto.getCreator();
        Participators participators = participatorsService.createParticipators(creator);

        //TODO добавить валидацию по этим полям в контроллер, а также датам
        Preview preview = previewService.addPreview(quizDto.getPreviewDto(), creator);
        List<Question> questions = questionService.addQuestions(quizDto.getQuestionsDto(), creator);
        participators = participatorsService.saveParticipators(participators);

        Quiz quiz = quizDto.createShortModel();

        quiz.setPreview(preview);
        quiz.setQuestions(questions);
        quiz.setParticipators(participators);
        quiz.setIsDeleted(false);

        quizModifierService.setRightState(quiz, creator);

        return quizzesRepository.save(quiz);
    }


}
