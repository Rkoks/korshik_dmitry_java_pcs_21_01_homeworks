package ru.rkoks.services.editor;

import ru.rkoks.domains.dto.requests.editor.QuestionRequestDto;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.security.details.UserInfo;

import java.util.List;

/**
 * Интерфейс сервиса по работе с вопросами викторин
 */
public interface QuestionService {

    /**
     * Достаёт из БД все вопросы по списку идентификаторов, которые добавлял конкретный пользователь
     *
     * @param questionIds запрашиваемые вопросы в формате списка идентификаторов
     * @param userInfo    пользователь по которому производится отбор
     * @return список объектов вопросов, найденных в БД
     */
    List<Question> getQuestionByIdListForUser(List<String> questionIds, UserInfo userInfo);

    /**
     * Добавляет список новых вопросов в БД
     *
     * @param questionsDto список информации по каждому вопросу для добавления
     * @param userInfo     пользователь, добавляющий вопросы
     * @return список объектов вопросов, сохранённых в БД
     */
    List<Question> addQuestions(List<QuestionRequestDto> questionsDto, UserInfo userInfo);

    /**
     * Удаляет (помечает удалёнными) все вопросы из БД в соответствии со списком
     *
     * @param questions список вопросов для удаления
     */
    void deleteQuestions(List<Question> questions);

    /**
     * Достаёт вопрос из БД по идентификатору
     *
     * @param questionId идентификатор для поиска
     * @return объект вопроса, найденный в БД
     */
    Question getById(String questionId);
}
