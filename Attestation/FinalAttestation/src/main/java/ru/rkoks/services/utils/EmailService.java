package ru.rkoks.services.utils;

/**
 * Интерфейс сервиса для отправки писем на электронную почту
 */
public interface EmailService {

    /**
     * Отправляем письмо для вновь зарегистрированного пользователя со ссылкой для подтверждения
     * @param email Адрес получателя, указанный при регистрации
     * @param token уникальный идентификатор для подтверждения электронного адреса
     * @return true - при успешном формировании и отправке письма, иначе false
     */
    boolean sendConfirmationEmail(String email, String token);


}
