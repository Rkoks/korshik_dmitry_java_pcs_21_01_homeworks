package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.dto.requests.editor.PreviewRequestDto;
import ru.rkoks.domains.models.main.Preview;
import ru.rkoks.domains.utils.ModelUtils;
import ru.rkoks.repositories.jpa.PreviewsRepository;
import ru.rkoks.security.details.UserInfo;

@Service
@RequiredArgsConstructor
public class PreviewServiceImpl implements PreviewService {
    private final PreviewsRepository previewsRepository;
    private final FileStorageService fileStorageService;

    @Override
    public Preview addPreview(PreviewRequestDto previewDto, UserInfo userInfo) {
        previewDto.setCreator(userInfo);

        Preview preview = previewDto.createModel();
        preview.setIsDeleted(false);
        return previewsRepository.save(preview);
    }

    @Override
    public Preview addImage(Preview preview, MultipartFile image, UserInfo userInfo) {
        String imageFilename = fileStorageService.uploadFile(image);
        preview.setImageFilename(imageFilename);
        return previewsRepository.save(preview);
    }

    @Override
    public Preview updatePreview(Preview preview, PreviewRequestDto previewDto) throws RuntimeException {
        Preview newPreview = previewDto.createModel();
        return previewsRepository.save(ModelUtils.updateModelWithoutNullFields(preview, newPreview));
    }


}
