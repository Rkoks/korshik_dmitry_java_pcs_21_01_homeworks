package ru.rkoks.services.utils;

/**
 * Интерфейс сервиса, описывающий необходимое поведение для выполнения по расписанию с помощью Spring Scheduler
 */
public interface SchedulerService {
    /**
     * Метод вызывающийся по расписанию cron-выражения (${quiz.state.update.cron.expression}) и обновляющий статус
     * викторин в зависимости от указанных в них датах начала и окончания
     */
    void updateQuizState();
}
