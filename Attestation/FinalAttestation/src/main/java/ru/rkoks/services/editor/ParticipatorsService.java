package ru.rkoks.services.editor;

import ru.rkoks.domains.models.main.Participators;
import ru.rkoks.security.details.UserInfo;

/**
 * Интерфейс сервиса по работе со списками участников викторины
 */
public interface ParticipatorsService {
    /**
     * Создать объект списков участников
     *
     * @param creator пользователь, создающий викторину
     * @return объект списков участников, добавленный в БД
     */
    Participators createParticipators(UserInfo creator);

    /**
     * Сохраняет объект списка участников
     *
     * @param participators списки, которые необходимо удалить
     * @return объект списков участников, сохранённый в БД
     */
    Participators saveParticipators(Participators participators);

    /**
     * Добавляет пользователя в список записавшихся на участие
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо добавить в список
     */
    void addToWillingList(Participators participators, Long playerId);

    /**
     * Добавляет пользователя в список приступивших к выполнению заданий викторины
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо добавить в список
     */
    void addToBeginList(Participators participators, Long playerId);

    /**
     * Добавляет пользователя в список завершивших викторину
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо добавить в список
     */
    void addToFinishedList(Participators participators, Long playerId);

    /**
     * Проверяет отсутствие пользователя в списке записавшихся на участие
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо проверить
     */
    boolean notInWillingList(Participators participators, Long playerId);

    /**
     * Проверяет отсутствие пользователя в списке приступивших к выполнению заданий викторины
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо проверить
     */
    boolean notInBeginList(Participators participators, Long playerId);

    /**
     * Проверяет отсутствие пользователя в списке завершивших викторину
     *
     * @param participators объект списков участников, содержащий список нужной викторины
     * @param playerId      пользователь, которого необходимо проверить
     */
    boolean notInFinishedList(Participators participators, Long playerId);
}
