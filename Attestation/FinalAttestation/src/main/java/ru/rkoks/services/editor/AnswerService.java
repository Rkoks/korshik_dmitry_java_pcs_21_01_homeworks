package ru.rkoks.services.editor;

import ru.rkoks.domains.models.main.Answer;

/**
 * Интерфейс сервиса по работе с объектами ответов
 */
public interface AnswerService {

    /**
     * Добавляет ответ в БД
     *
     * @param answer ответ, необходимый к добавлению
     * @return объект, сохранённый в БД
     */
    Answer addAnswer(Answer answer);

    /**
     * Меняет статус объекта вопроса на "удалённый"
     *
     * @param answer ответ, необходимый к удалению
     */
    void deleteAnswer(Answer answer);

}
