package ru.rkoks.services.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.repositories.jpa.QuizzesRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SchedulerServiceImpl implements SchedulerService {
    private final QuizzesRepository quizzesRepository;

    @Scheduled(cron = "${quiz.state.update.cron.expression}")
    @Override
    public void updateQuizState() {
        System.out.println("Cron is working at " + LocalDateTime.now());
        List<Quiz> quizzes = quizzesRepository.findAllByStateNotAndIsDeletedFalse(Quiz.State.HIDDEN);
        for (Quiz quiz : quizzes) {
            checkState(quiz);
        }

    }

    /**
     * Проверяет текущее состояние викторины и меняет его при необходимости
     *
     * @param quiz викторина, состояние которой проверяется
     */
    private void checkState(Quiz quiz) {
        LocalDate checkDate = LocalDate.now().plusDays(1);
        Quiz.State quizState = quiz.getState();

        if (checkDate.isAfter(quiz.getFinishDate())) {
            if (!quizState.equals(Quiz.State.FINISH)) {
                quiz.setState(Quiz.State.FINISH);
                quizzesRepository.save(quiz);
                System.out.println("Quiz update to Finish");
            }
        } else if (checkDate.isAfter(quiz.getBeginDate())) {
            if (!quizState.equals(Quiz.State.ACTIVE)) {
                quiz.setState(Quiz.State.ACTIVE);
                quizzesRepository.save(quiz);
                System.out.println("Quiz update to Active");
            }
        } else {
            if (!quizState.equals(Quiz.State.AWAIT)) {
                quiz.setState(Quiz.State.AWAIT);
                quizzesRepository.save(quiz);
                System.out.println("Quiz update to Await");
            }
        }
    }
}
