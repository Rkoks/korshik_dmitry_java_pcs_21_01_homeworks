package ru.rkoks.services.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.dto.requests.game.UserAnswerDto;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.models.statistics.QuizStatistic;
import ru.rkoks.repositories.jpa.StatisticRepository;
import ru.rkoks.services.editor.QuestionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatisticServiceImpl implements StatisticService {
    private final StatisticRepository statisticRepository;
    private final QuestionService questionService;

    @Override
    public QuizStatistic createStatisticBySubscribe(UserAnswerDto userAnswer, Quiz quiz) {
        QuizStatistic statistic = QuizStatistic.builder()
                .playerId(userAnswer.getPlayer().getId())
                .quizId(userAnswer.getQuizId())
                .state(QuizStatistic.State.SUBSCRIBED)
                .answerTries(new ArrayList<>())
                .currentPoints(0)
                .percentsToPass(quiz.getPointsLimitInPercents())
                .build();

        int maxPoints = 0;
        for (Question question : quiz.getQuestions()) {
            maxPoints += question.getAnswer().getPointsForCorrectAnswer();
        }
        statistic.setMaxPoints(maxPoints);


        return statistic;
    }

    @Override
    public QuizStatistic updateStatisticByStart(QuizStatistic statistic, Quiz quiz) {
        statistic.getAnswerTries().clear();
        int maxPoints = 0;
        List<String> withoutAnswerList = new ArrayList<>();
        for (Question question : quiz.getQuestions()) {
            maxPoints += question.getAnswer().getPointsForCorrectAnswer();
            withoutAnswerList.add(question.get_id());
        }

        statistic.setMaxPoints(maxPoints);
        statistic.setQuestionsWithoutAnswerIdList(withoutAnswerList);
        return statistic;
    }


    @Override
    public Optional<QuizStatistic> getStatistic(UserAnswerDto userAnswer) {
        return statisticRepository.findByQuizIdAndPlayerId(userAnswer.getQuizId(), userAnswer.getPlayer().getId());
    }

    @Override
    public QuizStatistic saveStatistic(QuizStatistic statistic) {
        return statisticRepository.save(statistic);
    }

    @Override
    public Integer checkUserAnswer(UserAnswerDto userAnswer) {
        Question question = questionService.getById(userAnswer.getQuestionId());
        return question.getAnswer().checkCorrect(userAnswer.getAnswer());
    }


}
