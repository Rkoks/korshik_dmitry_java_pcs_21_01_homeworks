package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.models.main.Answer;
import ru.rkoks.repositories.jpa.AnswersRepository;

@Service
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {
    private final AnswersRepository answersRepository;

    @Override
    public Answer addAnswer(Answer answer) {
        if (!answer.validAnswer()) {
            throw new RuntimeException("Ответ должен быть корректно заполнен");
        }
        answer.setIsDeleted(false);
        return answersRepository.save(answer);
    }

    @Override
    public void deleteAnswer(Answer answer) {
        answer.setIsDeleted(true);
        answersRepository.save(answer);
    }

}
