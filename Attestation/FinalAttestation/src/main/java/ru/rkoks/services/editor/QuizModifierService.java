package ru.rkoks.services.editor;

import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.dto.requests.editor.QuizRequestDto;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.security.details.UserInfo;

import java.util.List;

/**
 * Интерфейс сервиса по изменению существующих викторин
 */
public interface QuizModifierService {
    /**
     * Обновляет информацию в викторине
     *
     * @param quizId  идентификатор викторины, которую необходимо обновить
     * @param quizDto информация, которую необходимо вставить/изменить в викторине
     * @return объект викторины, сохранённой в БД
     */
    Quiz updateQuiz(String quizId, QuizRequestDto quizDto);

    /**
     * Удаляет (помечает удалённым) викторину в БД
     *
     * @param quizId   идентификатор викторины, которую необходимо удалить
     * @param userInfo пользователь, производящий операцию для проверки прав доступа
     * @return удалённый объект викторины
     */
    Quiz deleteQuiz(String quizId, UserInfo userInfo);

    /**
     * Актуализирует состояние викторины
     *
     * @param quiz     объект викторины для актуализации
     * @param userInfo пользователь, производящий операцию для проверки прав доступа
     */
    void setRightState(Quiz quiz, UserInfo userInfo);

    /**
     * Скрывает викторину или открывает её для пользователей
     *
     * @param quizId   идентификатор викторины, которую необходимо скрыть/отобразить
     * @param userInfo пользователь, производящий операцию для проверки прав доступа
     * @return объект актуализированный в БД
     */
    Quiz changeHiddenState(String quizId, UserInfo userInfo);

    /**
     * Добавляет вопросы в викторину
     *
     * @param quizId  идентификатор викторины, в которую необходимо добавить вопросы
     * @param quizDto новые вопросы для добавления, переданные пользователем
     * @return объект актуализированный в БД
     */
    Quiz pushQuestions(String quizId, QuizRequestDto quizDto);

    /**
     * Удаляет вопросы из викторины
     *
     * @param quizId       идентификатор викторины, из которой необходимо удалить вопросы
     * @param questionsIds список идентификаторов вопросов для удаления
     * @param userInfo     пользователь, производящий операцию для проверки прав доступа
     * @return объект актуализированный в БД
     */
    Quiz deleteQuestions(String quizId, List<String> questionsIds, UserInfo userInfo);

    /**
     * Добавляет изображение в анонс указанной викторины
     *
     * @param quizId   дентификатор викторины, в анонс которой необходимо добавить файл
     * @param image    добавляемый файл
     * @param userInfo пользователь, производящий операцию для проверки прав доступа
     * @return объект актуализированный в БД
     */
    Quiz addImageToPreview(String quizId, MultipartFile image, UserInfo userInfo);
}
