package ru.rkoks.services.game;

import ru.rkoks.domains.dto.requests.game.UserAnswerDto;
import ru.rkoks.domains.dto.responses.game.QuizResponseDto;
import ru.rkoks.domains.models.main.Quiz;

import java.util.List;

/**
 * Интерфейс сервиса для прохождения викторины
 */
public interface GameService {

    /**
     * Достаёт необходимую для отображения информацию по анонсу викторины
     *
     * @param quiz запрашиваемая викторина
     * @param result объект для сохранения результата, следует проверять на null
     * @return возвращает результат отображения
     */
    QuizResponseDto getFullPreviewInfo(Quiz quiz, QuizResponseDto result);

    /**
     * Выполняет основную логику участия в викторине, например, запись, начало, ответы на вопросы, вывод результатов,
     * а также проверку на корректность вводимых пользователем данных в запросе
     *
     * @param userAnswer запрос от пользователя с указанием необходимой информации для текущего этапа прохождения
     * @return возвращает отображаемую информацию, в соответствии с текущим этапом викторины
     */
    QuizResponseDto playQuiz(UserAnswerDto userAnswer);

    /**
     * Показывает все викторины, доступные для участия
     *
     * @param state фильтр запрашиваемого состояния (например, планируется или уже началась)
     * @return отображение анонсов подходящих по запросу викторин в виде списка
     */
    List<QuizResponseDto> showQuizzes(String state);
}
