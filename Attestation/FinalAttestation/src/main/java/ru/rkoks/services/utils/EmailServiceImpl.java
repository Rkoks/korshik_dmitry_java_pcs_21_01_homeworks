package ru.rkoks.services.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender emailSender;
    private final SimpleMailMessage template;

    @Override
    public boolean sendConfirmationEmail(String email, String token) {
        MimeMessage message = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, false);
            helper.setFrom("dima.rkoks@gmail.com");
            helper.setTo(email);
            helper.setSubject("Регистрация на kvanto-quiz.ru");
            helper.setText(String.format(Objects.requireNonNull(template.getText()), token), true);
            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


}
