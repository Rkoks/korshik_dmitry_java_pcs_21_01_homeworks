package ru.rkoks.services.editor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class FileStorageServiceImpl implements FileStorageService {
    @Value("${files.storage.path}")
    private String pathName;
    @Value("${files.available.content-type}")
    private List<String> availableTypes;
    private Path path;
    private boolean notInited = true;

    @Override
    public void init() {
        try {
            path = Paths.get(pathName);
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }
            notInited = false;
        } catch (IOException e) {
            throw new RuntimeException("Не удалось создать папку " + pathName);
        }
    }

    @Override
    public String uploadFile(MultipartFile file) {
        if (notInited) {
            init();
        }

        String filename = file.getOriginalFilename();
        filename = filename.substring(filename.lastIndexOf("."));
        try {
            if (isSupportedContentType(file.getContentType())) {
                filename = UUID.randomUUID() + filename;
                Path targetPath = path.resolve(filename);
                Files.copy(file.getInputStream(), targetPath);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return filename;
    }

    @Override
    public Resource loadFile(String filename) {
        if (notInited) {
            init();
        }

        Path file = path.resolve(filename);
        try {
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Невозможно прочитать файл");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


    public boolean isSupportedContentType(String contentType) {
        return availableTypes.contains(contentType);
    }

}
