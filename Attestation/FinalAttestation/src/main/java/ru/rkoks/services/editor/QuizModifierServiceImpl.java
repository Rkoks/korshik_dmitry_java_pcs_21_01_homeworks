package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.dto.requests.editor.QuizRequestDto;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.utils.ModelUtils;
import ru.rkoks.repositories.jpa.QuizzesRepository;
import ru.rkoks.security.details.UserInfo;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuizModifierServiceImpl implements QuizModifierService {
    private final QuizzesRepository quizzesRepository;
    private final PreviewService previewService;
    private final QuestionService questionService;

    //Актуализируем состояние мероприятия
    @Override
    public void setRightState(Quiz quiz, UserInfo userInfo) {
        if (quiz.getState() != Quiz.State.HIDDEN) {
            if (LocalDate.now().plusDays(1).isAfter(quiz.getFinishDate())) {
                quiz.setState(Quiz.State.FINISH);
            } else if (LocalDate.now().plusDays(1).isAfter(quiz.getBeginDate())) {
                quiz.setState(Quiz.State.ACTIVE);
            } else {
                quiz.setState(Quiz.State.AWAIT);
            }
        }
    }

    //Скрывает или открываем мероприятие
    @Override
    public Quiz changeHiddenState(String quizId, UserInfo userInfo) {
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, userInfo);

        if (existingQuiz.getState() != Quiz.State.HIDDEN) {
            existingQuiz.setState(Quiz.State.HIDDEN);
        } else {
            existingQuiz.setState(Quiz.State.NONE);
            setRightState(existingQuiz, userInfo);
        }

        return quizzesRepository.save(existingQuiz);
    }

    //Добавить вопросы в существующую викторину
    @Override
    public Quiz pushQuestions(String quizId, QuizRequestDto quizDto) {
        UserInfo creator = quizDto.getCreator();
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, creator);

        List<Question> newQuestions = questionService.addQuestions(quizDto.getQuestionsDto(), creator);
        existingQuiz.getQuestions().addAll(newQuestions);

        return quizzesRepository.save(existingQuiz);
    }

    //Удалить вопросы из викторины
    @Override
    public Quiz deleteQuestions(String quizId, List<String> questionsIds, UserInfo userInfo) {
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, userInfo);

        List<Question> questions = questionService.getQuestionByIdListForUser(questionsIds, userInfo);
        existingQuiz.getQuestions().removeAll(questions);
        questionService.deleteQuestions(questions);
        return quizzesRepository.save(existingQuiz);
    }

    @Override
    public Quiz addImageToPreview(String quizId, MultipartFile image, UserInfo userInfo) {
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, userInfo);

        previewService.addImage(existingQuiz.getPreview(), image, userInfo);
        return quizzesRepository.save(existingQuiz);
    }

    //Обновляем поля кроме state, questions и participators
    @Override
    public Quiz updateQuiz(String quizId, QuizRequestDto quizDto) {
        UserInfo creator = quizDto.getCreator();
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, creator);

        Quiz newQuiz = Quiz.builder().build();
        if (quizDto.getPreviewDto() != null) {
            newQuiz.setPreview(previewService.updatePreview(existingQuiz.getPreview(),
                    quizDto.getPreviewDto()));
        }

        if (quizDto.getPointsLimitInPercents() != null) {
            newQuiz.setPointsLimitInPercents(quizDto.getPointsLimitInPercents());
        }

        if (quizDto.getBeginDate() != null) {
            newQuiz.setBeginDate(quizDto.getBeginDate());
        }

        if (quizDto.getFinishDate() != null) {
            newQuiz.setFinishDate(quizDto.getFinishDate());
        }

        ModelUtils.updateModelWithoutNullFields(existingQuiz, newQuiz);
        setRightState(existingQuiz, creator);

        return quizzesRepository.save(existingQuiz);
    }

    @Override
    public Quiz deleteQuiz(String quizId, UserInfo userInfo) {
        Quiz existingQuiz = checkExistenceAndReturn(quizId);
        checkPermission(existingQuiz, userInfo);
        existingQuiz.setIsDeleted(true);
        return quizzesRepository.save(existingQuiz);
    }

    /**
     * Возвращает объект запрашиваемой викторины из БД, выбрасывает исключение в случае его отсутствия
     *
     * @param quizId идентификатор викторины для поиска
     * @return объект найденной викторины
     */
    private Quiz checkExistenceAndReturn(String quizId) {
        if (quizId == null || quizId.isEmpty()) {
            throw new RuntimeException("Отсутствует ID");
        }
        return quizzesRepository.findById(quizId).orElseThrow(() -> new RuntimeException("Несуществующая викторина"));
    }

    /**
     * Проверяет имеет ли пользователь права на изменение данной викторины
     *
     * @param checking объект викторины, с которой происходит работа
     * @param actor    пользователь, выполняющий действия
     */
    private void checkPermission(Quiz checking, UserInfo actor) {
        if (!(actor.getRole().equals("ADMIN") || checking.getCreatorId() == actor.getId())) {
            throw new RuntimeException("Вы не создавали викторины с ID: " + checking.get_id());
        }
    }
}
