package ru.rkoks.services.editor;

import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.dto.requests.editor.PreviewRequestDto;
import ru.rkoks.domains.models.main.Preview;
import ru.rkoks.security.details.UserInfo;

/**
 * Интерфейс сервиса по работе с анонсами викторин
 */
public interface PreviewService {

    /**
     * Обновляет информацию в указанном анонсе
     *
     * @param preview    анонс, который необходимо обновить
     * @param previewDto информация, которую необходимо вставить/изменить в анонсе
     * @return сохранённый в БД анонс
     */
    Preview updatePreview(Preview preview, PreviewRequestDto previewDto);

    /**
     * Добавляет анонс с необходимой информацией в БД
     *
     * @param previewDto информация для создания анонса
     * @param userInfo   пользователь, создающий викторину
     * @return сохранённый в БД анонс
     */
    Preview addPreview(PreviewRequestDto previewDto, UserInfo userInfo);

    /**
     * Сохраняет файл в хранилище и назначает имя файла в анонсе
     *
     * @param preview  анонс, для которого загружаем файл
     * @param image    изображение для загрузки и сохранения
     * @param userInfo пользователь, добавляющий файл
     * @return сохранённый в БД анонс
     */
    Preview addImage(Preview preview, MultipartFile image, UserInfo userInfo);
}
