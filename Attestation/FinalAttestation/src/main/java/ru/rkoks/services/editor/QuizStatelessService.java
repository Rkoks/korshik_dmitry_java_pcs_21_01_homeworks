package ru.rkoks.services.editor;

import ru.rkoks.domains.dto.requests.editor.QuizRequestDto;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.security.details.UserInfo;

import java.util.List;

/**
 * Интерфейс сервиса по созданию викторин и работе с ними без внесения изменений
 */
public interface QuizStatelessService {
    /**
     * Достаёт все викторины из БД
     *
     * @return список объектов викторин, найденных в БД
     */
    List<Quiz> getAllQuizzes();

    /**
     * Создаёт новую викторину в БД
     *
     * @param quizDto информация от пользователя для создания новой викторины
     * @return объект викторины, сохранённой в БД
     */
    Quiz addQuiz(QuizRequestDto quizDto);

    /**
     * Достаёт из БД запрашиваемую викторину для указанного пользователя
     *
     * @param quizId   идентификатор викторины для поиска
     * @param userInfo пользователь, производящий операцию для проверки прав доступа
     * @return объект викторины, найденной в БД
     */
    Quiz getQuiz(String quizId, UserInfo userInfo);

    /**
     * Достаёт из БД все викторины, созданные указанным пользователем
     *
     * @param userInfo пользователь, для которого необходимо найти викторины
     * @return список объектов викторин, найденных в БД
     */
    List<Quiz> getUserQuizzes(UserInfo userInfo);


}
