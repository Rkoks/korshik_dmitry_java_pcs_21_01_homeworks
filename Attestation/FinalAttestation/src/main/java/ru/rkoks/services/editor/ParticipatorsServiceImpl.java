package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.models.main.Participators;
import ru.rkoks.repositories.jpa.ParticipatorsRepository;
import ru.rkoks.security.details.UserInfo;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class ParticipatorsServiceImpl implements ParticipatorsService {
    private final ParticipatorsRepository participatorsRepository;

    @Override
    public Participators createParticipators(UserInfo creator) {

        return Participators.builder()
                .creatorId(creator.getId())
                .usersWillGetPart(new ArrayList<>())
                .usersBegin(new ArrayList<>())
                .usersFinished(new ArrayList<>())
                .isDeleted(false)
                .build();

    }

    @Override
    public Participators saveParticipators(Participators participators) {
        return participatorsRepository.save(participators);
    }

    @Override
    public void addToWillingList(Participators participators, Long playerId) {
        participators.getUsersWillGetPart().add(playerId);
        participatorsRepository.save(participators);
    }

    @Override
    public void addToBeginList(Participators participators, Long playerId) {
        participators.getUsersBegin().add(playerId);
        participatorsRepository.save(participators);
    }

    @Override
    public void addToFinishedList(Participators participators, Long playerId) {
        participators.getUsersFinished().add(playerId);
        participatorsRepository.save(participators);

    }

    @Override
    public boolean notInWillingList(Participators participators, Long playerId) {
        return participatorsRepository
                .findBy_idAndUsersWillGetPartContains(participators.get_id(), playerId).isEmpty();
    }

    @Override
    public boolean notInBeginList(Participators participators, Long playerId) {
        return participatorsRepository
                .findBy_idAndUsersBeginContains(participators.get_id(), playerId).isEmpty();
    }

    @Override
    public boolean notInFinishedList(Participators participators, Long playerId) {
        return participatorsRepository
                .findBy_idAndUsersFinishedContains(participators.get_id(), playerId).isEmpty();
    }
}
