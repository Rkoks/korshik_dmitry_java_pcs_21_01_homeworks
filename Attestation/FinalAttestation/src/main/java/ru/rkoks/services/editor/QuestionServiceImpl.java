package ru.rkoks.services.editor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.domains.dto.requests.editor.QuestionRequestDto;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.repositories.jpa.QuestionsRepository;
import ru.rkoks.security.details.UserInfo;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {
    private final QuestionsRepository questionsRepository;
    private final AnswerService answerService;

    @Override
    public List<Question> addQuestions(List<QuestionRequestDto> questionsDto, UserInfo userInfo) {
        List<Question> questions = questionsDto.stream().map(questionDto -> {
            questionDto.setCreator(userInfo);
            questionDto.getAnswerDto().setCreator(userInfo);
            return questionDto.createModel();
        }).collect(Collectors.toList());

        questions.forEach(question -> {
            answerService.addAnswer(question.getAnswer());
            question.setIsDeleted(false);
        });

        return questionsRepository.saveAll(questions);
    }

    @Override
    public void deleteQuestions(List<Question> questions) {
        questions.forEach(question -> {
            answerService.deleteAnswer(question.getAnswer());
            question.setIsDeleted(true);
        });
        questionsRepository.saveAll(questions);
    }

    @Override
    public Question getById(String questionId) {
        return questionsRepository.findById(questionId).get();
    }

    @Override
    public List<Question> getQuestionByIdListForUser(List<String> questionIds, UserInfo userInfo) {
        return questionsRepository.getAllBy_idInAndCreatorIdAndIsDeletedFalse(
                questionIds, userInfo.getId());
    }

}
