package ru.rkoks.services.editor;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

//TODO удалить
public interface FileStorageService {
    void init();

    String uploadFile(MultipartFile file);

    Resource loadFile(String filename);


}
