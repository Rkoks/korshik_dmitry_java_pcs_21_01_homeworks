package ru.rkoks.services.game;

import ru.rkoks.domains.dto.requests.game.UserAnswerDto;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.models.statistics.QuizStatistic;

import java.util.Optional;

/**
 * Интерфейс сервиса для работы с объектами статистики
 */
public interface StatisticService {
    /**
     * Создаёт объект статистики для пользователя по выбранной викторине при подписке на участие
     *
     * @param userAnswer запрос от пользователя с указанием необходимой информации для текущего этапа прохождения
     * @param quiz       запрашиваемая викторина
     * @return объект сформированной статистики
     */
    QuizStatistic createStatisticBySubscribe(UserAnswerDto userAnswer, Quiz quiz);

    /**
     * Обновляет данные статистики при начале участия в ней
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @param quiz      запрашиваемая викторина
     * @return обновлённый объект статистики
     */
    QuizStatistic updateStatisticByStart(QuizStatistic statistic, Quiz quiz);

    /**
     * Достаёт из БД статистику по запросу пользователя
     *
     * @param userAnswer запрос от пользователя с указанием необходимой информации для текущего этапа прохождения
     * @return объект статистики при её наличии или пустой опшионал при отсутствии
     */
    Optional<QuizStatistic> getStatistic(UserAnswerDto userAnswer);

    /**
     * Сохраняет объект статистики в БД
     *
     * @param statistic объект хранящий статистику по текущей для пользователя викторине
     * @return сохранённый объект в БД
     */
    QuizStatistic saveStatistic(QuizStatistic statistic);

    /**
     * Проверяет ответ пользователя на правильность
     *
     * @param userAnswer запрос от пользователя с выбранным ответом на вопрос
     * @return количество заработанных за ответ баллов
     */
    Integer checkUserAnswer(UserAnswerDto userAnswer);
}
