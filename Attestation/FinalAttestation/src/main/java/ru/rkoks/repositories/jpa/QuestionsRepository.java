package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rkoks.domains.models.main.Question;

import java.util.List;

public interface QuestionsRepository extends MongoRepository<Question, String> {

    List<Question> getAllBy_idInAndCreatorIdAndIsDeletedFalse(List<String> questionIds, Long creatorId);


}
