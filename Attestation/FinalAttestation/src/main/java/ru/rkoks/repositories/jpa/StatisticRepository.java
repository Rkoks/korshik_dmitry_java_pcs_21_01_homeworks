package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rkoks.domains.models.statistics.QuizStatistic;

import java.util.Optional;

public interface StatisticRepository extends MongoRepository<QuizStatistic, String> {
    Optional<QuizStatistic> findByQuizIdAndPlayerId(String quizId, Long playerId);

}

