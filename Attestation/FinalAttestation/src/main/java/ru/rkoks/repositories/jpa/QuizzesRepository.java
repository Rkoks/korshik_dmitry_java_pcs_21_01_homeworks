package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rkoks.domains.models.main.Quiz;

import java.util.List;
import java.util.Optional;

public interface QuizzesRepository extends MongoRepository<Quiz, String> {

    List<Quiz> findAllByCreatorIdAndIsDeletedFalse(Long creatorId);

    Optional<Quiz> findBy_idAndCreatorId(String quizId, Long creatorId);

    Optional<Quiz> findBy_idAndIsDeletedFalse(String quizId);

    List<Quiz> findAllByStateNotAndIsDeletedFalse(Quiz.State state);

    List<Quiz> findAllByStateInAndIsDeletedFalse(List<Quiz.State> states);
}
