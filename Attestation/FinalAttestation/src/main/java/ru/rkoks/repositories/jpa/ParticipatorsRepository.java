package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rkoks.domains.models.main.Participators;

import java.util.Optional;

public interface ParticipatorsRepository extends MongoRepository<Participators, String> {
    Optional<Participators> findBy_idAndUsersWillGetPartContains(String participatorsId, Long playerId);

    Optional<Participators> findBy_idAndUsersBeginContains(String participatorsId, Long playerId);

    Optional<Participators> findBy_idAndUsersFinishedContains(String participatorsId, Long playerId);
}
