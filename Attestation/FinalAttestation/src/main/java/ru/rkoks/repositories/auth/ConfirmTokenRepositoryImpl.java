package ru.rkoks.repositories.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Repository
@RequiredArgsConstructor
public class ConfirmTokenRepositoryImpl implements ConfirmTokenRepository {
    private final RedisTemplate<String, String> redisTemplate;
    private static final String CONFIRM_PREFIX = "CNF_";

    @Override
    public void saveToConfirm(String token, Long userId) {
        redisTemplate.opsForValue().set(CONFIRM_PREFIX + token, userId.toString(), 7, TimeUnit.DAYS);
    }

    @Override
    public boolean existsInConfirm(String token) {
        Boolean hasToken = redisTemplate.hasKey(CONFIRM_PREFIX + token);
        return hasToken != null && hasToken;
    }

    @Override
    public Long findInConfirm(String token) {
        Long result = Long.parseLong(Objects.requireNonNull(redisTemplate.opsForValue().get(CONFIRM_PREFIX + token)));
        redisTemplate.delete(CONFIRM_PREFIX + token);
        return result;
    }
}
