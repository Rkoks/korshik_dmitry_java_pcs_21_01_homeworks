package ru.rkoks.repositories.auth;

public interface ConfirmTokenRepository {
    void saveToConfirm(String token, Long userId);

    boolean existsInConfirm(String token);

    Long findInConfirm(String token);
}
