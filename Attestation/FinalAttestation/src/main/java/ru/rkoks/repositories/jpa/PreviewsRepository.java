package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rkoks.domains.models.main.Preview;

public interface PreviewsRepository extends MongoRepository<Preview, String> {

}
