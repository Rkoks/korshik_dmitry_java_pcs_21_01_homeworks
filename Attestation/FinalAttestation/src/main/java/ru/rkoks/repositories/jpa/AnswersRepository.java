package ru.rkoks.repositories.jpa;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ru.rkoks.domains.models.main.Answer;

import java.util.Optional;

public interface AnswersRepository extends MongoRepository<Answer, String> {
}
