package ru.rkoks.domains.forms;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * Форма аутентификации
 */
@Data
public class AuthForm {

    @NotBlank
    @Pattern(regexp = "(\\w|\\.|-)+@(\\w+\\.)+\\w+")
    private String email;
    @NotBlank
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}")
    private String password;
}
