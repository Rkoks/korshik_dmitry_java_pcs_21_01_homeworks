package ru.rkoks.domains.models.main;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.rkoks.domains.utils.ModelView;

/**
 * Модель анонса викторин
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "previews")
@JsonView(ModelView.MainView.class)
public class Preview {
    @Id
    private String _id;
    @JsonView(ModelView.CreatorView.class)
    private Boolean isDeleted;
    @JsonView(ModelView.AdminView.class)
    private Long creatorId;
    private String title;
    @JsonIgnore
    private String imageFilename;
    private String logoDescription;
    private String fullDescription;


}
