package ru.rkoks.domains.dto.requests.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.security.details.UserInfo;

/**
 * Модель ответов пользователей при участии в викторине
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAnswerDto {
    @JsonIgnore
    private UserInfo player;
    @JsonIgnore
    private String quizId;

    private String questionId;

    private String answer;

    private Boolean repeatQuestion;
}
