package ru.rkoks.domains.models.main;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import ru.rkoks.domains.utils.ModelView;

import java.util.List;

/**
 * Модель данных об участниках викторин, содержащая списки участников для различных этапов
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(ModelView.MainView.class)
public class Participators {
    @Id
    private String _id;
    @JsonView(ModelView.CreatorView.class)
    private Boolean isDeleted;
    @JsonView(ModelView.AdminView.class)
    private Long creatorId;
    //Список пользователей записавшихся на участие
    private List<Long> usersWillGetPart;
    //Список пользователей начавших прохождение
    private List<Long> usersBegin;
    //Список пользователей закончивших прохождение
    private List<Long> usersFinished;
}
