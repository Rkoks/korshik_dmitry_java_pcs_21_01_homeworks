package ru.rkoks.domains.utils;

/**
 * Класс для управления де/с-ериализацией моделей из/в контроллер-ов/ы
 */
public class ModelView {
    public interface MainView{} //Показывает основные поля модели
    public interface CreatorView extends MainView{} //Добавляет поля доступные создателю модели
    public interface AdminView extends CreatorView{} //Добавляет поля доступные Админу системы
}
