package ru.rkoks.domains.dto.responses.game;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.models.statistics.QuizStatistic;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuizResponseDto {
    //Эти поля возвращаются всегда
    private String quizId;
    private QuizStatistic.State state;
    private String message;
    //Эти только 1 раз после записи
    private PreviewResponseDto preview;
    private LocalDate beginDate;
    private LocalDate finishDate;
    //Это пока проходится викторина
    private QuestionResponseDto question;
    //Эти для результата
    private Integer maxPoints;
    private Integer resultPoints;
    private Integer pointsToPass;
    private Integer percentsToPass;

    //Результат при начале теста или не последнем ответе
    public static QuizResponseDto mapByAnswered(String quizId, Question question, QuizResponseDto result) {
        result.quizId = quizId;
        result.state = QuizStatistic.State.ANSWERED;
        result.question = QuestionResponseDto.from(question);
        return result;
    }

    //Результат при окончании теста или запросе статистики
    public static QuizResponseDto mapByFinished(QuizStatistic statistic, QuizResponseDto result) {
        result.quizId = statistic.getQuizId();
        result.state = QuizStatistic.State.FINISHED;
        result.maxPoints = statistic.getMaxPoints();
        result.resultPoints = statistic.getCurrentPoints();
        result.pointsToPass = result.maxPoints * statistic.getPercentsToPass() / 100;
        return result;
    }

    //Результат при запросе списка всех идущих или предстоящих викторин
    public static List<QuizResponseDto> mapForShowing(List<Quiz> quizzes) {
        return quizzes.stream().map(quiz -> QuizResponseDto.builder()
                    .quizId(quiz.get_id())
                    .preview(PreviewResponseDto.from(quiz.getPreview()))
                    .beginDate(quiz.getBeginDate())
                    .finishDate(quiz.getFinishDate())
                    .percentsToPass(quiz.getPointsLimitInPercents())
                    .build()
        ).collect(Collectors.toList());
    }



}
