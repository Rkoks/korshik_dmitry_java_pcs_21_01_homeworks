package ru.rkoks.domains.utils;

import org.springframework.util.ReflectionUtils;

/**
 * Служебный класс для работы с моделями
 */
public class ModelUtils {
    /**
     * Вносит изменения в значения полей <code>oldModel</code> на основе не null полей <code>newModel</code>.
     * Обрабатывает любую пару моделей с идентичным типом
     *
     * @param oldModel существующая модель, в которую необходимо внести изменения
     * @param newModel модель с новыми значениями полей
     * @param <T>      тип обрабатываемых моделей
     * @return изменённую модель
     */
    public static <T> T updateModelWithoutNullFields(T oldModel, T newModel) {
        if (oldModel.getClass().equals(newModel.getClass())) {
            ReflectionUtils.doWithFields(oldModel.getClass(), field -> {
                field.setAccessible(true);
                if (field.get(newModel) != null) {
                    field.set(oldModel, field.get(newModel));
                }
            });
        }
        return oldModel;
    }
}
