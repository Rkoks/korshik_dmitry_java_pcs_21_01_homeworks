package ru.rkoks.domains.utils;

/**
 * Класс для управления де/с-ериализацией запросов из/в контроллер-ов/ы
 */
public class RequestView {
    public interface MainView{} //Показывает основные поля тела запроса на создание модели
    public interface WithQuestionsView{} //Предоставляет тело вопросов для добавления
    public interface MainWithQuestionView extends MainView, WithQuestionsView{}
}
