package ru.rkoks.domains.dto.requests.editor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.domains.models.main.Answer;
import ru.rkoks.domains.models.main.Question;
import ru.rkoks.domains.utils.RequestView;
import ru.rkoks.security.details.UserInfo;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(RequestView.WithQuestionsView.class)
public class QuestionRequestDto {
    @JsonIgnore
    protected UserInfo creator;
    private String text;
    @JsonIgnore
    private String imageUrl;
    private AnswerRequestDto answerDto;
    @JsonIgnore
    private Answer answer;

    public Question createModel() {
        return Question.builder()
                .text(text)
                .imageUrl(imageUrl)
                .answer(answerDto.createModel())
                .creatorId(creator.getId())
                .build();
    }
}
