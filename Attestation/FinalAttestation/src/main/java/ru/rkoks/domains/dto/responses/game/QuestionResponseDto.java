package ru.rkoks.domains.dto.responses.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.domains.models.main.Question;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionResponseDto {
    private String id;
    private String text;
    @JsonIgnore
    private String imageUrl;
    private AnswerResponseDto answerDto;

    public static QuestionResponseDto from(Question question) {
        if (question == null) {
            return null;
        }
        return QuestionResponseDto.builder()
                .id(question.get_id())
                .text(question.getText())
                .imageUrl(question.getImageUrl())
                .answerDto(AnswerResponseDto.from(question.getAnswer()))
                .build();
    }
}
