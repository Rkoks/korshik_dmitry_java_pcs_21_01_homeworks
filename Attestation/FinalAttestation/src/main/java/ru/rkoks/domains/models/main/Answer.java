package ru.rkoks.domains.models.main;


import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.rkoks.domains.utils.ModelView;

import java.util.Set;

/**
 * Модель ответов к вопросам с вариантами
 */
@Document(collection = "answers")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonView(ModelView.MainView.class)
public class Answer {
    @Id
    private String _id;
    @JsonView(ModelView.CreatorView.class)
    private Boolean isDeleted;
    @JsonView(ModelView.AdminView.class)
    private Long creatorId;
    private String correctAnswer;
    private Set<String> options;
    protected Integer pointsForCorrectAnswer;

    /**
     * Проверяет ответ введённый пользователем на правильность
     *
     * @param answer ответ пользователя
     * @return количество баллов за ответ
     */
    public Integer checkCorrect(String answer) {
        if (correctAnswer.equalsIgnoreCase(answer)) {
            return pointsForCorrectAnswer;
        }
        return 0;
    }

    /**
     * Проверяет корректность ответов при добавлении к вопросу
     *
     * @return true - в случае корректных данных, иначе false
     */
    public boolean validAnswer() {
        if (pointsForCorrectAnswer == null || pointsForCorrectAnswer <= 0) {
            return false;
        }
        if (options == null || options.size() < 2) {
            return false;
        }
        if (correctAnswer != null && !correctAnswer.isEmpty()) {
            return options.contains(correctAnswer);
        }
        return false;
    }

}

