package ru.rkoks.domains.dto.requests.editor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.models.main.Preview;
import ru.rkoks.domains.utils.RequestView;
import ru.rkoks.security.details.UserInfo;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(RequestView.MainView.class)
public class PreviewRequestDto {
    @JsonIgnore
    protected UserInfo creator;
    private String title;
    private String logoDescription;
    @JsonIgnore
    private MultipartFile file;
    @JsonIgnore
    private String imageFilename;
    private String fullDescription;

    public Preview createModel() {
        return Preview.builder()
                .title(title)
                .logoDescription(logoDescription)
                .imageFilename(imageFilename)
                .fullDescription(fullDescription)
                .creatorId(creator == null ? null : creator.getId())
                .build();
    }

}
