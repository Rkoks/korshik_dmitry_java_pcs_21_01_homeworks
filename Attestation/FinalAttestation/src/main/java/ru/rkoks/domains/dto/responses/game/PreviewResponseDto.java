package ru.rkoks.domains.dto.responses.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import ru.rkoks.domains.models.main.Preview;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PreviewResponseDto {
    @JsonIgnore
    private String id;
    private String title;
    @JsonIgnore
    private String imageFilename;
    @JsonIgnore
    private Resource file;
    private String fullDescription;

    public static PreviewResponseDto from(Preview preview) {
        if (preview == null) {
            return null;
        }
        return PreviewResponseDto.builder()
                .title(preview.getTitle())
                .imageFilename(preview.getImageFilename())
                .fullDescription(preview.getFullDescription())
                .build();
    }
}
