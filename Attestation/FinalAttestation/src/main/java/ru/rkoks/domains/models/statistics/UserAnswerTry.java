package ru.rkoks.domains.models.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Ответ пользователя на конкретный вопрос для сохранения в статистике
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAnswerTry {
    private String questionId;
    private String answer;
}
