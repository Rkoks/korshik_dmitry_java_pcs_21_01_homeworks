package ru.rkoks.domains.models.main;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.rkoks.domains.utils.ModelView;

import java.time.LocalDate;
import java.util.List;

/**
 * Модель викторин
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "quizzes")
@JsonView(ModelView.MainView.class)
public class Quiz {
    @Id
    private String _id;
    @JsonView(ModelView.CreatorView.class)
    private Boolean isDeleted;
    @JsonView(ModelView.AdminView.class)
    private Long creatorId;
    @DBRef
    private Preview preview;
    @DBRef
    private List<Question> questions;
    private Integer pointsLimitInPercents;
    private State state;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate beginDate;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate finishDate;
    @DBRef
    private Participators participators;


    public enum State {
        /**
         * Мероприятие запланировано, но ещё не началось
         */
        AWAIT,
        /**
         * Мероприятие уже идёт
         */
        ACTIVE,
        /**
         * Мероприятие завершилось
         */
        FINISH,
        /**
         * Мероприятие скрыто от всех участников
         */
        HIDDEN,
        /**
         * Служебное состояние для снятия HIDDEN
         */
        NONE
    }
}
