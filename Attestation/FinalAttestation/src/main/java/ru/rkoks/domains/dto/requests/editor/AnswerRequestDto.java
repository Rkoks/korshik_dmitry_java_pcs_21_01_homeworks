package ru.rkoks.domains.dto.requests.editor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import ru.rkoks.domains.models.main.Answer;
import ru.rkoks.domains.utils.RequestView;
import ru.rkoks.security.details.UserInfo;

import java.util.Set;

@JsonView(RequestView.WithQuestionsView.class)
@Data
public class AnswerRequestDto {
    @JsonIgnore
    protected UserInfo creator;
    private Integer pointsForCorrectAnswer;
    private String correctAnswer;
    private Set<String> options;

    public Answer createModel() {
        return Answer.builder()
                .correctAnswer(correctAnswer)
                .options(options)
                .pointsForCorrectAnswer(pointsForCorrectAnswer)
                .creatorId(creator.getId())
                .build();
    }

}
