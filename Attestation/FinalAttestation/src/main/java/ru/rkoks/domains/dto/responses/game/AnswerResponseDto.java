package ru.rkoks.domains.dto.responses.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.domains.models.main.Answer;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnswerResponseDto {
    @JsonIgnore
    private String id;
    private Set<String> options;

    public static AnswerResponseDto from(Answer answer) {
        if (answer == null) {
            return null;
        }

        return AnswerResponseDto.builder()
                .id(answer.get_id())
                .options(answer.getOptions())
                .build();
    }
}
