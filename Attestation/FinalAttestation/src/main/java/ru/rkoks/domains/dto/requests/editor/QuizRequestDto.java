package ru.rkoks.domains.dto.requests.editor;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.domains.models.main.Participators;
import ru.rkoks.domains.models.main.Quiz;
import ru.rkoks.domains.utils.RequestView;
import ru.rkoks.security.details.UserInfo;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(RequestView.MainView.class)
public class QuizRequestDto {
    @JsonIgnore
    protected UserInfo creator;
    private PreviewRequestDto previewDto;
    @JsonView(RequestView.WithQuestionsView.class)
    private List<QuestionRequestDto> questionsDto;
    private Integer pointsLimitInPercents;
    @JsonIgnore
    private Quiz.State state;
    @JsonIgnore
    private Participators participators;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate finishDate;

    public Quiz createModel() {
        return Quiz.builder()
                .preview(previewDto.createModel())
                .questions(questionsDto.stream().map(QuestionRequestDto::createModel).collect(Collectors.toList()))
                .pointsLimitInPercents(pointsLimitInPercents)
                .state(state)
                .participators(participators)
                .beginDate(beginDate)
                .finishDate(finishDate)
                .creatorId(creator.getId())
                .build();
    }

    public Quiz createShortModel() {
        return Quiz.builder()
                .pointsLimitInPercents(pointsLimitInPercents)
                .beginDate(beginDate)
                .finishDate(finishDate)
                .creatorId(creator.getId())
                .build();
    }
}
