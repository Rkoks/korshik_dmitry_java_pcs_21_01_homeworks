package ru.rkoks.domains.models.statistics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Сохраняемая статистика по прохождению каждой отдельной викторины каждым пользователем
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "statistics")
public class QuizStatistic {
    @Id
    private String _id;
    private Long playerId;
    private String quizId;
    private State state;
    private String nextQuestionId;
    private List<UserAnswerTry> answerTries;
    private List<String> questionsWithoutAnswerIdList;
    private Integer maxPoints;
    private Integer currentPoints;
    private Integer percentsToPass;

    public enum State {
        /**
         * Записался на прохождение викторины
         */
        SUBSCRIBED,
        /**
         * Приступил к прохождению викторины
         */
        ANSWERED,
        /**
         * Закончил прохождение викторины
         */
        FINISHED
    }
}
