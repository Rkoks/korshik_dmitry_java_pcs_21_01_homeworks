package ru.rkoks.domains.models.main;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.rkoks.domains.utils.ModelView;

/**
 * Модель вопросов в викторинах
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "questions")
@JsonView(ModelView.MainView.class)
public class Question {
    @Id
    private String _id;
    @JsonView(ModelView.CreatorView.class)
    private Boolean isDeleted;
    @JsonView(ModelView.AdminView.class)
    private Long creatorId;
    private String text;
    @JsonIgnore
    private String imageUrl;
    @DBRef
    private Answer answer;


}
