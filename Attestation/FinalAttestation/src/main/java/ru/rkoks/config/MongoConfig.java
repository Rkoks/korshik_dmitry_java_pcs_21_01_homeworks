package ru.rkoks.config;

import com.mongodb.client.MongoClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = "ru.rkoks.repositories")
@RequiredArgsConstructor
public class MongoConfig {
    private final MongoClient mongoClients;
    @Value("spring.data.mongodb.database")
    private String dbName;

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClients, dbName);
    }
}
