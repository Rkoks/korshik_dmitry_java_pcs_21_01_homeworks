package ru.rkoks.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class Config {

    /**
     * Создаёт бин шаблона письма для подтверждения Email адреса
     *
     * @return
     */
    @Bean
    public SimpleMailMessage templateSimpleMessage() {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setText("<html><body>Вы зарегистрировались на сайте kvanto-quiz.ru! <br>" +
                "Для получения доступа к функциям сайта вам необходимо подтвердить свой Email.<br>" +
                "Для этого перейдите по следующей ссылке: <a href=localhost/api/auth/email?token=%s>Подтвердить</a></body></html>");

        return message;
    }

    @Bean
    public OpenAPI openAPI(){
        return new OpenAPI().components(new Components())
                .info(new Info().title("Кванто-Квиз OpenAPI")
                        .version("v0.1")
                        .description("REST API документация к сервису проведения онлайн викторин" +
                                " и квизов \"Кванто-Квиз\""));
    }
}
