import exceptions.BadEmailException;
import exceptions.BadPasswordException;
import exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserManager implements UserService {

    List<User> users = new ArrayList<>();


    @Override
    public void signUp(String email, String password) throws BadEmailException, BadPasswordException{
        if (checkEmail(email)) {
            if (isRegistered(email) < 0) {
                if (checkPassword(password)) {
                    User newUser = new User(email, password);
                    users.add(newUser);
                    System.out.println("Registered successful");
                } else {
                    throw new BadPasswordException("Неверный формат пароля");
                }
            } else {
                System.out.println("Email already exists");
            }
        } else {
            throw new BadEmailException("Неверный формат Email адреса");
        }

    }

    @Override
    public void signIn(String email, String password) throws UserNotFoundException{
        if (checkEmail(email)) {
            int userIndex = isRegistered(email);
            if (userIndex >= 0) {
                User currentUser = users.get(userIndex);
                if (currentUser.getPassword().equals(password)) {
                    System.out.println("Login successful");
                } else {
                    System.out.println("Wrong password");
                }
            } else {
                throw new UserNotFoundException("Пользователь не зарегистрирован");
            }
        }
    }

    private int isRegistered(String checkingEmail) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getEmail().equals(checkingEmail)) {
                return i;
            }
        }
        return -1;
    }

    private boolean checkEmail(String email) {
        return email.contains("@");
    }

    private boolean checkPassword(String password) {
        return password.length() > 7 && password.matches("(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9]+");

    }
}
