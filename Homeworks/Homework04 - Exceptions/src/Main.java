import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UserManager userManager = new UserManager();

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Введите 'up' для регистрации, 'in' для аутентификации или всё что угодня для завершения работы");
            int operation = getOperationId(sc.nextLine());
            if (operation == -1) {
                break;
            }

            String email = sc.nextLine();
            String password = sc.nextLine();

            if (operation == 0) {
                userManager.signUp(email, password);
            } else {
                userManager.signIn(email, password);
            }

        }

    }

    public static int getOperationId(String string) {
        if (string.toLowerCase().equals("up")) {
            return 0;
        } else if (string.toLowerCase().equals("in")) {
            return 1;
        }
        return -1;

    }
}
