package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.dto.LessonsResponse;
import ru.rkoks.models.Course;
import ru.rkoks.models.Lesson;
import ru.rkoks.repositories.CoursesRepository;
import ru.rkoks.repositories.LessonsRepository;

import java.util.List;

import static ru.rkoks.dto.CourseDto.from;
import static ru.rkoks.dto.LessonDto.from;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {
    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public List<CourseDto> getCourses() {
        return from(coursesRepository.findAllByIsDeletedIsNull());
    }

    @Override
    public CourseDto addCourse(CourseDto course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .build();
        coursesRepository.save(newCourse);
        return from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseDto course) {
        Course existingCourse = coursesRepository.getById(courseId);
        existingCourse.setTitle(course.getTitle());
        coursesRepository.save(existingCourse);
        return from(existingCourse);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course course = coursesRepository.getById(courseId);
        course.setIsDeleted(true);
        coursesRepository.save(course);
    }

    @Override
    public LessonsResponse addLessonToCourse(Long courseId, LessonDto lesson) {
        Course course = coursesRepository.getById(courseId);
        Lesson existingLesson = lessonsRepository.getById(lesson.getId());
        existingLesson.setCourse(course);
        lessonsRepository.save(existingLesson);
        return LessonsResponse.builder()
                .data(from(course.getLessons()))
                .build();
    }

    @Override
    public LessonsResponse deleteLessonFromCourse(Long courseId, LessonDto lesson) {
        Lesson existingLesson = lessonsRepository.getById(lesson.getId());
        existingLesson.setCourse(null);
        lessonsRepository.save(existingLesson);
        return LessonsResponse.builder()
                .data(from(coursesRepository.getById(courseId).getLessons()))
                .build();
    }
}
