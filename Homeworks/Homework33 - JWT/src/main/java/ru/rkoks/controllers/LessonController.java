package ru.rkoks.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.dto.LessonsResponse;
import ru.rkoks.services.LessonService;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/lessons")
public class LessonController {
    private final LessonService lessonService;

    @GetMapping
    public ResponseEntity<LessonsResponse> getLessons() {
        return ResponseEntity.ok()
                .body(LessonsResponse.builder()
                        .data(lessonService.getLessons())
                        .build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDto addLesson(@RequestBody LessonDto lesson) {
        return lessonService.addLesson(lesson);
    }

    @PutMapping("/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonDto updateLesson(@PathVariable("lesson-id") Long lessonId,
                                  @RequestBody LessonDto lesson) {
        return lessonService.updateLesson(lessonId, lesson);
    }

    @DeleteMapping("/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonService.deleteLesson(lessonId);
    }
}
