package repositories;

import model.Product;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ProductsRepositoryJdbcImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select id, name, quantity, price from product where id = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name, quantity, price from product order by id limit ? offset ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into product (name, quantity, price) VALUES (?, ?, ?)";
    //language=SQL
    private static final String SQL_UPDATE = "update product set name = ?, quantity = ?, price = ? where id = ?";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = ?";
    //language=SQL
    private static final String SQL_DELETE_PRODUCT = "delete from product where name = ? and quantity = ? and price = ?";

    private final DataSource dataSource;

    /**
     * Преобразует результат запроса из базы данных в объект модели Product
     */
    private static final Function<ResultSet, Product> productMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            Integer quantity = resultSet.getObject("quantity", Integer.class);
            Double price = resultSet.getObject("price", Double.class);

            return Product.builder()
                    .withId(id)
                    .withName(name)
                    .withQuantity(quantity)
                    .withPrice(price)
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public ProductsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Выполняет запрос в базу данных на поиск продукта по его id
     * @param productId id, который необходимо найти в БД
     * @return возвращает <code>Optional</code> с объектом Product или пустой (в случае отсутствия id)
     */
    @Override
    public Optional<Product> findById(Long productId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, productId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(productMapper.apply(resultSet));
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Выполняет select на все продукты в БД с учётом limit <code>size</code> и offset <code>page * size</code>
     * сортировка ASC по значению id
     * @param page номер группы строк
     * @param size размер одной группы строк
     * @return список продуктов <code>ArrayList of Product</code>
     */
    @Override
    public List<Product> findAll(int page, int size) {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            statement.setInt(1, size);
            statement.setInt(2, size * page);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    products.add(productMapper.apply(resultSet));
                }
                return products;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * выполняет INSERT продукта с заданными полями, id назначается БД автоматически
     * @param product новый продукт, который необходимо вставить в таблицу БД
     */
    @Override
    public void save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getQuantity());
            statement.setDouble(3, product.getPrice());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            }

            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                product.setId(resultSet.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Обновляет данные о продукте в БД, если продукта с указанным id нет - выкидывает исключение
     * @param product продукт, с изменёнными данными
     */
    @Override
    public void update(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getQuantity());
            statement.setDouble(3, product.getPrice());
            statement.setLong(4, product.getId());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't update product");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Удаляет продукт <code>product</code> из БД при соответствии
     * полей <code>name</code>, <code>quantity</code> и <code>price</code>
     * @param product продукт, который необходимо удалить
     */
    @Override
    public void delete(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PRODUCT)) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getQuantity());
            statement.setDouble(3, product.getPrice());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete product. Affected rows = " + affectedRows);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * удаляет продукт из БД по его id
     * @param id продукта, который необходимо удалить
     */
    @Override
    public void deleteById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, id);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete product");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
