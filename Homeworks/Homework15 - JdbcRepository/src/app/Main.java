package app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import model.Product;
import repositories.ProductsRepository;
import repositories.ProductsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.util.Optional;

public class Main {

    public static final String DB_USER = "postgres";
    public static final String DB_PASS = "123";
    public static final String DB_URL = "jdbc:postgresql://localhost:5432/online_shop";
    public static final String POSTGRESQL_DRIVER = "org.postgresql.Driver";

    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setUsername(DB_USER);
        config.setPassword(DB_PASS);
        config.setJdbcUrl(DB_URL);
        config.setDriverClassName(POSTGRESQL_DRIVER);
        config.setMaximumPoolSize(5);

        DataSource dataSource = new HikariDataSource(config);

        ProductsRepository repo = new ProductsRepositoryJdbcImpl(dataSource);

//        System.out.println(repo.findById(11L));

//        System.out.println(repo.findAll(0, 2));

//        Product product = Product.builder()
//                .withName("Пепси")
//                .withQuantity(24)
//                .withPrice(68.2)
//                .build();
//        repo.save(product);
//        System.out.println(product);

//        Optional<Product> productOpt = repo.findById(2L);
//        Product product = null;
//        if (productOpt.isPresent()) {
//            product = productOpt.get();
//            System.out.println(product);
//            product.setQuantity(20);
//            product.setName("Кефир");
//            repo.update(product);
//        } else {
//            System.out.println("Продукт не найден");
//        }

//        repo.deleteById(13L);

//        Optional<Product> productOpt = repo.findById(12L);
//        Product product = null;
//        if (productOpt.isPresent()) {
//            product = productOpt.get();
//            System.out.println(product);
//            repo.delete(product);
//        } else {
//            System.out.println("Продукт не найден");
//        }

    }
}
