package ru.rkoks.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.services.LessonService;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
@DisplayName("LessonController is working when")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class LessonControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LessonService lessonService;

    @BeforeEach
    public void setUp() {
        when(lessonService.getLessons()).thenReturn(Collections.singletonList(
                LessonDto.builder()
                        .id(1L)
                        .name("Data JPA")
                        .build()
        ));

        when(lessonService.addLesson(LessonDto.builder()
                .name("New Lesson")
                .build()))
                .thenReturn(LessonDto.builder()
                        .id(3L)
                        .name("New Lesson")
                        .build());

        when(lessonService.updateLesson(2L, LessonDto.builder()
                .name("Spring Test")
                .build()))
                .thenReturn(LessonDto.builder()
                        .id(2L)
                        .name("Spring Test")
                        .build());

        when(lessonService.deleteLesson(2L)).thenReturn(LessonDto.builder()
                .id(2L)
                .name("JUnit 5")
                .build());


    }

    @Test
    public void getLessons() throws Exception {
        mockMvc.perform(get("/api/lessons"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data[0].id", is(1)))
                .andExpect(jsonPath("$.data[0].name", is("Data JPA")));
    }


    @Test
    void addLesson() throws Exception {
        mockMvc.perform(post("/api/lessons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"New Lesson\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.name", is("New Lesson")));
    }

    @Test
    void updateLesson() throws Exception {
        mockMvc.perform(put("/api/lessons/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Spring Test\"}"))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("Spring Test")));
    }

    @Test
    void deleteLesson() throws Exception {
        mockMvc.perform(delete("/api/lessons/2"))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("JUnit 5")));
    }
}