package ru.rkoks.services;

import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.CoursesResponse;
import ru.rkoks.dto.StudentDto;

import java.util.List;

public interface StudentService {
    List<StudentDto> getStudents(int page, int size);

    StudentDto addStudent(StudentDto student);

    StudentDto updateStudent(Long studentId, StudentDto student);

    void deleteStudent(Long studentId);

    CoursesResponse addCourseToStudent(Long studentId, CourseDto course);
}
