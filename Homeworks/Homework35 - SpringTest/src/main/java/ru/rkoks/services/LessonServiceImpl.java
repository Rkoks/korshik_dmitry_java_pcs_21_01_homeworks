package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.models.Lesson;
import ru.rkoks.repositories.LessonsRepository;

import java.util.List;

import static ru.rkoks.dto.LessonDto.from;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final LessonsRepository lessonsRepository;

    @Override
    public List<LessonDto> getLessons() {
        return from(lessonsRepository.findAllByIsDeletedIsNull());
    }

    @Override
    public LessonDto addLesson(LessonDto lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .build();
        lessonsRepository.save(newLesson);
        return from(newLesson);
    }

    @Override
    public LessonDto updateLesson(Long lessonId, LessonDto lesson) {
        Lesson existingLesson = lessonsRepository.getById(lessonId);
        existingLesson.setName(lesson.getName());
        lessonsRepository.save(existingLesson);
        return from(existingLesson);
    }

    @Override
    public LessonDto deleteLesson(Long lessonId) {
        Lesson lesson = lessonsRepository.getById(lessonId);
        lesson.setIsDeleted(true);
        lessonsRepository.save(lesson);
        return from(lesson);
    }
}
