package ru.rkoks.services;

import ru.rkoks.dto.LessonDto;

import java.util.List;

public interface LessonService {
    List<LessonDto> getLessons();

    LessonDto addLesson(LessonDto lesson);

    LessonDto updateLesson(Long lessonId, LessonDto lesson);

    LessonDto deleteLesson(Long lessonId);

}
