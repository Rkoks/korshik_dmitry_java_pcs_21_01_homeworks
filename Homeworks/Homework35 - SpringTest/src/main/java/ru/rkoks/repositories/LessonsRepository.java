package ru.rkoks.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rkoks.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByIsDeletedIsNull();

}
