package ru.rkoks.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.rkoks.models.MethodsCallStatistic;

import javax.transaction.Transactional;
import java.util.Optional;


public interface MethodsCallStatisticRepository extends JpaRepository<MethodsCallStatistic, String> {
    Optional<MethodsCallStatistic> getByName(String name);

    @Transactional
    @Modifying
    @Query("update method " +
            "set callCount = callCount + 1 " +
            "where name = ?1")
    int updateCallCountById(String name);

}
