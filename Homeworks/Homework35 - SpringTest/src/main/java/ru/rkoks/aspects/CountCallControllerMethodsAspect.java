package ru.rkoks.aspects;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import ru.rkoks.models.MethodsCallStatistic;
import ru.rkoks.repositories.MethodsCallStatisticRepository;

import java.util.Optional;

@Aspect
@Component
@RequiredArgsConstructor
public class CountCallControllerMethodsAspect {

    private final MethodsCallStatisticRepository methodsCallStatisticRepository;

    @Before(value = "within(ru.rkoks.controllers.*)")
    public void CountBeforeExecution(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        String fullMethodName = signature.getDeclaringTypeName() + "." + signature.getName();

        Optional<MethodsCallStatistic> statistic = methodsCallStatisticRepository.getByName(fullMethodName);
        statistic.ifPresentOrElse(
                methodsCallStatistic -> methodsCallStatisticRepository.updateCallCountById(methodsCallStatistic.getName()),
                () -> methodsCallStatisticRepository.save(MethodsCallStatistic.builder()
                        .name(fullMethodName)
                        .callCount(1L)
                        .build()));

    }
}
