package ru.rkoks.listeners;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import java.io.IOException;
import java.util.Properties;

@WebListener
public class AppContextServletListener implements ServletContextListener {
    private HikariDataSource dataSource;

    /**
     * Инициализируем и сохраняем в контекст сервлетов необходимые для работы объекты с репозиторием
     * @param sce событие появления контекста сервлетов
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("database/application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setDriverClassName(properties.getProperty("db.driver-class-name"));
        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.max-pool-size")));

        this.dataSource = new HikariDataSource(config);

        servletContext.setAttribute("dataSource", dataSource);
        servletContext.setAttribute("storagePath", properties.getProperty("storage.path"));
    }

    /**
     * "Уничтожаем" объекты, переданные в контекст сервлетов, для работы с БД
     * @param sce событие завершения работы контекста сервлетов
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        dataSource.close();
    }
}
