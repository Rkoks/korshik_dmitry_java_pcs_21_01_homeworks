package ru.rkoks.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import ru.rkoks.dto.FileDto;
import ru.rkoks.repositories.FileInfoRepository;
import ru.rkoks.repositories.FileInfoRepositoryJdbcImpl;
import ru.rkoks.services.FilesService;
import ru.rkoks.services.FilesServiceImpl;
import ru.rkoks.services.ProfileServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

// GET /files?fileName=7d48959f-5258-49a2-92df-8cbba0213505.png
@WebServlet("/files")
public class FilesServlet extends HttpServlet {
    private FilesService filesService;

    private ObjectMapper objectMapper;

    /**
     * Инициализируем сервис работы с данными
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));

        this.objectMapper = new ObjectMapper();
    }

    //загружаем файл при наличии параметра в запросе, иначе открываем сервлет со списком файлов
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storageFileName = request.getParameter("fileName");
        if (storageFileName == null) {
            request.getRequestDispatcher("jsp/files.jsp").forward(request, response);
        } else {
            loadFile(storageFileName, response);
        }

    }

    //загрузка файла на сервер
    private void loadFile(String storageFileName, HttpServletResponse response) throws IOException {
        FileDto file = filesService.getFile(storageFileName);

        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        String newName = new String(file.getOriginalFileName().getBytes(), StandardCharsets.ISO_8859_1);
        response.setHeader("Content-Disposition", "filename=\"" + newName + "\"");
        filesService.writeFile(file, response.getOutputStream());

        response.flushBuffer();
    }

    //запрос на поиск файлов
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = request.getReader().readLine();
        fileName = fileName == null ? "" : fileName;

        displayFiles(request, response, fileName);
    }

    //передать файлы для отображения на странице
    private void displayFiles(HttpServletRequest request, HttpServletResponse response, String fileName) throws IOException {
        HttpSession session = request.getSession(false);
        Long authUserId = (Long) session.getAttribute(ProfileServiceImpl.DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME);

        List<FileDto> filesDto = filesService.searchByOriginalFileName(authUserId, fileName);

        response.getWriter().println(objectMapper.writeValueAsString(filesDto));
    }
}
