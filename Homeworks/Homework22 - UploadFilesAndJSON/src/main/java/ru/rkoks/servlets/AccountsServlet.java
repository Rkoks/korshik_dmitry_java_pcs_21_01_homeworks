package ru.rkoks.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.repositories.AccountsRepository;
import ru.rkoks.repositories.AccountsRepositoryJdbcImpl;
import ru.rkoks.services.AccountsService;
import ru.rkoks.services.AccountsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts")
public class AccountsServlet extends HttpServlet {
    private AccountsService accountsService;

    /**
     * Инициализируем сервис работы с данными
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryJdbcImpl(dataSource);

        this.accountsService = new AccountsServiceImpl(accountsRepository);
    }

    //отдаём все зарегистрированные аккаунты
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AccountDto> accounts = accountsService.getAll();
        request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("jsp/accounts.jsp").forward(request, response);
    }
}
