package ru.rkoks.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import ru.rkoks.dto.FileDto;
import ru.rkoks.repositories.FileInfoRepository;
import ru.rkoks.repositories.FileInfoRepositoryJdbcImpl;
import ru.rkoks.services.FilesService;
import ru.rkoks.services.FilesServiceImpl;
import ru.rkoks.services.ProfileServiceImpl;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {
    private FilesService filesService;

    /**
     * Инициализируем сервис работы с данными
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
    }

    //передаём запрос на сервлет с отображением страницы загрузки файлов
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/filesUpload.jsp").forward(request, response);
    }

    //загружаем файл на сервер со всеми необходимыми данными
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader descriptionReader =
                new BufferedReader(new InputStreamReader(request.getPart("description").getInputStream(), StandardCharsets.UTF_8));
        Part filePart = request.getPart("file");

        Long authUserId = (Long) request.getSession().getAttribute(ProfileServiceImpl.DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME);
        String submittedFileName = new String(filePart.getSubmittedFileName().getBytes());
        FileDto form = FileDto.builder()
                .description(descriptionReader.readLine())
                .originalFileName(submittedFileName)
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .fileStream(filePart.getInputStream())
                .uploaderId(authUserId)
                .build();

        filesService.upload(form);
        response.sendRedirect("files");
    }
}
