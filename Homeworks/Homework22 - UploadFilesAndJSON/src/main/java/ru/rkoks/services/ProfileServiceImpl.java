package ru.rkoks.services;

import ru.rkoks.dto.AccountDto;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.Optional;

public class ProfileServiceImpl implements ProfileService {
    public static final String DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME = "accountId";

    private final AccountsRepository accountsRepository;

    public ProfileServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Достаём данные аккаунта пользователя по <code>id</code>
     * @param id идентификатор пользователя
     * @return информация о пользователе, доступная клиенту
     */
    @Override
    public AccountDto getProfileData(Long id) {
        Optional<Account> accountOptional = accountsRepository.findById(id);

        return accountOptional
                .map(account -> AccountDto.builder()
                        .id(account.getId())
                        .firstName(account.getFirstName())
                        .lastName(account.getLastName())
                        .build())
                .orElse(null);
    }
}
