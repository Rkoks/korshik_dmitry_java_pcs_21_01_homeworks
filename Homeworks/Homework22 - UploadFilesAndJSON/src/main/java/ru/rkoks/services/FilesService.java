package ru.rkoks.services;

import ru.rkoks.dto.FileDto;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    void upload(FileDto form);

    void setStoragePath(String path);

    FileDto getFile(String storageFileName);

    void writeFile(FileDto file, OutputStream output);

    List<FileDto> searchByOriginalFileName(Long uploaderId, String originalFileName);
}
