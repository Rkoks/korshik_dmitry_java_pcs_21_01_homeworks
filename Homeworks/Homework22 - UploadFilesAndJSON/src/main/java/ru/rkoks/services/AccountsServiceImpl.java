package ru.rkoks.services;

import ru.rkoks.dto.AccountDto;
import ru.rkoks.repositories.AccountsRepository;

import java.util.List;

import static ru.rkoks.dto.AccountDto.from;

public class AccountsServiceImpl implements AccountsService {
    private AccountsRepository accountsRepository;

    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Получает из репозитория всех зарегистрированных пользователей
     * @return список найденных пользователей с доступной для клиента информацией
     */
    @Override
    public List<AccountDto> getAll() {
        return from(accountsRepository.findAll());
    }

    /**
     * Производит поиск пользователей в репозитории по email
     * @param email строка для поиска по совпадению
     * @return список найденных пользователей с доступной для клиента информацией
     */
    @Override
    public List<AccountDto> searchByEmail(String email) {
        return from(accountsRepository.searchByEmail(email));
    }
}
