package main;

import exceptions.BadEmailException;
import exceptions.BadPasswordException;
import exceptions.UserNotFoundException;
import interfaces.UserService;
import interfaces.UsersRepository;

import java.util.Optional;

/**
 * Отвечает за регистрацию и авторизацию пользователя
 */
public class UserManager implements UserService {


    private final UsersRepository repository;

    public UserManager(UsersRepository repository) {
        this.repository = repository;
    }

    /**
     * реегистрирует нового пользователя (при его отсутствии) после валидации введённых логина и пароля
     * @param email логин должен содержать символ @
     * @param password пароль должен содержать как буквы, так и цифры
     * @throws BadEmailException пробрасывается при некорректно введённом логине (email)
     * @throws BadPasswordException пробрасывается при некорректно введёном пароле
     */
    @Override
    public void signUp(String email, String password) throws BadEmailException, BadPasswordException{
        if (checkEmail(email)) {
            if (!repository.existsByEmail(email)) {
                if (checkPassword(password)) {
                    User newUser = new User(email, password);
                    repository.save(newUser);
                    System.out.println("Registered successful");
                } else {
                    throw new BadPasswordException("Неверный формат пароля");
                }
            } else {
                System.out.println("Email already exists");
            }
        } else {
            throw new BadEmailException("Неверный формат Email адреса");
        }

    }

    /**
     * Аутентифицирует пользователя при его наличии и корректности введёных логина и пароля
     * @param email логин должен содержать символ @
     * @param password пароль должен содержать как буквы, так и цифры
     * @throws UserNotFoundException пробрасывается при отсутствии пользователя в хранилище
     */
    @Override
    public void signIn(String email, String password) throws UserNotFoundException{
        if (checkEmail(email)) {
            Optional<User> optionalUser = repository.findByEmail(email);
            if (optionalUser.isPresent()) {
                User currentUser = optionalUser.get();
                if (currentUser.getPassword().equals(password)) {
                    System.out.println("Login successful");
                } else {
                    System.out.println("Wrong password");
                }
            } else {
                throw new UserNotFoundException("Пользователь не зарегистрирован");
            }
        }
    }


    /**
     * Обновляет пароль пользователя
     * @param email какому логину нужно поменять пароль
     * @param password новый пароль
     */
    @Override
    public void update(String email, String password) {
        if (checkEmail(email)) {
            if (repository.existsByEmail(email)) {
                if (checkPassword(password)) {
                    User user = new User(email, password);
                    repository.update(user);
                    System.out.println("Update successful");
                } else {
                    throw new BadPasswordException("Неверный формат пароля");
                }
            } else {
                System.out.println("Email is not registered");
            }
        } else {
            throw new BadEmailException("Неверный формат Email адреса");
        }
    }

    /**
     * удаляет указанного пользователя после проверки его данных
     * @param email
     * @param password
     */
    @Override
    public void delete(String email, String password) {
        if (checkEmail(email)) {
            if (repository.existsByEmail(email)) {
                if (checkPassword(password)) {
                    User user = new User(email, password);
                    repository.delete(user);
                    System.out.println("Delete successful");
                } else {
                    throw new BadPasswordException("Неверный формат пароля");
                }
            } else {
                System.out.println("Email is not registered");
            }
        } else {
            throw new BadEmailException("Неверный формат Email адреса");
        }
    }

    /**
     * Проверяет введёный логин на валидность
     * @param email проверяемый логин
     * @return true - при наличие символа @, false - при отсутствии символа @
     */
    private boolean checkEmail(String email) {
        return email.contains("@");
    }

    /**
     * Проверяет введённый пароль на валидность
     * @param password проверяемый пароль
     * @return true - больше 7 символов и содержить буквы и цифры, false - иначе
     */
    private boolean checkPassword(String password) {
        return password.length() > 7 && password.matches("(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9]+");

    }
}
