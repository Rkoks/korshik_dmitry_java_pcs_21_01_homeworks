package main;

import impls.IdGeneratorFileImpl;
import impls.UsersRepositoryFileImpl;
import interfaces.IdGenerator;
import interfaces.UserService;
import interfaces.UsersRepository;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        IdGenerator idGenerator = new IdGeneratorFileImpl("users_id.txt");
        UsersRepository repository = new UsersRepositoryFileImpl("users.txt", "users_id.txt", idGenerator);
        UserService service = new UserManager(repository);

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Введите 'up' для регистрации, 'in' для аутентификации, 'ud' для смены пароля, 'de' для удаления или всё что угодня для завершения работы");
            int operation = getOperationId(sc.nextLine());
            if (operation == -1) {
                break;
            }

            String email = sc.nextLine();
            String password = sc.nextLine();

            if (operation == 0) {
                service.signUp(email, password);
            } else if (operation == 1){
                service.signIn(email, password);
            } else if (operation == 2) {
                service.update(email, password);
            } else if (operation == 3) {
                service.delete(email, password);
            }

        }

    }

    /**
     * Преобразует введённую операцию в идентификатор, при неподходящем значении выдаёт код для завершения работы
     * @param string введённая строка операции
     * @return численный код операции
     */
    public static int getOperationId(String string) {
        if (string.equalsIgnoreCase("up")) {
            return 0;
        } else if (string.equalsIgnoreCase("in")) {
            return 1;
        } else if (string.equalsIgnoreCase("ud")) {
            return 2;
        } else if (string.equalsIgnoreCase("de")) {
            return 3;
        }
        return -1;

    }
}
