package interfaces;

import java.util.Iterator;

public interface IdGenerator extends Iterator<Integer> {
    int getCount();

    void setCount(int newCount);

}
