package interfaces;

public interface UserService {
    void signUp(String email, String password);

    void signIn(String email, String password);

    void update(String email, String password);

    void delete(String email, String password);
}
