package impls;

import interfaces.IdGenerator;

import java.io.*;
import java.util.Scanner;

/**
 * Управляет идентификатором последнего добавленного пользователя
 */
public class IdGeneratorFileImpl implements IdGenerator {

    private final String idFilename;

    public IdGeneratorFileImpl(String idFilename) {
        this.idFilename = idFilename;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    /**
     * Считывает идентификатор последнего пользователя, инкрементирует его и сохраняет
     * @return идентификатор нового пользователя
     */
    @Override
    public Integer next() {
        int lastId = getCount();
        lastId++;
        setCount(lastId);
        return lastId;
//        try (Scanner sc = new Scanner(new FileInputStream(idFilename))) {
//            int lastId = sc.nextInt();
//            lastId++;
//            sc.close();
//            try (PrintWriter printWriter = new PrintWriter(new FileWriter(idFilename))) {
//                printWriter.print(lastId);
//            }
//            return lastId;
//        } catch (IOException e) {
//            throw new IllegalArgumentException(e);
//        }
    }

    /**
     * Считывает значение идентификатора последнего пользователя
     * @return количество добавленных пользователей
     */
    @Override
    public int getCount() {
        try (Scanner sc = new Scanner(new FileInputStream(idFilename))) {
            return sc.nextInt();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * записывает в файл новое количество пользователей
     * @param newCount
     */
    @Override
    public void setCount(int newCount) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(idFilename))) {
            writer.write(newCount+"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
