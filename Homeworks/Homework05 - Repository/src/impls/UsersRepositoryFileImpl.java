package impls;

import interfaces.IdGenerator;
import interfaces.UsersRepository;
import main.User;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

/**
 * Управление хранилищем пользователей
 */
public class UsersRepositoryFileImpl implements UsersRepository {
    private final String usersFilename;
    private final String idFilename;
    private final IdGenerator idGenerator;

    public UsersRepositoryFileImpl(String usersFilename, String idFileName, IdGenerator idGenerator) {
        this.usersFilename = usersFilename;
        this.idFilename = idFileName;
        this.idGenerator = idGenerator;
    }

    /**
     * обновляет пароль для указанного пользователя, перезаписывая все данные в новый файл и заменяя им старый
     *
     * @param user , которому необходимо изменить пароль
     */
    @Override
    public void update(User user) {
        UserIterator userIterator = (UserIterator) iterator();
        File tempFile;
        FileWriter fileWriter;

        try {
            tempFile = File.createTempFile("users_temp", ".tmp");
            fileWriter = new FileWriter(tempFile, true);

            while (userIterator.hasNext()) {
                User currentUser = userIterator.next();
                if (currentUser.getEmail().equalsIgnoreCase(user.getEmail())) {
                    currentUser.setPassword(user.getPassword());
                }
                fileWriter.write(currentUser.toString());
            }

            fileWriter.close();

            File originalFile = new File(usersFilename);
            originalFile.delete();
            tempFile.renameTo(originalFile);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * Ищет пользователя по email
     *
     * @param email пользователя, который необходимо проверить на наличие в хранилище
     * @return пользователя в обёртке
     */
    @Override
    public Optional<User> findByEmail(String email) {
        Iterator<User> userIterator = iterator();

        while (userIterator.hasNext()) {
            User user = userIterator.next();
            if (user.getEmail().equalsIgnoreCase(email)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    /**
     * Находит всех зарегистрированных пользователей в хранилище
     *
     * @return список всех пользователей
     */
    @Override
    public List<User> findAll() {
        List<User> userList = new ArrayList<>();

        Iterator<User> userIterator = iterator();

        while (userIterator.hasNext()) {
            userList.add(userIterator.next());
        }

        return userList;
    }

    /**
     * удаляет указанного пользователя, перезаписывая все данные в новый файл и заменяя им старый
     *
     * @param user , которого необходимо удалить
     */
    @Override
    public void delete(User user) {
        UserIterator userIterator = (UserIterator) iterator();
        File tempFile;
        FileWriter fileWriter;

        try {
            tempFile = File.createTempFile("users_temp", ".tmp");
            fileWriter = new FileWriter(tempFile, true);

            int newId = 1;

            while (userIterator.hasNext()) {
                User currentUser = userIterator.next();
                currentUser.setId(newId);

                if (currentUser.getEmail().equalsIgnoreCase(user.getEmail())) {
                    continue;
                }
                fileWriter.write(currentUser.toString());
                newId++;
            }

            fileWriter.close();

            File originalFile = new File(usersFilename);
            originalFile.delete();
            tempFile.renameTo(originalFile);

            idGenerator.setCount(newId - 1);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * сохраняет пользователя в хранилище в формате "id|email|password\n"
     *
     * @param user , которого необходимо добавить
     */
    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(usersFilename, true))) {
            user.setId(idGenerator.next());
            String userAsLine = user.toString();

            writer.write(userAsLine);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * @return общее количество пользователей в хранилеще
     */
    @Override
    public int count() {
        return idGenerator.getCount();
    }

    /**
     * проверяет наличие пользователя в хранилище по @email
     *
     * @param email , по которому необходимо проверить
     * @return true - при наличии, false - при отсутствии
     */
    @Override
    public boolean existsByEmail(String email) {
        Iterator<User> userIterator = iterator();

        while (userIterator.hasNext()) {
            if (userIterator.next().getEmail().equalsIgnoreCase(email)) {
                return true;
            }
        }

        return false;
    }


    /**
     * создаёт экземпляр итератора для прохода по записям хранилища
     *
     * @return экзепляр итератора пользователя
     */
    public Iterator<User> iterator() {
        return new UserIterator();
    }

    /**
     * Отвечает за логику прохода по хранилищу пользователей
     */
    private class UserIterator implements Iterator<User> {
        private long charsForSkip = 0; //отвечает за смещение в файле на следующего пользователя
        private int current = 0;

        @Override
        public boolean hasNext() {
            try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(idFilename)))) {
                return current < scanner.nextInt();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public User next() {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(usersFilename))) {
                bufferedReader.skip(charsForSkip);
                String userAsLine = bufferedReader.readLine();

                charsForSkip += userAsLine.length() + 2;
                current++;

                return getUserByLine(userAsLine);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        /**
         * Преобразует строку в пользователя
         *
         * @param userAsLine строка в формате "id|email|password"
         * @return экземпляр пользователя
         */
        public User getUserByLine(String userAsLine) {
            String[] userAsArray = userAsLine.split("\\|");
            User user = new User(userAsArray[1], userAsArray[2]);
            user.setId(Integer.parseInt(userAsArray[0]));
            return user;
        }

        public long getOffset() {
            return charsForSkip - 2;
        }

    }
}
