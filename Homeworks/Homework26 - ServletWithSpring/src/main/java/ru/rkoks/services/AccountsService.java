package ru.rkoks.services;

import ru.rkoks.dto.AccountDto;

import java.util.List;

public interface AccountsService {
    List<AccountDto> getAll();

    List<AccountDto> searchByEmail(String email);
}
