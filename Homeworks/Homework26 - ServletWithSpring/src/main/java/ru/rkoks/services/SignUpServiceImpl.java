package ru.rkoks.services;

import ru.rkoks.dto.SignUpForm;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.Locale;

public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository accountsRepository;

    public SignUpServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Регистрируем аккаунт в репозитории по полученным из формы данным
     *
     * @param form объект формы созданной в SignUpServlet
     */
    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail().toLowerCase(Locale.ROOT))
                .password(form.getPassword())
                .build();

        accountsRepository.save(account);

    }
}
