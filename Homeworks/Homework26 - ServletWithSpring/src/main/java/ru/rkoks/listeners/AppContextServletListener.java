package ru.rkoks.listeners;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebListener
public class AppContextServletListener implements ServletContextListener {
    private HikariDataSource dataSource;

    /**
     * Инициализируем и сохраняем в контекст сервлетов необходимые для работы объекты с репозиторием
     * @param sce событие появления контекста сервлетов
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        this.dataSource = context.getBean(HikariDataSource.class);
        servletContext.setAttribute("springContext", context);

    }

    /**
     * "Уничтожаем" объекты, переданные в контекст сервлетов, для работы с БД
     * @param sce событие завершения работы контекста сервлетов
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        dataSource.close();
    }
}
