package ru.rkoks.servlets;


import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.rkoks.dto.SignUpForm;
import ru.rkoks.services.SignUpService;

import java.io.IOException;

@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private SignUpService signUpService;


    /**
     * вызывается при первом обращении к сервлету
     * Инициализируем необходимые объекты
     *
     */
    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        signUpService = context.getBean(SignUpService.class);
    }

    /**
     * Обрабатываем GET запрос
     * перекидываем на JSP сервлет с формой регистрации
     *
     * @param request  запрос
     * @param response ответ
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/signUp.jsp").forward(request, response);
    }

    /**
     * Обрабатываем POST запрос с данными из заполненной формы signUp.jsp
     * создаёт экземпляр DTO и сохраняет данные в БД
     *
     * @param request  запрос
     * @param response ответ
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SignUpForm form = SignUpForm.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();

        signUpService.signUp(form);

        response.sendRedirect("signIn");
    }
}
