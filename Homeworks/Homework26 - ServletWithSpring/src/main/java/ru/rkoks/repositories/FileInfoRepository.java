package ru.rkoks.repositories;

import ru.rkoks.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository {
    void save(FileInfo file);

    Optional<FileInfo> findByStorageFileName(String storageFileName);

    List<FileInfo> searchByOriginalFileName(Long uploaderId, String originalFileName);
}
