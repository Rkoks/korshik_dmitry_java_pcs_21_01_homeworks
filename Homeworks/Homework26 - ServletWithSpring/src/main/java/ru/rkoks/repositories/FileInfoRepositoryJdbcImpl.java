package ru.rkoks.repositories;

import ru.rkoks.models.FileInfo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class FileInfoRepositoryJdbcImpl implements FileInfoRepository {

    //language=SQL
    public static final String SQL_INSERT =
            "insert into file_info(original_file_name, storage_file_name, size, mime_type, description, uploader_id)" +
                    " values (?,?,?,?,?,?)";
    //language=SQL
    private static final String SQL_SELECT_BY_STORAGE_FILE_NAME = "select * from file_info where storage_file_name = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_UPLOADER_ID = "select * from file_info where uploader_id = ? order by original_file_name ";
    //language=SQL
    private static final String SQL_LIKE_BY_ORIGINAL_FILE_NAME = "select * from file_info where " +
            "uploader_id = ? and lower(original_file_name) like lower(?) order by original_file_name ";

    private final DataSource dataSource;

    /**
     * лямбда функция для преобразования данных строки БД в объект (из ResultSet в FileInfo)
     */
    private final Function<ResultSet, FileInfo> fileInfoMapper = row -> {
        try {
            return FileInfo.builder()
                    .id(row.getLong("id"))
                    .originalFileName(row.getString("original_file_name"))
                    .storageFileName(row.getString("storage_file_name"))
                    .size(row.getLong("size"))
                    .mimeType(row.getString("mime_type"))
                    .description(row.getString("description"))
                    .uploaderId(row.getLong("uploader_id"))
                    .build();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public FileInfoRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Сохраняем информацию о файле в БД
     * @param file сохраняемая модель файла
     */
    @Override
    public void save(FileInfo file) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, file.getOriginalFileName());
            statement.setString(2, file.getStorageFileName());
            statement.setLong(3, file.getSize());
            statement.setString(4, file.getMimeType());
            statement.setString(5, file.getDescription());
            statement.setLong(6, file.getUploaderId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert FileInfo");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                file.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Достаём из БД информацию о файле по его имени в хранилище
     * @param storageFileName UUID имя файла
     * @return информацию о файле
     */
    @Override
    public Optional<FileInfo> findByStorageFileName(String storageFileName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_STORAGE_FILE_NAME)) {
            statement.setString(1, storageFileName);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(fileInfoMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Производим поиск по совпадению имени загруженного на сервер файла
     * @param uploaderId идентификатор владельца файла (кто загрузил)
     * @param originalFileName имя или часть имени файла для поиска
     * @return список файлов содержащих в имени <code>originalFileName</code>
     */
    @Override
    public List<FileInfo> searchByOriginalFileName(Long uploaderId, String originalFileName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_LIKE_BY_ORIGINAL_FILE_NAME)) {
            statement.setLong(1, uploaderId);
            statement.setString(2, "%" + originalFileName + "%");

            try (ResultSet resultSet = statement.executeQuery()) {
                List<FileInfo> files = new ArrayList<>();
                while (resultSet.next()) {
                    files.add(fileInfoMapper.apply(resultSet));
                }
                return files;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
