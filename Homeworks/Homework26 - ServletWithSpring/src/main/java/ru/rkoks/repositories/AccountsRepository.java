package ru.rkoks.repositories;

import ru.rkoks.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountsRepository {
    void save(Account account);

    List<Account> findAll();

    Optional<Account> findByEmail(String email);

    Optional<Account> findById(Long id);

    List<Account> searchByEmail(String email);
}
