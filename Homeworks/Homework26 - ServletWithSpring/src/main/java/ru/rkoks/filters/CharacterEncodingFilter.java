package ru.rkoks.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;

import java.io.IOException;

@WebFilter("/filesUpload")
public class CharacterEncodingFilter implements Filter {
    private static final String encodingCharset = "UTF-8";

    //Устанавливает кодировку для запросов и ответов в UTF-8
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encodingCharset);
        servletResponse.setCharacterEncoding(encodingCharset);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
