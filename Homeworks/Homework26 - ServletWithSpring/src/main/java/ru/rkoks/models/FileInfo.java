package ru.rkoks.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
/**
 * Модель файлов
 */
public class FileInfo {
    private Long id;
    private String originalFileName;
    private String storageFileName;
    private Long size;
    private String mimeType;
    private String description;
    private Long uploaderId;

}
