package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
/**
 * Модель пользователя
 */
public class Account {
    private Long id;

    private String firstName;
    private String lastName;

    private String email;
    private String password;


}
