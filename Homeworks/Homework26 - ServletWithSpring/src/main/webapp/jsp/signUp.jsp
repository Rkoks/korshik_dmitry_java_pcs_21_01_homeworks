<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SignUp</title>
</head>
<body>
    <h1>Sign Up Page</h1>
    <h2>Please enter your data:</h2>
<%--    Форма регистрации пользователя--%>
    <form action="signUp" method="post">
        <label for="firstName">Enter your firstname:</label>
        <input id="firstName" name="firstName" placeholder="Your firstname">
        <br>
        <label for="lastName">Enter your lastname:</label>
        <input id="lastName" name="lastName" placeholder="Your lastname">
        <br>
        <label for="email">Enter email:</label>
        <input id="email" name="email" type="email" placeholder="Your email">
        <br>
        <label for="password">Enter password:</label>
        <input id="password" name="password" type="password" placeholder="Your password">
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
