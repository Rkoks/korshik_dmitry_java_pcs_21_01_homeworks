<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: dkors
  Date: 20.10.2021
  Time: 21:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Profile</title>
</head>
<%--Страница профиля--%>
<body>
    <h1>Profile</h1>
    <p>Your ID: <b>${accountData.id}</b></p>
    <p>Your FirstName: <b>${accountData.firstName}</b></p>
    <p>Your LastName: <b>${accountData.lastName}</b></p>

    <a href="files">Список Ваших файлов</a>
    <br>
    <a href="filesUpload">Загрузить файлы</a>
    <form action="profile" method="post">
        <input type="submit" value="Logout">
    </form>
</body>
</html>
