<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SignUp</title>
</head>
<body>
    <h1>Sign In Page</h1>
    <h2>Please enter your data:</h2>
<%--    Форма аутентификации пользователя--%>
    <form action="signIn" method="post">
        <label for="email">Enter email:</label>
        <input id="email" name="email" type="email" placeholder="Your email">
        <br>
        <label for="password">Enter password:</label>
        <input id="password" name="password" type="password" placeholder="Your password">
        <br>
        <input type="submit" value="Sign In">
    </form>
</body>
</html>
