<%--
  Created by IntelliJ IDEA.
  User: dkors
  Date: 23.10.2021
  Time: 19:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Your Files</title>
</head>
<script>
<%--    Функция отправляет запрос на поиск файлов по имени для текущего пользователя, обрабатывает JSON и формирует список файлов--%>
    function searchFile(fileName) {
        let request = new XMLHttpRequest();

        request.open('POST', 'files', false);
        request.send(fileName);

        if (request.status !== 200) {
            alert("Ошибка! " + request.status);
        } else {
            let html =
                '<tr>' +
                    '<th>FileName</th>' +
                    '<th>Description</th>' +
                '</tr>';

            let json = JSON.parse(request.response);

            for (let i = 0; i < json.length; i++) {
                html += '<tr>'
                html +=     '<td><a href="files?fileName=' + json[i]['storageFileName'] + '">'
                html +=         json[i]['originalFileName'] + '</a></td>'
                html +=     '<td>' + json[i]['description'] + '</td>'
                html += '</tr>'
            }
            document.getElementById('files_table').innerHTML = html;
        }

    }
</script>
<body onload="searchFile(document.getElementById('fileName').value)">
<label for="fileName">Search by File Name:</label>
<%--Делаем запрос на поиск после введения каждого символа--%>
<input id="fileName" type="text" onkeyup="searchFile(document.getElementById('fileName').value)">
<table id="files_table">

</table>


</body>
</html>
