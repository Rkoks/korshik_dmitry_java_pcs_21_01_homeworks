package ru.rkoks.app;

import ru.rkoks.config.SqlConfigurator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import ru.rkoks.repositories.ProductsRepository;

import javax.sql.DataSource;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;
    public static void main(String[] args) {
        Main main = new Main();

        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        SqlConfigurator configurator = new SqlConfigurator();

        DataSource dataSource = configurator.getDataSource(main.poolSize);

        ProductsRepository repo = new ru.rkoks.repositories.ProductsRepositoryJdbcImpl(dataSource);

        System.out.println(repo.findAll());

    }
}
