package ru.rkoks.repositories;

import ru.rkoks.model.Product;

import java.util.List;

/**
 * Хранилище продуктов
 */
public interface ProductsRepository {
    List<Product> findAll();
}
