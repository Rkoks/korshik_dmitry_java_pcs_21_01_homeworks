package ru.rkoks.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SqlConfigurator {

    private String DB_USER;
    private String DB_PASS;
    private String DB_URL;
    private String POSTGRESQL_DRIVER;
    private HikariConfig config;

    public SqlConfigurator() {
        readProperties();
        configureDatabase();
    }

    private void configureDatabase() {
        config = new HikariConfig();
        config.setUsername(DB_USER);
        config.setPassword(DB_PASS);
        config.setJdbcUrl(DB_URL);
        config.setDriverClassName(POSTGRESQL_DRIVER);
    }

    public DataSource getDataSource(int connectionCount) {
        config.setMaximumPoolSize(connectionCount);
        return new HikariDataSource(config);
    }

    private void readProperties() {
        Properties properties = new Properties();
        try {
            InputStream fileInput = getClass().getResourceAsStream("/sqlconfig.properties");
            properties.load(fileInput);
            this.DB_USER = properties.getProperty("sql.db.user");
            this.DB_PASS = properties.getProperty("sql.db.pass");
            this.DB_URL = properties.getProperty("sql.db.url");
            this.POSTGRESQL_DRIVER = properties.getProperty("sql.db.driver");

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }
    
}