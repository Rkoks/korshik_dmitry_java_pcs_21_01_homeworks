package loading;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Loader {
    private String urlFilename; //имя файла, из которого будем загружать URL
    private final String dirToSave; //директория, в которую будем сохранять загружаемые файлы
    private List<String> urls; //список URL, по которым будем загружать файлы

    public Loader(String urlFilename, String dirToSave) {
        this.urlFilename = urlFilename;
        this.dirToSave = dirToSave;
    }

    /**
     * скачивает файл по указанному URL и сохраняет в папку <code>dirToSave</code> под указанным именем
     * @param url сслыка на скачиваемый файл
     * @param filename под каким именем сохранять файл
     * @return возвращает длину сохранённого файла
     */
    private long loadFromUrl(URL url, String filename) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            File fileToSave = new File(dirToSave + "/" + filename + ".csv");
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileToSave));

            String line = reader.readLine();
            while (line != null) {
                writer.write(line);
                writer.newLine();
                line = reader.readLine();
            }

            return fileToSave.length();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * загружает строки адресов для скачивания файлов в список <code>urls</code>
     */
    private void loadUrlsFromFile() {
        urls = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(urlFilename))) {
            String line = reader.readLine();
            while (line != null) {
                urls.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Начинает загрузку множества файлов по списку <code>urls</code> передавая задачи в отдельные потоки
     * используя экземпляр <code>ExecutorService</code>
     * при большом количестве загрузок по кругу проходит по списку <code>urls</code>
     * @param filename под каким именем сохранять, к имени файла добавляется число - номер загрузки
     * @param loadsNeeded сколько файлов необходимо загрузить, может быть больше <code>urls.length()</code>
     * @param threadsCount количество потоков между которыми распределяются задачи
     * @return общую длину загруженных файлов в байтах
     */
    public long startLoadInMultiThreads(String filename, int loadsNeeded, int threadsCount) {
        loadUrlsFromFile();
        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);

        int urlCount = urls.size();
        Future<Long>[] tasks = new Future[loadsNeeded];

        for (int i = 0; i < loadsNeeded; i++) {
            try {
                URL url = new URL(urls.get(i % urlCount));
                String nameToSave = filename + i;
                Callable<Long> loadFunc = () -> loadFromUrl(url, nameToSave);
                tasks[i] = executor.submit(loadFunc);
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException(e);
            }
        }

        waitAllResults(tasks); //заставляем поток main ждать завершения выполнения всех задач
        executor.shutdown(); //закрываем работу ExecutorService и всех его потоков

        return calculateTotalSize(tasks);
    }

    /**
     * Подсчитывает итоговый вес загруженных файлов
     * @param results массив задач на скачивание файлов
     * @return общую длину файлов в байтах
     */
    private long calculateTotalSize(Future<Long>[] results) {
        long result = 0;
        for (Future<Long> eachRes : results) {
            try {
                result += eachRes.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return result;
    }

    /**
     * Дожидается выполнения всех многопоточных задач из переданного массива Future<?>
     * @param tasks массив задач для проверки на готовность
     */
    private void waitAllResults(Future<?>[] tasks) {
        while (true) {
            int count = 0;

            for (Future<?> task: tasks) {
                if (task.isDone()) {
                    count++;
                }
            }
            if (count == tasks.length) {
                break;
            }
        }
    }
}
