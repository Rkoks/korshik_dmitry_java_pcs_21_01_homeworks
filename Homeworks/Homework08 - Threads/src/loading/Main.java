package loading;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        sc.nextLine();
        Loader loader = new Loader("urls.txt", "files");

        long loadedSize = loader.startLoadInMultiThreads("file", 100, 12);
        System.out.println("Загружено: " + loadedSize + " байт (" + (loadedSize / (1024*1024)) + " Мб)");

    }
}
