package ru.rkoks.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.CoursesResponse;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.dto.LessonsResponse;
import ru.rkoks.services.CourseService;

@RestController
@RequiredArgsConstructor
@RequestMapping("courses")
public class CourseController {
    private final CourseService courseService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CoursesResponse> getCourses() {
        return ResponseEntity.ok()
                .body(CoursesResponse.builder()
                        .data(courseService.getCourses())
                        .build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CourseDto addCourse(@RequestBody CourseDto course) {
        return courseService.addCourse(course);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CourseDto updateCourse(@PathVariable("course-id") Long courseId,
                                  @RequestBody CourseDto course) {
        return courseService.updateCourse(courseId, course);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCourse(@PathVariable("course-id") Long courseId) {
        courseService.deleteCourse(courseId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LessonsResponse> addLessonToCourse(@PathVariable("course-id") Long courseId,
                                                             @RequestBody LessonDto lesson) {
        return ResponseEntity.ok()
                .body(courseService.addLessonToCourse(courseId, lesson));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<LessonsResponse> deleteLessonFromCourse(@PathVariable("course-id") Long courseId,
                                                                  @RequestBody LessonDto lesson) {
        return ResponseEntity.ok()
                .body(courseService.deleteLessonFromCourse(courseId, lesson));
    }
}
