package ru.rkoks.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.dto.LessonsResponse;
import ru.rkoks.services.LessonService;

@RestController
@RequiredArgsConstructor
@RequestMapping("lessons")
public class LessonController {
    private final LessonService lessonService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<LessonsResponse> getLessons() {
        return ResponseEntity.ok()
                .body(LessonsResponse.builder()
                        .data(lessonService.getLessons())
                        .build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDto addLesson(@RequestBody LessonDto lesson) {
        return lessonService.addLesson(lesson);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonDto updateLesson(@PathVariable("lesson-id") Long lessonId,
                                  @RequestBody LessonDto lesson) {
        return lessonService.updateLesson(lessonId, lesson);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonService.deleteLesson(lessonId);
    }
}
