package ru.rkoks.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.CoursesResponse;
import ru.rkoks.dto.StudentDto;
import ru.rkoks.dto.StudentsResponse;
import ru.rkoks.models.Course;
import ru.rkoks.services.StudentService;

import java.time.LocalDateTime;

@RestController
@RequestMapping("students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<StudentsResponse> getStudents(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .header("DateTime", LocalDateTime.now().toString())
                .body(StudentsResponse.builder().data(studentService.getStudents(page, size)).build());

    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto addStudent(@RequestBody StudentDto student) {
        return studentService.addStudent(student);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public StudentDto updateStudent(@PathVariable("student-id") Long studentId, @RequestBody StudentDto student) {
        return studentService.updateStudent(studentId, student);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable("student-id") Long studentId) {
        studentService.deleteStudent(studentId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{student-id}/courses")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CoursesResponse> addCourseToStudent(@PathVariable("student-id") Long studentId,
                                                              @RequestBody CourseDto course){
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDateTime.now().toString()))
                .body(studentService.addCourseToStudent(studentId, course));
    }
}
