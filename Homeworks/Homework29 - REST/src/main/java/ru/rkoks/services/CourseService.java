package ru.rkoks.services;

import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.LessonDto;
import ru.rkoks.dto.LessonsResponse;

import java.util.List;

public interface CourseService {
    List<CourseDto> getCourses();

    CourseDto addCourse(CourseDto course);

    CourseDto updateCourse(Long courseId, CourseDto course);

    void deleteCourse(Long courseId);

    LessonsResponse addLessonToCourse(Long courseId, LessonDto lesson);

    LessonsResponse deleteLessonFromCourse(Long courseId, LessonDto lesson);
}
