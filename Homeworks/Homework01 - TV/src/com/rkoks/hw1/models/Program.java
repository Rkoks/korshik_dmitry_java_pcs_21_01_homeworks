package com.rkoks.hw1.models;

public class Program {
    private String name;

    public Program(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
