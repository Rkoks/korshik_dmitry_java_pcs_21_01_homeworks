package com.rkoks.hw1.models;

import java.util.Arrays;

public class TV {
    private String model;
    private Channel[] channels;
    private int channelCount;

    public TV(String model) {
        this.model = model;
        channelCount = 0;
        channels = new Channel[channelCount + 1];
    }

    public void addChannel(Channel channel) {
        channels[channelCount] = channel;
        channelCount++;
        channels = Arrays.copyOf(channels, channelCount + 1);
    }

    public Channel getChannel(int index) {
        if (index >= 0 && index < channelCount) {
            return channels[index];
        }
        return null;
    }

    public String getModel() {
        return model;
    }
}
