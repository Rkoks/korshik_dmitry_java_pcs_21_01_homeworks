package com.rkoks.hw1.models;

public class Controller {
    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on(int channelIndex) {
        Channel channel = tv.getChannel(channelIndex - 1);
        Program program = channel.getRndProgram();
        System.out.println("Включили ТВ: " + tv.getModel() + ". Идёт передача \"" + program.getName() + "\" на канале \"" + channel.getName() + "\"");

    }
}
