package com.rkoks.hw1.models;

import java.util.Arrays;
import java.util.Random;

public class Channel {
    private String name;
    private Program[] programs;
    private int programCount;

    public Channel(String name) {
        this.name = name;
        programCount = 0;
        programs = new Program[programCount + 1];
    }

    public void addProgram(Program program) {
        programs[programCount] = program;
        programCount++;
        programs = Arrays.copyOf(programs, programCount + 1);
    }

    public Program getRndProgram() {
        Random rnd = new Random();
        return programs[rnd.nextInt(programCount)];
    }

    public String getName() {
        return name;
    }
}
