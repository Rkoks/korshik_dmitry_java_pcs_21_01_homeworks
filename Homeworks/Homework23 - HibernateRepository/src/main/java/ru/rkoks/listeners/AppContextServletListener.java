package ru.rkoks.listeners;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.IOException;
import java.util.Properties;

@WebListener
public class AppContextServletListener implements ServletContextListener {
    /**
     * Инициализируем и сохраняем в контекст сервлетов необходимые для работы объекты с репозиторием
     * @param sce событие появления контекста сервлетов
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        servletContext.setAttribute("hibernateConfiguration", configuration);
        servletContext.setAttribute("sessionFactory", sessionFactory);
        servletContext.setAttribute("storagePath", properties.getProperty("storage.path"));
    }

    /**
     * "Уничтожаем" объекты, переданные в контекст сервлетов,  для работы с БД
     * @param sce событие завершения работы контекста сервлетов
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        SessionFactory sessionFactory = (SessionFactory) servletContext.getAttribute("sessionFactory");
        sessionFactory.close();
    }
}
