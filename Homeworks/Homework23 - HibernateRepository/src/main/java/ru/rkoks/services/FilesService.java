package ru.rkoks.services;

import ru.rkoks.dto.FileForm;
import ru.rkoks.models.FileInfo;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    void upload(FileForm fileForm);

    void setStoragePath(String storagePath);

    FileInfo getById(Long id);

    List<FileInfo> getAll();

    void writeFile(FileInfo fileInfo, OutputStream output);

    List<FileInfo> getByOriginalFilename(String filename);

}
