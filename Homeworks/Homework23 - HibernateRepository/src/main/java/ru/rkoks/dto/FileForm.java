package ru.rkoks.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
/**
 * Класс, определяющий форму файлов, для скачивания с сервера и загрузки на сервер
 */
public class FileForm {
    private String originalFilename;
    private Long size;
    private String mimeType;
    private InputStream fileStream;

}
