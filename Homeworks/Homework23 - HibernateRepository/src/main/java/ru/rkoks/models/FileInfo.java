package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
/**
 * Информация о загруженных на сервер файлах
 */
public class FileInfo {
    private Long id;
    private String originalFilename;
    private String storageFilename;
    private Long size;
    private String mimeType;
    private Date uploadDate;

}
