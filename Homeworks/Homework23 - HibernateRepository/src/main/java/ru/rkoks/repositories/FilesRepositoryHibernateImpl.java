package ru.rkoks.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.rkoks.models.FileInfo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FilesRepositoryHibernateImpl implements FilesRepository {
    //language=HQL
    private static final String HQL_LIKE_BY_FILENAME = "from FileInfo where lower(originalFilename) like lower(:filename)";
    private final SessionFactory sessionFactory;
    private Session session;

    public FilesRepositoryHibernateImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Сохраняем информацию о файле в БД
     * @param fileInfo модель информации о файле
     */
    @Override
    public void save(FileInfo fileInfo) {
        openTransaction();

        session.persist(fileInfo);

        closeTransaction();
    }

    /**
     * Достаём из БД информацию о файле по <code>id</code>
     * @param id идентификатор в БД запрашиваемого файла
     * @return информацию о файле
     */
    @Override
    public FileInfo getById(Long id) {
        openTransaction();

        FileInfo fileInfo = session.get(FileInfo.class, id);

        closeTransaction();
        return fileInfo;
    }

    /**
     * Находим информацию в БД о всех загруженных файлах
     * @return список информации о найденных файлах
     */
    @Override
    public List<FileInfo> findAll() {
        openTransaction();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<FileInfo> criteriaQuery = builder.createQuery(FileInfo.class);
        criteriaQuery.from(FileInfo.class);
        List<FileInfo> filesInfo = session.createQuery(criteriaQuery).getResultList();

        closeTransaction();

        return filesInfo;
    }

    /**
     * Производим поиск информации о файлах в БД по имени загруженных файлов
     * @param filename запрашиваемое имя файла для поиска
     * @return список информации о найденных файлах
     */
    @Override
    public List<FileInfo> findByName(String filename) {
        openTransaction();


        Query<FileInfo> query = session.createQuery(HQL_LIKE_BY_FILENAME, FileInfo.class);
        query.setParameter("filename", "%" + filename + "%");
        List<FileInfo> filesInfo = query.getResultList();

        closeTransaction();
        return filesInfo;

    }

    /**
     * Начинает работу с БД - подготавливает сессию и начинает транзакцию
     */
    private void openTransaction() {
        this.session = this.sessionFactory.openSession();
        this.session.beginTransaction();

    }

    /**
     * Заканчивает работу с БД - завершает транзакцию и закрывает сессию
     */
    private void closeTransaction() {
        this.session.getTransaction().commit();
        this.session.close();
    }
}
