package ru.rkoks.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.hibernate.SessionFactory;
import ru.rkoks.models.FileInfo;
import ru.rkoks.repositories.FilesRepository;
import ru.rkoks.repositories.FilesRepositoryHibernateImpl;
import ru.rkoks.services.FilesService;
import ru.rkoks.services.FilesServiceImpl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@WebServlet("/")
public class FilesManagerServlet extends HttpServlet {
    private FilesService filesService;
    private ObjectMapper objectMapper;

    /**
     * Инициализируем сервис работы с данными
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();

        SessionFactory sessionFactory = (SessionFactory) servletContext.getAttribute("sessionFactory");
        FilesRepository filesRepository = new FilesRepositoryHibernateImpl(sessionFactory);

        filesService = new FilesServiceImpl(filesRepository);
        filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));

        objectMapper = new ObjectMapper();
    }

    /**
     * Обрабатываем GET-запрос - переходим на сервлет со списком файлов или отдаём запрашиваемый файл
     * @param request может иметь параметр <code>download</code> - id файла для скачивания
     * @param response в случае скачивания файла находим файл в репозитории и отдаём его клиенту
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idString = request.getParameter("download");
        if (idString != null && idString.matches("\\d*")) {
            Long id = Long.valueOf(idString);
            FileInfo fileInfo = filesService.getById(id);

            response.setContentType(fileInfo.getMimeType());
            response.setContentLength(fileInfo.getSize().intValue());

            String newName = new String(fileInfo.getOriginalFilename().getBytes(), StandardCharsets.ISO_8859_1);
            response.setHeader("Content-Disposition", "filename=\"" + newName + "\"");

            filesService.writeFile(fileInfo, response.getOutputStream());
            response.flushBuffer();
        } else {
            request.getRequestDispatcher("jsp/fileManager.jsp").forward(request, response);
        }

    }

}
