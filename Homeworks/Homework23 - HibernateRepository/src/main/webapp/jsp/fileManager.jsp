<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>File Manager</title>
</head>
<script>
    // Функция отправляет запрос на сервлет и формирует данные в таблице по полученным JSON объектам
    function getFiles(filename) {
        let request = new XMLHttpRequest();
        request.open('GET', 'fileUpload?filename=' + filename, false);
        request.send();

        if (request.status !== 200) {
            alert("Ошибка! " + request.status);
        } else {
            let html =
                '<tr>' +
                '<th>Filename</th>' +
                '<th>Size</th>' +
                '<th>Upload Date</th>' +
                '</tr>';

            let json = JSON.parse(request.response);

            for (let i = 0; i < json.length; i++) {
                let date = new Date(json[i]['uploadDate']).toLocaleDateString();
                let size = json[i]['size'];
                let dimCount = 0;
                while (size > 1024) {
                    size /= 1024;
                    dimCount++;
                }
                let dim;
                switch (dimCount) {
                    case 0:
                        dim = 'B';
                        break;
                    case 1:
                        dim = 'KB';
                        break;
                    case 2:
                        dim = 'MB';
                        break;
                    case 3:
                        dim = 'GB';
                }
                html += '<tr>';
                html += '<td><a href="?download=' + json[i]['id'] + '">';
                html += json[i]['originalFilename'] + '</a></td>';
                html += '<td>' + Math.round(size) + ' ' + dim + '</td>'
                html += '<td>' + date + '</td>';
                html += '</tr>';
            }

            document.getElementById('files_table').innerHTML = html;
        }

    }
</script>
<%--    Обновляем данные из репозитория после загрузки страницы--%>
<body onload="getFiles('')">
<h1>File Manager</h1>
<div id="upload_file">
    <form method="post" action="fileUpload" enctype="multipart/form-data">
        <label for="file">Выберите файл для загрузки:</label>
        <br>
        <input id="file" type="file" name="file">
        <input type="submit" value="Upload File">
    </form>
</div>
<div id="search_by_name">
    <label for="filename"><b>LiveSearch: </b></label>
    <%--    Обновляем данные из репозитория после ввода нового символа--%>
    <input id="filename" type="text" placeholder="Enter filename.." name="filename"
           onkeyup="getFiles(document.getElementById('filename').value)">
</div>
<br>
<div>
    <table id="files_table">
        <tr>
            <th>Filename</th>
            <th>Size</th>
            <th>Upload Date</th>
        </tr>
    </table>
</div>
</body>
</html>