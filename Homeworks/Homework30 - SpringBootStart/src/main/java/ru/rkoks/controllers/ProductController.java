package ru.rkoks.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.ProductDto;
import ru.rkoks.services.ProductsService;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/products")
public class ProductController {
    private final ProductsService productsService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String getProductPage(Model model) {
        if (!model.containsAttribute("productDto")) {
            model.addAttribute("productDto", new ProductDto());
        }
        model.addAttribute("products", productsService.getAll());
        return "productPage";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public String addProduct(@Valid ProductDto product, BindingResult result, Model model) {
        if (result.hasErrors()){
            model.addAttribute("productDto", product);
            return getProductPage(model);
        }
        productsService.addProduct(product);
        model.addAttribute("productDto", new ProductDto());
        return getProductPage(model);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public List<ProductDto> deleteProduct(@RequestBody ProductDto product) {
        productsService.deleteProduct(product.getId());
        return productsService.getAll();
    }
}
