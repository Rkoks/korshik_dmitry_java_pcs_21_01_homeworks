package ru.rkoks.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rkoks.models.Product;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByIsDeletedIsNullOrderById();
}
