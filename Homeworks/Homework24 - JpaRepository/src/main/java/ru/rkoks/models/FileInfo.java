package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
/**
 * Информация о загруженных на сервер файлах
 */
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 100, name = "original_filename")
    private String originalFilename;
    @Column(length = 60, name = "storage_filename")
    private String storageFilename;
    private Long size;
    @Column(name = "mime_type")
    private String mimeType;
    @Column(name = "upload_date")
    private Date uploadDate;

}
