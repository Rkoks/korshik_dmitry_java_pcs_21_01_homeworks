package ru.rkoks.services;

import ru.rkoks.dto.FileForm;
import ru.rkoks.models.FileInfo;
import ru.rkoks.repositories.FilesRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class FilesServiceImpl implements FilesService {
    private final FilesRepository filesRepository;
    private String storagePath;

    public FilesServiceImpl(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    /**
     * загружаем файл на сервер - берём данные из формы и дополняем именем для хранения и датой загрузки,
     * сохраняем информацию в БД, а файл в хранилище
     * @param fileForm загружаемый файл и информация о нём
     */
    @Override
    public void upload(FileForm fileForm) {
        String originalFilename = fileForm.getOriginalFilename();
        String extension = originalFilename.substring(originalFilename.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFilename(originalFilename)
                .storageFilename(UUID.randomUUID() + extension)
                .size(fileForm.getSize())
                .mimeType(fileForm.getMimeType())
                .uploadDate(Date.valueOf(LocalDate.now()))
                .build();

        filesRepository.save(fileInfo);

        if (storagePath != null) {
            try {
                Files.copy(fileForm.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFilename()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    /**
     * устанавливаем директорию хранилища файлов, создаём папку в случае её отсутствия
     * @param storagePath путь к хранилищу в файловой системе
     */
    public void setStoragePath(String storagePath) {
        Path directory = Paths.get(storagePath);
        if (!Files.exists(directory)) {
            try {
                Files.createDirectory(directory);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
        this.storagePath = storagePath;
    }

    /**
     * Находим файл в репозитории по его <code>id</code>
     * @param id запрашиваемый <code>id</code>
     * @return информацию о найденном файле
     */
    @Override
    public FileInfo getById(Long id) {
        return filesRepository.getById(id);
    }

    /**
     * Находим информацию о всех загруженных файлах в репозитории
     * @return список с информацией о найденных файлах
     */
    @Override
    public List<FileInfo> getAll() {
        return filesRepository.findAll();
    }

    /**
     * Скачиваем необходимый файл из хранилища в <code>OutputStream</code>
     * @param fileInfo информация о запрашиваемом для скачивания файле
     * @param output исходящий поток для скачивания файла клиентом
     */
    @Override
    public void writeFile(FileInfo fileInfo, OutputStream output) {
        try {
            Files.copy(Paths.get(storagePath + fileInfo.getStorageFilename()), output);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Производим поиск информации о файлах в репозитории по запрашиваемому имени (части имени)
     * @param filename имя файла (часть имени) для поиска по репозиторию
     * @return список с информацией о найденных файлах
     */
    @Override
    public List<FileInfo> getByOriginalFilename(String filename) {
        return filesRepository.findAllByName(filename);
    }
}
