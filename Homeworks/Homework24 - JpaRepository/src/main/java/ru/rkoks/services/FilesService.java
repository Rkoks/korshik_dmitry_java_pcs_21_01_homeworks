package ru.rkoks.services;

import ru.rkoks.dto.FileForm;
import ru.rkoks.models.FileInfo;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    /**
     * загрузка файла на сервер
     * @param fileForm загружаемый файл и информация о нём
     */
    void upload(FileForm fileForm);

    /**
     * устанавливаем директорию хранилища файлов
     * @param storagePath путь к хранилищу в файловой системе
     */
    void setStoragePath(String storagePath);

    /**
     * Находим файл по его <code>id</code>
     * @param id запрашиваемый <code>id</code>
     * @return информацию о найденном файле
     */
    FileInfo getById(Long id);

    /**
     * Находим все загруженные файлы
     * @return список с информацией о найденных файлах
     */
    List<FileInfo> getAll();

    /**
     * Скачиваем необходимый файл из хранилища
     * @param fileInfo информация о запрашиваемом для скачивания файле
     * @param output исходящий поток для скачивания файла клиентом
     */
    void writeFile(FileInfo fileInfo, OutputStream output);

    /**
     * Производим поиск по имени загруженных файлов
     * @param filename имя файла (часть имени) для поиска по репозиторию
     * @return список с информацией о найденных файлах
     */
    List<FileInfo> getByOriginalFilename(String filename);

}
