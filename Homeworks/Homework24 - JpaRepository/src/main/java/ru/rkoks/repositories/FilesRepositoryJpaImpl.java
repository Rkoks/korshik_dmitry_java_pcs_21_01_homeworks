package ru.rkoks.repositories;

import ru.rkoks.models.FileInfo;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FilesRepositoryJpaImpl implements FilesRepository {
    //language=HQL
    private static final String HQL_LIKE_BY_FILENAME = "from FileInfo where lower(originalFilename) like lower(?1)";

    private final EntityManager entityManager;
    private EntityTransaction transaction;

    public FilesRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Сохраняем информацию о файле в БД
     * @param fileInfo модель информации о файле
     */
    @Override
    public void save(FileInfo fileInfo) {
        transaction = entityManager.getTransaction();
        transaction.begin();

        entityManager.persist(fileInfo);

        transaction.commit();
    }

    /**
     * Достаём из БД информацию о файле по <code>id</code>
     * @param id идентификатор в БД запрашиваемого файла
     * @return информацию о файле
     */
    @Override
    public FileInfo getById(Long id) {
        FileInfo fileInfo = entityManager.find(FileInfo.class, id);

        return fileInfo;
    }

    /**
     * Находим информацию в БД о всех загруженных файлах
     * @return список информации о найденных файлах
     */
    @Override
    public List<FileInfo> findAll() {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FileInfo> criteriaQuery = builder.createQuery(FileInfo.class);
        List<FileInfo> filesInfo = entityManager.createQuery(criteriaQuery).getResultList();

        return filesInfo;
    }

    /**
     * Производим поиск информации о файлах в БД по имени загруженных файлов
     * @param filename запрашиваемое имя файла для поиска
     * @return список информации о найденных файлах
     */
    @Override
    public List<FileInfo> findAllByName(String filename) {

        TypedQuery<FileInfo> query = entityManager.createQuery(HQL_LIKE_BY_FILENAME, FileInfo.class);
        query.setParameter(1, "%" + filename + "%");
        List<FileInfo> filesInfo = query.getResultList();

        return filesInfo;
    }
}
