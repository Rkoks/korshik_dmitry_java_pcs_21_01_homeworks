package ru.rkoks.repositories;

import ru.rkoks.models.FileInfo;

import java.util.List;

public interface FilesRepository {
    void save(FileInfo fileInfo);

    FileInfo getById(Long id);

    List<FileInfo> findAll();

    List<FileInfo> findAllByName(String filename);
}
