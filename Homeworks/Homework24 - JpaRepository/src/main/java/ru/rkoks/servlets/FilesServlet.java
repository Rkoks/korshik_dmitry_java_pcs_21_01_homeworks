package ru.rkoks.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.hibernate.SessionFactory;
import ru.rkoks.dto.FileForm;
import ru.rkoks.models.FileInfo;
import ru.rkoks.repositories.FilesRepository;
import ru.rkoks.repositories.FilesRepositoryJpaImpl;
import ru.rkoks.services.FilesService;
import ru.rkoks.services.FilesServiceImpl;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.List;

@WebServlet("/files")
@MultipartConfig
public class FilesServlet extends HttpServlet {
    private FilesService filesService;
    private ObjectMapper objectMapper;

    /**
     * Инициализируем сервис работы с данными
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();

        SessionFactory sessionFactory = (SessionFactory) servletContext.getAttribute("sessionFactory");
        EntityManager entityManager = sessionFactory.createEntityManager();
        FilesRepository filesRepository = new FilesRepositoryJpaImpl(entityManager);

        filesService = new FilesServiceImpl(filesRepository);
        filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));

        objectMapper = new ObjectMapper();
    }

    /**
     * Обрабатываем POST-запрос - загрузка файла на сервер
     * @param request получаем информацию о загружаемом файле и стрим самого файла
     * @param response возвращаемся на страницу откуда был произведён запрос
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file");

        String submittedFileName = new String(filePart.getSubmittedFileName().getBytes());

        FileForm fileForm = FileForm.builder()
                .originalFilename(submittedFileName)
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .fileStream(filePart.getInputStream())
                .build();

        filesService.upload(fileForm);
        response.sendRedirect(request.getContextPath());
    }

    /**
     * обрабатывает GET-запрос - отдаём информацию обо всех файлах или после фильтрации по оригинальному имени файла
     * @param request может получить <code>filename</code> - имя файла для фильтрации
     * @param response отдаём JSON с информацией о найденных файлах
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filename = request.getParameter("filename");
        List<FileInfo> filesInfo;
        if (filename == null) {
            filesInfo = filesService.getAll();
        } else {
            filesInfo = filesService.getByOriginalFilename(filename);
        }

        response.getWriter().println(objectMapper.writeValueAsString(filesInfo));

    }

}
