import chain.*;
import observer.CarNumberChecker;
import observer.ValidNumberChecker;
import observer.WantedNumberChecker;
import service.CarService;
import strategy.BufferedReaderFileProcessor;
import strategy.CarsFileProcessor;
import strategy.StreamFileProcessor;

public class Main {
    public static void main(String[] args) {
        //создаём обработчики данных
        CarHandler numberHandler = new NumberCarHandler("files/numbers.txt");
        CarHandler modelHandler = new ModelCarHandler("files/models.txt");
        CarHandler colorHandler = new ColorCarHandler("files/colors.txt");
        CarHandler mileageHandler = new MileageCarHandler("files/mileages.txt");
        CarHandler costHandler = new CostCarHandler("files/costs.txt");

        //формируем цепочку обработчиков данных
        numberHandler.setNextHandler(modelHandler);
        modelHandler.setNextHandler(colorHandler);
        colorHandler.setNextHandler(mileageHandler);
        mileageHandler.setNextHandler(costHandler);

        //создаём наблюдателей, проверяющих номера автомобилей и устанавливаем их обработчику номеров автомобилей
        CarNumberChecker validationChecker = new ValidNumberChecker("files/blacklist.txt");
        CarNumberChecker wantedListChecker = new WantedNumberChecker("files/wanted.txt");
        ((NumberCarHandler) numberHandler).setChecker(validationChecker);
        ((NumberCarHandler) numberHandler).setChecker(wantedListChecker);

        //создаём и задаём стратегию для считывания файла со списком автомобилей
//        CarsFileProcessor processor = new StreamFileProcessor();
        CarsFileProcessor processor = new BufferedReaderFileProcessor();

        //Создаём основной файл обработки и запускаем процесс обработки с заданными выше параметрами
        CarService service = new CarService("files/cars.txt", numberHandler, processor);
        service.processCarsFile();
    }
}
