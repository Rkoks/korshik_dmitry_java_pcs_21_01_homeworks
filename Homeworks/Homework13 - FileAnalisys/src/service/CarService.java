package service;

import chain.CarHandler;
import strategy.CarsFileProcessor;

/**
 * Главный класс по считыванию и обработке списка автомобилей из файла <code>cars.txt</code>
 */
public class CarService {
    String carsFilename;
    CarHandler firstHandler;
    CarsFileProcessor fileProcessor;

    public CarService(String carsFilename, CarHandler firstHandler, CarsFileProcessor fileProcessor) {
        this.carsFilename = carsFilename;
        this.firstHandler = firstHandler;
        this.fileProcessor = fileProcessor;
    }

    /**
     * метод запускающий обработку файла со списком автомобилей
     */
    public void processCarsFile() {
        fileProcessor.processFile(carsFilename, firstHandler);
    }

    public void setFirstHandler(CarHandler firstHandler) {
        this.firstHandler = firstHandler;
    }

    public void setFileProcessor(CarsFileProcessor fileProcessor) {
        this.fileProcessor = fileProcessor;
    }
}
