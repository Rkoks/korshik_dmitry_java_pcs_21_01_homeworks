package observer;

/**
 * Наблюдатели по проверке номеров автомобилей
 */
public interface CarNumberChecker {
    void handle(String number);
}
