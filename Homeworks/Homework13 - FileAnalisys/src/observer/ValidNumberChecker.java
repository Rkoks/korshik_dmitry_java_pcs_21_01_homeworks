package observer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Наблюдатель проверяющий номер на корректность
 */
public class ValidNumberChecker implements CarNumberChecker {
    private String blacklistFilename;

    public ValidNumberChecker(String blacklistFilename) {
        this.blacklistFilename = blacklistFilename;
    }

    /**
     * Проверяет соответствие номера формату <code>БукваНомерНомерНомерБукваБуква</code>,
     * в случае несоответствия записывает номер в черный список <code>blacklistFilename</code>
     * @param number номер автомобиля, переданный для проверки
     */
    @Override
    public void handle(String number) {
        String checkPattern = "[a-z]\\d{3}[a-z]{2}";
        if (!number.matches(checkPattern)) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(blacklistFilename, true))) {
                writer.write(number);
                writer.newLine();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
