package observer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Наблюдатель, проверяющий номер автомобиля по списку разыскиваемых
 */
public class WantedNumberChecker implements CarNumberChecker{
    private String wantedFilename;

    public WantedNumberChecker(String wantedFilename) {
        this.wantedFilename = wantedFilename;
    }

    /**
     * Проверяет номер автомобиля в файле угнанных автомобилей <code>wantedFilename</code>
     * если найден, то выводит сообщение в консоль
     * @param number номер автомобиля, полученный для проверки
     */
    @Override
    public void handle(String number) {
        try (BufferedReader reader = new BufferedReader(new FileReader(wantedFilename))){
            String wantedNumber = reader.readLine();
            while (wantedNumber != null) {
                if (wantedNumber.equals(number)) {
                    System.out.println("Автомобиль с номером " + number + " объявлен в розыск");
                }
                wantedNumber = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
