package placeholder;

public class Car {
    private String number;
    private String model;
    private String color;
    private int cost;
    private int mileage;

    public Car(String number, String model, String color, int cost, int mileage) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.cost = cost;
        this.mileage = mileage;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getCost() {
        return cost;
    }

    public int getMileage() {
        return mileage;
    }

    @Override
    public String toString() {
        return number + '|' + model + '|' + color + '|' + mileage + '|' + cost;
    }
}
