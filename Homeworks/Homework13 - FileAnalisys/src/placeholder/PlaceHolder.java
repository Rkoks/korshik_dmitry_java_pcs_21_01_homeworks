package placeholder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Класс для генерации случайного списка автомобилей и списка разыскиваемых автомобилей
 */
public class PlaceHolder {
    private enum Model {
        vitz, camry, fit, solaris, demio, teana, panamera, passage
    }

    private enum Color{
        red, blue, black, white, gray, green, yellow
    }

    private Model[] models;
    private Color[] colors;
    private int maxCost;
    private int minCost;
    private int maxMileage;

    private String filename = "files/cars.txt";
    private String wantedCars = "files/wanted.txt";

    public static void main(String[] args) {
        PlaceHolder holder = new PlaceHolder(1500000, 300000, 250000);
        holder.writeCarsToFile(100);
    }

    public PlaceHolder(int maxCost, int minCost, int maxMileage) {
        this.maxCost = maxCost;
        this.minCost = minCost;
        this.maxMileage = maxMileage;
        this.models = Model.values();
        this.colors = Color.values();
    }

    public void writeCarsToFile(int count) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {

            for (int i = 0; i < count; i++) {
                Car car = generateCar();
                if (Math.random() <= 0.1) {
                    writeCarToWantedList(car);
                }
                writer.write(car + "\n");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void writeCarToWantedList(Car car) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(wantedCars, true))) {
                writer.write(car.getNumber() + "\n");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Car generateCar() {
        String number;
        if (Math.random() > 0.15) {
            number = generateNumber();
        } else {
            number = generateNumberWithMistake();
        }
        return new Car(number, generateModel().name(), generateColor().name(), generateCost(), generateMileage());
    }

    private Model generateModel() {
        Random rnd = new Random();
        return models[rnd.nextInt(models.length)];
    }
    private Color generateColor() {
        Random rnd = new Random();
        return colors[rnd.nextInt(colors.length)];
    }
    private int generateCost() {
        Random rnd = new Random();
        return minCost + rnd.nextInt(maxCost - minCost);
    }

    private int generateMileage() {
        Random rnd = new Random();
        return rnd.nextInt(maxMileage);
    }

    private String generateNumber() {
        Random rnd = new Random();
        StringBuilder builder = new StringBuilder();
        builder.append(generateLetter())
                .append(rnd.nextInt(10))
                .append(rnd.nextInt(10))
                .append(rnd.nextInt(10))
                .append(generateLetter())
                .append(generateLetter());
        return builder.toString();
    }

    private String generateNumberWithMistake() {
        Random rnd = new Random();
        StringBuilder builder = new StringBuilder();
        builder.append(generateLetter())
                .append(rnd.nextInt(10))
                .append(rnd.nextInt(10))
                .append(rnd.nextInt(10))
                .append(generateLetter())
                .append(generateLetter());
        builder.deleteCharAt(rnd.nextInt(builder.length()));
        return builder.toString();
    }

    private char generateLetter() {
        Random rnd = new Random();
        return (char) (rnd.nextInt(26) + 'a');
    }
}
