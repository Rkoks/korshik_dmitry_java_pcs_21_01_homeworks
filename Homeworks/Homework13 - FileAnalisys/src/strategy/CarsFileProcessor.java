package strategy;

import chain.CarHandler;

/**
 * Считывает файл со списком автомобилей и вызывает обработчики
 */
public interface CarsFileProcessor {
    void processFile(String filename, CarHandler firstHandler);
}
