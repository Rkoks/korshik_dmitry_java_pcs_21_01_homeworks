package strategy;

import chain.CarHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Считывает файл с помощью Stream API
 */
public class StreamFileProcessor implements CarsFileProcessor {
    @Override
    public void processFile(String filename, CarHandler firstHandler) {
        Path pathToFile = Paths.get(filename);
        try (Stream<String> stream = Files.newBufferedReader(pathToFile).lines()) {
            stream.forEach(firstHandler::handle);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
