package strategy;

import chain.CarHandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * считывает файл построчно с помощью BufferedReader
 */
public class BufferedReaderFileProcessor implements CarsFileProcessor {
    @Override
    public void processFile(String filename, CarHandler firstHandler) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();

            while (line != null) {
                firstHandler.handle(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
