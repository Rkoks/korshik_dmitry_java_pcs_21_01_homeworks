package chain;

import placeholder.Car;

public abstract class AbstractCarHandler implements CarHandler {
    private CarHandler nextHandler;
    @Override
    public void setNextHandler(CarHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void nextHandler(String car) {
        if (nextHandler != null) {
            nextHandler.handle(car);
        }
    }
}
