package chain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ModelCarHandler extends AbstractCarHandler {
    private final String modelFilename;

    public ModelCarHandler(String modelFilename) {
        this.modelFilename = modelFilename;
    }

    /**
     * считывает модель из строки и добавляет её в отдельный файл для моделей <code>modelFilename</code>
     * @param car экземпляр автомобиля в строковом представлении для обработки
     */
    @Override
    public void handle(String car) {
        String carModel = car.split("\\|")[1];
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(modelFilename, true))) {
            writer.write(carModel);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        nextHandler(car);
    }
}
