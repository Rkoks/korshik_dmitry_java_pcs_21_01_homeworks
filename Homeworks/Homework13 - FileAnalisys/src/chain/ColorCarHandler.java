package chain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ColorCarHandler extends AbstractCarHandler {
    private final String colorFilename;

    public ColorCarHandler(String colorFilename) {
        this.colorFilename = colorFilename;
    }

    /**
     * считывает цвет из строки и добавляет его в отдельный файл для цветов <code>colorFilename</code>
     * @param car экземпляр автомобиля в строковом представлении для обработки
     */
    @Override
    public void handle(String car) {
        String carColor = car.split("\\|")[2];
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(colorFilename, true))) {
            writer.write(carColor);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        nextHandler(car);
    }
}
