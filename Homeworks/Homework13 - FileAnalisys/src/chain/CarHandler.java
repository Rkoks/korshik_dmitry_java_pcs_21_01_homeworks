package chain;

import placeholder.Car;

public interface CarHandler {
    /**
     * Обрабатывает свою часть данных (поле) об автомобиле
     * @param car экземпляр автомобиля для обработки
     */
    void handle(String car);

    /**
     * устанавливает обработчик следующего поля автомобиля
     * @param handler следующий обработчик данных автомобиля
     */
    void setNextHandler(CarHandler handler);

    /**
     * передаёт задачу следующему обработчику данных автомобиля
     * @param car экземпляр автомобиля для обработки
     */
    void nextHandler(String car);
}
