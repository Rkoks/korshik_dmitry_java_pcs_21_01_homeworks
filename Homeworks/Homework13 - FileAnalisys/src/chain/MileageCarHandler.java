package chain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MileageCarHandler extends AbstractCarHandler {
    private final String mileageFilename;

    public MileageCarHandler(String mileageFilename) {
        this.mileageFilename = mileageFilename;
    }

    /**
     * считывает пробег из строки и добавляет его в отдельный файл для пробега <code>mileageFilename</code>
     * @param car экземпляр автомобиля в строковом представлении для обработки
     */
    @Override
    public void handle(String car) {
        String carMileage = car.split("\\|")[3];
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(mileageFilename, true))) {
            writer.write(carMileage);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        nextHandler(car);
    }
}
