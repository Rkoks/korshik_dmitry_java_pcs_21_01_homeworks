package chain;

import observer.CarNumberChecker;
import placeholder.Car;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NumberCarHandler extends AbstractCarHandler {
    private final String numbersFilename;
    private List<CarNumberChecker> checkers;

    public NumberCarHandler(String numbersFilename) {
        this.numbersFilename = numbersFilename;
        checkers = new ArrayList<>();
    }

    /**
     * считывает номер из строки и добавляет его в отдельный файл для номеров <code>numberFilename</code>,
     * передаёт номер автомобиля на проверку
     * @param car экземпляр автомобиля в строковом представлении для обработки
     */
    //TODO реализовать оповещение по классам розыска и чёрного списка
    @Override
    public void handle(String car) {
        String carNumber = car.split("\\|")[0];
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(numbersFilename, true))) {
            writer.write(carNumber);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        checkNumber(carNumber);
        nextHandler(car);
    }

    /**
     * Оповещает наблюдателей о необходимости проверить номер автомобиля
     * @param number строка номера, который необходимо проверить
     */
    private void checkNumber(String number) {
        for (CarNumberChecker checker : checkers) {
            checker.handle(number);
        }
    }

    /**
     * Добавляем наблюдателя в список проверяющих <code>checkers</code>
     * @param checker новый наблюдатель
     */
    public void setChecker(CarNumberChecker checker) {
        this.checkers.add(checker);
    }
}
