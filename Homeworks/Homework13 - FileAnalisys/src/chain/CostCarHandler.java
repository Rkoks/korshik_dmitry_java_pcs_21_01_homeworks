package chain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CostCarHandler extends AbstractCarHandler {
    private final String costFilename;

    public CostCarHandler(String costFilename) {
        this.costFilename = costFilename;
    }

    /**
     * считывает стоимость из строки и добавляет её в отдельный файл для стоимости <code>costFilename</code>
     * @param car экземпляр автомобиля в строковом представлении для обработки
     */
    @Override
    public void handle(String car) {
        String carCost = car.split("\\|")[4];
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(costFilename, true))) {
            writer.write(carCost);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        nextHandler(car);
    }
}
