package ru.rkoks.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    private static final List<String> PROTECTED_URIS =
            Arrays.asList("/profile");

    private static final List<String> FORBIDDEN_AFTER_AUTHENTICATION_URIS =
            Arrays.asList("/signUp", "/signIn");

    private static final String DEFAULT_REDIRECT_URI = "/profile";
    private static final String DEFAULT_SIGN_IN_URI = "/signIn";

    public static final String DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME =
            "isAuthenticated";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (isAuthenticated(request)) {
            if (isForbiddenAfterAuthentication(request)) {
                response.sendRedirect(DEFAULT_REDIRECT_URI);
                return;
            }
        } else if (isProtected(request)) {
            response.sendRedirect(DEFAULT_SIGN_IN_URI);
            return;
        }

        filterChain.doFilter(request, response);
    }

    private boolean isForbiddenAfterAuthentication(HttpServletRequest request) {
        return FORBIDDEN_AFTER_AUTHENTICATION_URIS.contains(request.getRequestURI());
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return false;
        }

        Boolean result = (Boolean) session.getAttribute(DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME);

        return result != null && result;
    }

    private boolean isProtected(HttpServletRequest request) {
        return PROTECTED_URIS.contains(request.getRequestURI());
    }
}
