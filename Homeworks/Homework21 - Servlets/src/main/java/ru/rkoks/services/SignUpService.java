package ru.rkoks.services;

import ru.rkoks.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
