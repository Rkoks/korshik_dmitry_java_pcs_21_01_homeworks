package ru.rkoks.services;

import ru.rkoks.dto.SignInForm;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.Optional;

public class SignInServiceImpl implements SignInService {

    private final AccountsRepository accountsRepository;

    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public Optional<Long> doAuthenticate(SignInForm form) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(form.getEmail());

        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            if (account.getPassword().equals(form.getPassword())) {
                return Optional.of(account.getId());
            }
        }

        return Optional.empty();
    }
}
