package ru.rkoks.repositories;

import ru.rkoks.models.Account;

import java.util.Optional;

public interface AccountsRepository {
    void save(Account account);

    Optional<Account> findByEmail(String email);

    Optional<Account> findById(Long id);
}
