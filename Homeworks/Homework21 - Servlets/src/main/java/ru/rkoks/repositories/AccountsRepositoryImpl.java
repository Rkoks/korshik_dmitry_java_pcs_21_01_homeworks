package ru.rkoks.repositories;

import ru.rkoks.models.Account;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;
import java.util.function.Function;

public class AccountsRepositoryImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from accounts order by id";
    //language=SQL
    private static final String SQL_INSERT = "insert into accounts (first_name, last_name, email, password)" +
            "values (?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from accounts where email = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from accounts where id = ?";


    /**
     * лямбда функция для преобразования данных строки БД в объект (из ResultSet в Account)
     */
    private static final Function<ResultSet, Account> accountMapper = row -> {
        try {
            return Account.builder()
                    .id(row.getLong("id"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };


    private final DataSource dataSource;

    public AccountsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Сохраняем аккаунт в базу данных
     *
     * @param account сохраняемый аккаунт
     */
    @Override
    public void save(Account account) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setString(3, account.getEmail());
            statement.setString(4, account.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert account");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                account.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)) {
            statement.setString(1, email);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(accountMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Account> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(accountMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
