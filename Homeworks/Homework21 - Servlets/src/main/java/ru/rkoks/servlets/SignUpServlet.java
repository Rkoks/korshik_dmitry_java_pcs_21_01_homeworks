package ru.rkoks.servlets;


import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ru.rkoks.dto.SignUpForm;
import ru.rkoks.repositories.AccountsRepository;
import ru.rkoks.repositories.AccountsRepositoryImpl;
import ru.rkoks.services.SignUpService;
import ru.rkoks.services.SignUpServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;

public class SignUpServlet extends HttpServlet {
    private SignUpService signUpService;


    /**
     * вызывается при первом обращении к сервлету
     * Инициализируем необходимые объекты
     *
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryImpl(dataSource);

        this.signUpService = new SignUpServiceImpl(accountsRepository);
    }

    /**
     * Обрабатываем GET запрос
     * перекидываем на JSP сервлет с формой регистрации
     *
     * @param request  запрос
     * @param response ответ
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/signUp.jsp").forward(request, response);
    }

    /**
     * Обрабатываем POST запрос с данными из заполненной формы signUp.jsp
     * создаёт экземпляр DTO и сохраняет данные в БД
     *
     * @param request  запрос
     * @param response ответ
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SignUpForm form = SignUpForm.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();

        signUpService.signUp(form);

        response.sendRedirect("/signIn");
    }
}
