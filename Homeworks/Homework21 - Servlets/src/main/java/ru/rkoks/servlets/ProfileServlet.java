package ru.rkoks.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.filters.AuthenticationFilter;
import ru.rkoks.repositories.AccountsRepository;
import ru.rkoks.repositories.AccountsRepositoryImpl;
import ru.rkoks.services.ProfileService;
import ru.rkoks.services.ProfileServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private ProfileService profileService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountsRepository accountsRepository = new AccountsRepositoryImpl(dataSource);

        this.profileService = new ProfileServiceImpl(accountsRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        AccountDto accountDto =
                profileService.getProfileData((Long) session.getAttribute(ProfileServiceImpl.DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME));

        request.setAttribute("accountData", accountDto);
        request.getRequestDispatcher("jsp/profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.removeAttribute(AuthenticationFilter.DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME);
        response.sendRedirect("/signIn");
    }
}
