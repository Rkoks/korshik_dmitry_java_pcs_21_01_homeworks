package com.some.repositories;

import com.some.model.Product;

import java.util.List;

/**
 * Хранилище продуктов
 */
public interface ProductsRepository {
    List<Product> findAll();
}
