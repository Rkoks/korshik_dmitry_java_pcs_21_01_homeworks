package ru.rkoks.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.rkoks.config.SqlConfigurator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import ru.rkoks.repositories.ProductsRepository;

import javax.sql.DataSource;

@Parameters(separators = "=")
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;
    public static void main(String[] args) {
        logger.info("Приложение \"Homework18 - Loggers\" запустилось");
        Main main = new Main();

        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);
        logger.debug("Parsed hikari-pool-size: " + main.poolSize);

        SqlConfigurator configurator = new SqlConfigurator();

        DataSource dataSource = configurator.getDataSource(main.poolSize);

        ProductsRepository repo = new ru.rkoks.repositories.ProductsRepositoryJdbcImpl(dataSource);

        System.out.println(repo.findAll());

        logger.info("Приложение \"Homework18 - Loggers\" завершилось");
    }
}
