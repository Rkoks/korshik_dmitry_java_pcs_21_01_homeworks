package ru.rkoks.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.rkoks.model.Product;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;


public class ProductsRepositoryJdbcImpl implements ProductsRepository {

    private static final Logger logger = LoggerFactory.getLogger(ProductsRepositoryJdbcImpl.class);
    
    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name, quantity, price from product order by id";

    private final DataSource dataSource;

    /**
     * Преобразует результат запроса из базы данных в объект модели Product
     */
    private static final Function<ResultSet, Product> productMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            Integer quantity = resultSet.getObject("quantity", Integer.class);
            Double price = resultSet.getObject("price", Double.class);

            return Product.builder()
                    .withId(id)
                    .withName(name)
                    .withQuantity(quantity)
                    .withPrice(price)
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public ProductsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Выполняет select на все продукты в БД с учётом limit <code>size</code> и offset <code>page * size</code>
     * сортировка ASC по значению id
     * @return список продуктов <code>ArrayList of Product</code>
     */
    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            logger.info("Connected to database"); //это попадёт только в файл

            try (ResultSet resultSet = statement.executeQuery()) {
                logger.debug("Query is executed: {}", SQL_SELECT_ALL); //это появится в консоли
                while (resultSet.next()) {
                    products.add(productMapper.apply(resultSet));
                }

                logger.trace("Connection closed"); //это не должно попасть ни в один лог
                return products;
            }
        } catch (SQLException e) {
            logger.error("Got SQLException: ", e); //при выбросе исключения это запишется в файл
            throw new IllegalArgumentException(e);
        }
    }
}
