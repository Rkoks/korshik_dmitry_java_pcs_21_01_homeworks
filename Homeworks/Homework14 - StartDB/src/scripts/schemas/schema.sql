drop table if exists buyer cascade;
create table buyer
(
    id      serial primary key,
    name    text,
    address text
);

drop table if exists product cascade;
create table product
(
    id       serial primary key,
    name     text,
    quantity integer,
    price    float
);

drop table if exists buy_order cascade;
create table buy_order
(
    id             serial primary key,
    formation_date timestamp,
    buyer_id       integer,
    foreign key (buyer_id) references buyer (id)
);

drop table if exists booking_product cascade;
create table booking_product
(
    id         serial primary key,
    count      integer,
    product_id integer,
    order_id   integer,
    foreign key (product_id) references product (id),
    foreign key (order_id) references buy_order (id)
);

drop table if exists booking_order cascade;
create table booking_order
(
    id              serial primary key,
    order_id        integer,
    expiration_date timestamp,
    foreign key (order_id) references buy_order (id)
);

drop table if exists bill cascade;
create table bill
(
    id               serial primary key,
    booking_order_id integer,
    payment_date     timestamp,
    foreign key (booking_order_id) references booking_order (id)
);



