-- самый большой чек в этом магазине
with paid_orders as (
    select order_id from booking_order bo inner join bill b on bo.id = b.booking_order_id
),
     quantity_prods_in_order as (
         select count, product_id, po.order_id from booking_product bp inner join paid_orders po on po.order_id = bp.order_id
     ),
     costs_of_prods_in_order as (
         select sum(count * price) as summary from quantity_prods_in_order qpio inner join product p on qpio.product_id = p.id group by qpio.order_id
     )
select max(summary) from costs_of_prods_in_order copio;