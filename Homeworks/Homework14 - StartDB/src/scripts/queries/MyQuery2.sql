--сколько денег потратил Пётр Петров с 03.10.2021 00:00 по 04.10.2021 00:00
with client_orders as (
    select bo.id
    from buy_order bo
             inner join buyer b on bo.buyer_id = b.id
    where b.name = 'Пётр Петров'
),
     bills_in_date as (
         select order_id
         from booking_order bo
                  inner join bill b on bo.id = b.booking_order_id
         where payment_date between '2021-10-03 00:00:00.000000)' and '2021-10-04 00:00:00.000000)'
     ),
     products_in_bills as (
         select product_id, count
         from booking_product bp
                  inner join client_orders co on bp.order_id = co.id
                  inner join bills_in_date bid on co.id = bid.order_id
     )
select sum(count * price) from products_in_bills pib inner join product p on pib.product_id = p.id;