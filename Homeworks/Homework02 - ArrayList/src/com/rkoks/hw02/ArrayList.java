package com.rkoks.hw02;

import java.util.Arrays;

public class ArrayList<E> implements List<E> {

    private E[] array;
    private int count = 0;

    public ArrayList() {
        this.array = (E[]) new Object[3];
    }

    @Override
    public void add(E element) {
        if (count == array.length) {
            array = Arrays.copyOf(array, count + count / 3);
        }
        array[count] = element;
        count++;

    }

    @Override
    public E get(int index) {
        return array[index];
    }

    private class ArrayListIterator implements Iterator<E> {

        int current = 0;

        @Override
        public boolean hasNext() {
            if (current < count) {
                return array[current] != null;
            }
            return false;
        }

        @Override
        public E next() {
            E element = array[current];
            current++;
            return element;
        }
    }


    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }
}
