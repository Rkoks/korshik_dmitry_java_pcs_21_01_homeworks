package com.rkoks.hw02;

public interface Iterator<E> {
    boolean hasNext();

    E next();
}
