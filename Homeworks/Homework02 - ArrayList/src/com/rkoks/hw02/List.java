package com.rkoks.hw02;

public interface List<E> {
    void add(E element);

    E get(int index);

    Iterator<E> iterator();
}
