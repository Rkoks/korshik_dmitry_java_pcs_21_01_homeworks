package reflection;

import annotations.Max;
import annotations.Min;
import annotations.MinMaxLength;
import annotations.NotEmpty;

import java.lang.reflect.Field;

public class Validator {

    /**
     * Проверяет все объявленные поля в классе объекта <code>object</code> на наличие аннотаций,
     * а также соответствие значений этих полей указанным аннотациям
     * @param object который необходимо проверить
     */
    public void validate(Object object) {
        Class<?> objClass = object.getClass();
        Field[] fields = objClass.getDeclaredFields();

        for (Field field : fields) {
            processNotEmpty(field, object);
            processMin(field, object);
            processMax(field, object);
            processMinMaxLength(field, object);

        }
    }

    /**
     * Проверяет поле на наличие аннотации <code>@NotEmpty</code>, если имеет, то проверяет на null или пустоту строки
     *
     * @param field  поле, которое проверяем
     * @param object в каком объекте проверяются значения поля <code>field</code>
     */
    private void processNotEmpty(Field field, Object object) {
        NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);

        if (notEmpty != null) {
            field.setAccessible(true);
            Object value;

            try {
                value = field.get(object);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }

            if (value == null) {
                throw new IllegalArgumentException("Поле " + field.getName() + " должно быть инициализировано");
            } else if (value.getClass().getName().equals("java.lang.String")) {
                if (((String) value).length() == 0) {
                    throw new IllegalArgumentException("Поле " + field.getName() + " должно быть непустым");
                }
            }
        }

    }

    /**
     * Проверяет значение численного поля <code>field</code> по нижнему порогу,
     * если у поля присутствует аннотация <code>@Min</code>
     * Приводит значение порога и поля к типу double
     *
     * @param field  поле, которое проверяем
     * @param object в каком объекте проверяются значения поля <code>field</code>
     */
    private void processMin(Field field, Object object) {
        Min min = field.getAnnotation(Min.class);

        if (min != null) {
            double minValue = Double.parseDouble(min.value());
            field.setAccessible(true);

            try {
                if (field.getDouble(object) < minValue) {
                    throw new IllegalArgumentException("Значение поля " + field.getName() + " должно быть не ниже "
                            + minValue);
                }
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }

        }
    }

    /**
     * Проверяет значение численного поля <code>field</code> по верхнему порогу,
     * если у поля присутствует аннотация <code>@Max</code>
     * Приводит значение порога и поля к типу double
     *
     * @param field  поле, которое проверяем
     * @param object в каком объекте проверяются значения поля <code>field</code>
     */
    private void processMax(Field field, Object object) {
        Max max = field.getAnnotation(Max.class);

        if (max != null) {
            double maxValue = Double.parseDouble(max.value());
            field.setAccessible(true);

            try {
                if (field.getDouble(object) > maxValue) {
                    throw new IllegalArgumentException("Значение поля " + field.getName() + " должно быть не выше "
                            + maxValue);
                }
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }

        }
    }

    /**
     * Проверяет длину строкового поля <code>field</code> по нижнему и верхнему порогам,
     * если у поля присутствует аннотация <code>@MinMaxLength</code>
     *
     * @param field  поле, которое проверяем
     * @param object в каком объекте проверяются значения поля <code>field</code>
     */
    private void processMinMaxLength(Field field, Object object) {
        MinMaxLength minMaxLength = field.getAnnotation(MinMaxLength.class);

        if (minMaxLength != null) {
            int minLength = minMaxLength.min();
            int maxLength = minMaxLength.max();

            String value;
            try {
                value = (String) field.get(object);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }

            int length = value.length();
            if (length < minLength || length > maxLength) {
                throw new IllegalArgumentException("Значение поля " + field.getName() + " должно иметь длину от "
                        + minLength + " до " + maxLength);
            }
        }
    }
}
