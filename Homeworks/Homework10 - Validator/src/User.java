import annotations.Max;
import annotations.Min;
import annotations.MinMaxLength;
import annotations.NotEmpty;

public class User {
    @NotEmpty
    @Min("20")
    @Max("35")
    int steps;

    @NotEmpty
    @Min("20")
    @Max("35")
    long path;

    @NotEmpty
    @Min("20")
    @Max("35")
    float age;

    @NotEmpty
    @MinMaxLength(min = 5, max = 10)
    String name;

}
