import reflection.Validator;

public class Main {
    public static void main(String[] args) {
        //сделал у юзера поля с дефолтными модификаторами доступа для упрощения проверки
        User user = new User();
        user.steps = 30;
        user.path = 25;
        user.age = 20;
        user.name = "212345";

        Validator validator = new Validator();
        validator.validate(user);

    }
}
