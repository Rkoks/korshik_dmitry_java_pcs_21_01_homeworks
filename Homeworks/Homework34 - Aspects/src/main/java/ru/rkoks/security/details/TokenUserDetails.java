package ru.rkoks.security.details;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import ru.rkoks.models.Account;

import java.util.Collection;
import java.util.Collections;

@RequiredArgsConstructor
public class TokenUserDetails implements UserDetails {
    private final DecodedJWT decodedJWT;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = decodedJWT.getClaim("role").asString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !decodedJWT.getClaim("state").toString().equals(Account.State.BANNED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return decodedJWT.getClaim("state").asString().equals(Account.State.CONFIRMED);
    }
}
