package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "method")
public class MethodsCallStatistic {

    @Id
    private String name;
    private Long callCount;
}
