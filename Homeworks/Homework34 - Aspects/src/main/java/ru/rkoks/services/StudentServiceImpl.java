package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.CoursesResponse;
import ru.rkoks.dto.StudentDto;
import ru.rkoks.models.Course;
import ru.rkoks.models.Student;
import ru.rkoks.repositories.CoursesRepository;
import ru.rkoks.repositories.StudentsRepository;

import java.util.List;

import static ru.rkoks.dto.CourseDto.from;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentsRepository studentsRepository;
    private final CoursesRepository coursesRepository;

    @Override
    public List<StudentDto> getStudents(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Student> students = studentsRepository.findAllByIsDeletedIsNull(request);
        return StudentDto.from(students.getContent());
    }

    @Override
    public StudentDto addStudent(StudentDto student) {
        Student newStudent = Student.builder()
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .build();
        studentsRepository.save(newStudent);

        return StudentDto.from(newStudent);
    }

    @Override
    public StudentDto updateStudent(Long studentId, StudentDto student) {
        Student existingStudent = studentsRepository.getById(studentId);
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        studentsRepository.save(existingStudent);
        return StudentDto.from(existingStudent);
    }

    @Override
    public void deleteStudent(Long studentId) {
        Student student = studentsRepository.getById(studentId);
        student.setIsDeleted(true);
        studentsRepository.save(student);
    }

    @Override
    public CoursesResponse addCourseToStudent(Long studentId, CourseDto course) {
        Student student = studentsRepository.getById(studentId);
        Course existedCourse = coursesRepository.getById(course.getId());
        student.getCourses().add(existedCourse);

        studentsRepository.save(student);
        return CoursesResponse.builder()
                .data(from(student.getCourses()))
                .build();
    }

}
