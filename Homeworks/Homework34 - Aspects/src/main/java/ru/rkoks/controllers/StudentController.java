package ru.rkoks.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rkoks.dto.CourseDto;
import ru.rkoks.dto.CoursesResponse;
import ru.rkoks.dto.StudentDto;
import ru.rkoks.dto.StudentsResponse;
import ru.rkoks.services.StudentService;

import java.time.LocalDateTime;

@RestController
@RequestMapping("api/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<StudentsResponse> getStudents(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .header("DateTime", LocalDateTime.now().toString())
                .body(StudentsResponse.builder().data(studentService.getStudents(page, size)).build());

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto addStudent(@RequestBody StudentDto student) {
        return studentService.addStudent(student);
    }

    @PutMapping("/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public StudentDto updateStudent(@PathVariable("student-id") Long studentId, @RequestBody StudentDto student) {
        return studentService.updateStudent(studentId, student);
    }

    @DeleteMapping("/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable("student-id") Long studentId) {
        studentService.deleteStudent(studentId);
    }

    @PostMapping( "/{student-id}/courses")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CoursesResponse> addCourseToStudent(@PathVariable("student-id") Long studentId,
                                                              @RequestBody CourseDto course){
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDateTime.now().toString()))
                .body(studentService.addCourseToStudent(studentId, course));
    }
}
