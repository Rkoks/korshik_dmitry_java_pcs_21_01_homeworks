package ru.rkoks.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.rkoks.model.Product;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductsRepositoryJdbcImplNormalTest {

    private ProductsRepositoryJdbcImpl productsRepositoryJdbc;

    private DataSource dataSource;

    //Подготовка БД в памяти для проверки нормальной работы select
    public void setUp() {
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();

        productsRepositoryJdbc = new ProductsRepositoryJdbcImpl(dataSource);
    }

    public List<Product> setUpResult(){
        List<Product> result = new ArrayList<>();
        //Первый продукт согласно schema.sql
        Product product1 = Product.builder()
                .withId(1L)
                .withName("Хлеб")
                .withQuantity(15)
                .withPrice(24.5)
                .build();
        result.add(product1);

        Product product2 = Product.builder()
                .withId(2L)
                .withName("Шоколад")
                .withQuantity(10)
                .withPrice(78.3)
                .build();
        result.add(product2);

        Product product3 = Product.builder()
                .withId(3L)
                .withName("Молоко")
                .withQuantity(25)
                .withPrice(60.0)
                .build();
        result.add(product3);

        Product product4 = Product.builder()
                .withId(4L)
                .withName("Яблоки")
                .withQuantity(120)
                .withPrice(14.7)
                .build();
        result.add(product4);

        return result;
    }
    //Проверка нормальной работы select
    @Test
    public void findAllTest() {
        setUp();
        assertEquals(setUpResult(), productsRepositoryJdbc.findAll());

    }
}