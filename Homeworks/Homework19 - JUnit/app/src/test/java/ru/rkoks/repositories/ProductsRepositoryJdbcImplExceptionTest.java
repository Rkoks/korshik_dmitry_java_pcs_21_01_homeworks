package ru.rkoks.repositories;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductsRepositoryJdbcImplExceptionTest {

    private ProductsRepositoryJdbcImpl productsRepositoryJdbc;

    private DataSource dataSource;

    //Подготовка таблиц для выброса исключений в select
    public void setUpForThrows(String schema, String data) {
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript(schema)
                .addScript(data)
                .build();

        productsRepositoryJdbc = new ProductsRepositoryJdbcImpl(dataSource);
    }

    //Массив с именами файлов для различных проверок
    static String[] forTest(){

        String[] strings = new String[2];
        strings[0] = "throws/schemaInTable.sql,throws/dataInTable.sql"; //исключение при попытке подключиться к бд
        strings[1] = "throws/schemaInColumn.sql,throws/dataInColumn.sql"; //исключение при преобразовании результатов из бд

        return strings;
    }

    //проверка выброса исключений
    @ParameterizedTest
    @MethodSource("forTest")
    public void throwsInFindAll(String list) {
        String[] names = list.split(",");
        setUpForThrows(names[0], names[1]);
        assertThrows(IllegalArgumentException.class, () -> {
            productsRepositoryJdbc.findAll();
        });
    }
}