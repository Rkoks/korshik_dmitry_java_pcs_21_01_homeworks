drop table if exists product1;
drop table if exists product;
create table product1 (
    id bigint auto_increment,
    name varchar(255),
    quantity int,
    price double
);
