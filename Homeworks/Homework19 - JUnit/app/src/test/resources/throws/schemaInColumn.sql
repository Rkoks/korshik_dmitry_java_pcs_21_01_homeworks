drop table if exists product;
create table product (
    id bigint auto_increment,
    name varchar(255),
    quantity int,
    price varchar(255)
);
