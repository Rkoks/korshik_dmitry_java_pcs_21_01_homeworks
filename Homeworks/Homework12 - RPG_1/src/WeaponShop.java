import weapon.Weapon;

/**
 * Придумал как использовать Prototype - покупка оружия в магазине
 */
public class WeaponShop {
    public Weapon buyWeapon(Weapon weapon) {
       return weapon.clone();

    }

}
