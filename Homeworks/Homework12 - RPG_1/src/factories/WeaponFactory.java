package factories;

import weapon.bows.Bow;
import weapon.swords.Sword;

/**
 * Абстрактная фабрика для оружия с созданием двух продуктов
 */
public interface WeaponFactory {
    Bow createBow();

    Sword createSword();
}
