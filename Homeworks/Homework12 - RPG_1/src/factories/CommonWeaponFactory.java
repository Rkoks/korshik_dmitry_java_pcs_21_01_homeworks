package factories;

import weapon.bows.Bow;
import weapon.bows.CommonBow;
import weapon.swords.CommonSword;
import weapon.swords.Sword;

public class CommonWeaponFactory implements WeaponFactory {

    /**
     * Создаёт <code>Bow</code> через <code>Builder</code> с дефолтными значениями
     * @return CommonBow со значениями по умолчанию
     */
    @Override
    public Bow createBow() {
        CommonBow bow = new CommonBow.Builder()
                .withAmmo(35)
                .withDamage(120f)
                .withAccuracy(0.6f)
                .withRange(350)
                .withSpeed(1.2f)
                .withEnduranceCost(10)
                .build();
        return bow;
    }

    /**
     * Создаёт <code>Sword</code> через <code>Builder</code> с дефолтными значениями
     * @return CommonSword со значениями по умолчанию
     */
    @Override
    public Sword createSword() {
        CommonSword sword = new CommonSword.Builder()
                .isTwoHanded(false)
                .withBlockChance(0.33f)
                .withDamage(60)
                .withEnduranceCost(15)
                .withSpeed(1f)
                .build();
        return sword;
    }
}
