package factories;

import weapon.bows.Bow;
import weapon.bows.EnchantedBow;
import weapon.swords.EnchantedSword;
import weapon.swords.Sword;

public class EnchantedWeaponFactory implements WeaponFactory {
    /**
     * Создаёт <code>Bow</code> через <code>Builder</code> с дефолтными значениями
     * @return EnchantedBow со значениями по умолчанию
     */
    @Override
    public Bow createBow() {
        EnchantedBow bow = new EnchantedBow.Builder()
                .withAccuracy(0.6f)
                .withAmmo(35)
                .withDamage(105)
                .withMagicDamage(65)
                .withManaCost(15)
                .withRange(450)
                .withSpeed(1.2f)
                .build();
        return bow;
    }

    /**
     * Создаёт <code>Sword</code> через <code>Builder</code> с дефолтными значениями
     * @return EnchantedSword со значениями по умолчанию
     */
    @Override
    public Sword createSword() {
        EnchantedSword sword = new EnchantedSword.Builder()
                .isTwoHanded(true)
                .withBlockChance(0.5f)
                .withDamage(50)
                .withMagicDamage(55)
                .withManaCost(20)
                .withSpeed(1)
                .build();
        return sword;
    }
}
