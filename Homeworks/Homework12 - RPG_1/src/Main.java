import factories.CommonWeaponFactory;
import factories.EnchantedWeaponFactory;
import factories.WeaponFactory;
import weapon.bows.Bow;
import weapon.swords.Sword;

public class Main {
    public static void main(String[] args) {

        WeaponFactory comFactory = new CommonWeaponFactory();

        Bow bow = comFactory.createBow();
        System.out.println(bow);

        WeaponFactory enchFactory = new EnchantedWeaponFactory();
        Sword sword = comFactory.createSword();
        System.out.println(sword);


        WeaponShop shop = new WeaponShop();
        Bow bowFromShop = (Bow) shop.buyWeapon(bow);
        System.out.println(bowFromShop);
        System.out.println(bow == bowFromShop); //показать, что это разные объекты



    }
}
