package weapon.bows;

public class CommonBow implements Bow {
    private float damage;
    private int ammo;
    private float accuracy;
    private int enduranceCost;
    private float speed;
    private int range;

    private CommonBow(Builder builder) {
        this.damage = builder.damage;
        this.ammo = builder.ammo;
        this.accuracy = builder.accuracy;
        this.enduranceCost = builder.enduranceCost;
        this.speed = builder.speed;
        this.range = builder.range;
    }

    //Реализация паттерна Prototype
    @Override
    public CommonBow clone() {
        CommonBow clone = CommonBow.builder()
                .withAccuracy(this.accuracy)
                .withAmmo(this.ammo)
                .withDamage(this.damage)
                .withEnduranceCost(this.enduranceCost)
                .withRange(this.range)
                .withSpeed(this.speed)
                .build();
        return clone;
    }

    //Реализация паттерна Builder
    public static class Builder {
        private float damage;
        private int ammo;
        private float accuracy;
        private int enduranceCost;
        private float speed;
        private int range;

        public Builder withDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public Builder withAmmo(int ammo) {
            this.ammo = ammo;
            return this;
        }

        public Builder withAccuracy(float accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder withEnduranceCost(int enduranceCost) {
            this.enduranceCost = enduranceCost;
            return this;
        }

        public Builder withSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        public Builder withRange(int range) {
            this.range = range;
            return this;
        }

        public CommonBow build() {
            return new CommonBow(this);

        }

    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public float shot() {
        return damage;
    }

    @Override
    public int reload() {
        if (ammo >= 5) {
            ammo -= 5;
            return 5;
        } else if (ammo > 0) {
            int tempAmmo = ammo;
            ammo = 0;
            return tempAmmo;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "CommonBow{" +
                "damage=" + damage +
                ", ammo=" + ammo +
                ", accuracy=" + accuracy +
                ", enduranceCost=" + enduranceCost +
                ", speed=" + speed +
                ", range=" + range +
                '}';
    }
}
