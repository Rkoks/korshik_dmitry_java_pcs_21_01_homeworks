package weapon.bows;

import weapon.Weapon;

public interface Bow extends Weapon {
    /**
     * произвести выстрел
     *
     * @return true при попадании, false при промахе
     */
    float shot();

    /**
     * перезарядиться
     *
     * @return количество заряженных боеприпасов
     */
    int reload();


}
