package weapon.bows;

public class EnchantedBow implements Bow {
    private float damage;
    private float magicDamage;
    private int ammo;
    private float accuracy;
    private int manaCost;
    private float speed;
    private int range;

    private EnchantedBow(Builder builder) {
        this.damage = builder.damage;
        this.magicDamage = builder.magicDamage;
        this.ammo = builder.ammo;
        this.accuracy = builder.accuracy;
        this.manaCost = builder.manaCost;
        this.speed = builder.speed;
        this.range = builder.range;
    }

    //Реализация паттерна Prototype
    @Override
    public EnchantedBow clone() {
        EnchantedBow clone = EnchantedBow.builder()
                .withAccuracy(this.accuracy)
                .withAmmo(this.ammo)
                .withMagicDamage(this.magicDamage)
                .withManaCost(this.manaCost)
                .withDamage(this.damage)
                .withRange(this.range)
                .withSpeed(this.speed)
                .build();
        return clone;
    }

    //Реализация паттерна Builder
    public static class Builder {
        private float damage;
        private float magicDamage;
        private int ammo;
        private float accuracy;
        private int manaCost;
        private float speed;
        private int range;

        public Builder withDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public Builder withMagicDamage(float magicDamage) {
            this.magicDamage = magicDamage;
            return this;
        }

        public Builder withAmmo(int ammo) {
            this.ammo = ammo;
            return this;
        }

        public Builder withAccuracy(float accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder withManaCost(int manaCost) {
            this.manaCost = manaCost;
            return this;
        }

        public Builder withSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        public Builder withRange(int range) {
            this.range = range;
            return this;
        }

        public EnchantedBow build() {
            return new EnchantedBow(this);

        }

    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public float shot() {
        return damage + magicDamage;
    }

    @Override
    public int reload() {
        if (ammo >= 5) {
            ammo -= 5;
            return 5;
        } else if (ammo > 0) {
            int tempAmmo = ammo;
            ammo = 0;
            return tempAmmo;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "EnchantedBow{" +
                "damage=" + damage +
                ", magicDamage=" + magicDamage +
                ", ammo=" + ammo +
                ", accuracy=" + accuracy +
                ", manaCost=" + manaCost +
                ", speed=" + speed +
                ", range=" + range +
                '}';
    }
}
