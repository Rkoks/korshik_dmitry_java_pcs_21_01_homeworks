package weapon;

/**
 * Обобщающий интерфейс для оружия и реализация паттерна Прототип
 */
public interface Weapon {
    Weapon clone();
}
