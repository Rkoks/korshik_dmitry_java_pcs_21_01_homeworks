package weapon.swords;

public class EnchantedSword implements Sword {
    private float damage;
    private float magicDamage;
    private float speed;
    private int manaCost;
    private float blockChance;
    private boolean isTwoHanded;

    private EnchantedSword(Builder builder) {
        this.damage = builder.damage;
        this.magicDamage = builder.magicDamage;
        this.speed = builder.speed;
        this.manaCost = builder.manaCost;
        this.blockChance = builder.blockChance;
        this.isTwoHanded = builder.isTwoHanded;
    }

    //Реализация паттерна Prototype
    @Override
    public EnchantedSword clone() {
        EnchantedSword clone = EnchantedSword.builder()
                .isTwoHanded(this.isTwoHanded)
                .withBlockChance(this.blockChance)
                .withDamage(this.damage)
                .withMagicDamage(this.magicDamage)
                .withManaCost(this.manaCost)
                .withSpeed(this.speed)
                .build();
        return clone;
    }

    //Реализация паттерна Builder
    public static class Builder {
        private float damage;
        private float magicDamage;
        private float speed;
        private int manaCost;
        private float blockChance;
        private boolean isTwoHanded;

        public Builder withDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public Builder withMagicDamage(float magicDamage) {
            this.magicDamage = magicDamage;
            return this;
        }

        public Builder withSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        public Builder withManaCost(int manaCost) {
            this.manaCost = manaCost;
            return this;
        }

        public Builder withBlockChance(float blockChance) {
            this.blockChance = blockChance;
            return this;
        }

        public Builder isTwoHanded(boolean isTwoHanded) {
            this.isTwoHanded = isTwoHanded;
            return this;
        }

        public EnchantedSword build() {
            return new EnchantedSword(this);

        }

    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public float cut() {
        return damage + magicDamage;
    }

    @Override
    public float stab() {
        return 0.5f * damage + magicDamage;
    }

    @Override
    public boolean block() {
        return Math.random() >= blockChance;
    }

    @Override
    public String toString() {
        return "EnchantedSword{" +
                "damage=" + damage +
                ", magicDamage=" + magicDamage +
                ", speed=" + speed +
                ", manaCost=" + manaCost +
                ", blockChance=" + blockChance +
                ", isTwoHanded=" + isTwoHanded +
                '}';
    }
}
