package weapon.swords;

import weapon.Weapon;

public interface Sword extends Weapon {
    /**
     * произвести рубящий удар
     *
     * @return количество нанесённого урона
     */
    float cut();

    /**
     * произвести колющий удар
     *
     * @return количество нанесённого урона
     */
    float stab();

    /**
     * блокировать получаемый урон
     *
     * @return true в случае успешной защиты, false при неудачной попытке
     */
    boolean block();

}
