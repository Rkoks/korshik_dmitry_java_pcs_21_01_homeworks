package weapon.swords;

public class CommonSword implements Sword {
    private float damage;
    private float speed;
    private int enduranceCost;
    private float blockChance;
    private boolean isTwoHanded;

    private CommonSword(Builder builder) {
        this.damage = builder.damage;
        this.speed = builder.speed;
        this.enduranceCost = builder.enduranceCost;
        this.blockChance = builder.blockChance;
        this.isTwoHanded = builder.isTwoHanded;
    }

    //Реализация паттерна Prototype
    @Override
    public CommonSword clone() {
        CommonSword clone = CommonSword.builder()
                .isTwoHanded(this.isTwoHanded)
                .withBlockChance(this.blockChance)
                .withDamage(this.damage)
                .withEnduranceCost(this.enduranceCost)
                .withSpeed(this.speed)
                .build();
        return clone;
    }

    //Реализация паттерна Builder
    public static class Builder {
        private float damage;
        private float speed;
        private int enduranceCost;
        private float blockChance;
        private boolean isTwoHanded;

        public Builder withDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public Builder withSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        public Builder withEnduranceCost(int enduranceCost) {
            this.enduranceCost = enduranceCost;
            return this;
        }

        public Builder withBlockChance(float blockChance) {
            this.blockChance = blockChance;
            return this;
        }

        public Builder isTwoHanded(boolean isTwoHanded) {
            this.isTwoHanded = isTwoHanded;
            return this;
        }

        public CommonSword build() {
            return new CommonSword(this);

        }

    }

    public static Builder builder() {
        return new Builder();
    }


    @Override
    public float cut() {
        return damage;
    }

    @Override
    public float stab() {
        return 0.5f * damage;
    }

    @Override
    public boolean block() {
        return Math.random() >= blockChance;
    }

    @Override
    public String toString() {
        return "CommonSword{" +
                "damage=" + damage +
                ", speed=" + speed +
                ", enduranceCost=" + enduranceCost +
                ", blockChance=" + blockChance +
                ", isTwoHanded=" + isTwoHanded +
                '}';
    }
}
