import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        char[] input = StringGenerator.generate().toCharArray();

        HashMap<Character, Integer> hashMap = new HashMap<>();

        count(hashMap, input);

        System.out.println(hashMap);
    }

    public static void count(HashMap<Character, Integer> hashMap, char[] array){
        for (int i = 0; i < array.length; i++) {
            char current = array[i];
            if (hashMap.containsKey(current)) {
                int count = hashMap.get(current);
                hashMap.put(current, ++count);
            } else {
                hashMap.put(current, 1);
            }
        }
    }
}
