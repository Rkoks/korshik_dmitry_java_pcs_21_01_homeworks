package ru.rkoks.config.i18n;

import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;
import ru.rkoks.models.Currency;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LocaleChangeInterceptorWithCurrency extends LocaleChangeInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException {
        boolean result = super.preHandle(request, response, handler);
//        String newLocale = request.getParameter(this.getParamName());
        String newLocale = RequestContextUtils.getLocaleResolver(request).resolveLocale(request).toString();
        Currency.setLocale(newLocale);
        return result;
    }

}
