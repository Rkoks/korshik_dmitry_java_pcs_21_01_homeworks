package ru.rkoks.validation;

import ru.rkoks.dto.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotSameNamesValidator implements ConstraintValidator<NotSameNames, SignUpForm> {
    @Override
    public boolean isValid(SignUpForm form, ConstraintValidatorContext context) {

        return !form.getFirstName().equals(form.getLastName());
    }
}

