package ru.rkoks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.rkoks.models.Currency;

import static ru.rkoks.models.Currency.fromString;

@SpringBootApplication
public class Homework31SpringBootSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework31SpringBootSecurityApplication.class, args);

////        Currency rubles = new Currency(1_000_000_03L, Currency.CurrencyName.RUB);
//        System.out.println(fromString("10.25 rub"));
//        System.out.println(fromString("10,5 rub"));
//        System.out.println(fromString("1025 rub"));
//        System.out.println(fromString("10.253"));
////        System.out.println(fromString("10.25 "));
//        System.out.println(fromString("1025"));
////        System.out.println(fromString(".1025"));
////        System.out.println(fromString("10.1 asdqw"));
////        System.out.println(fromString("10.12 as"));

    }

}
