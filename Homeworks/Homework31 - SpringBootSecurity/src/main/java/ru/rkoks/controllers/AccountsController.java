package ru.rkoks.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.rkoks.services.AccountsService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountsService accountsService;

    @GetMapping
    public String getAccountsPage(Model model) {
        model.addAttribute("accounts", accountsService.getAllAccounts());
        return "accounts";
    }

    @PostMapping("/{account-id}/delete")
    public String deleteAccount(@PathVariable("account-id") Long accountId) {
        accountsService.deleteAccount(accountId);
        return "redirect:/accounts";
    }

}
