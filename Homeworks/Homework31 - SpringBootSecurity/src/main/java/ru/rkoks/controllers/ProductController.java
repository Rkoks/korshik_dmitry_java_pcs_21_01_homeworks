package ru.rkoks.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import ru.rkoks.dto.ProductDto;
import ru.rkoks.services.ProductsService;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@Controller
@RequestMapping("/products")
public class ProductController {
    private final ProductsService productsService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String getProductPage(Model model, Locale locale) {
        System.out.println(locale.toString());
        if (!model.containsAttribute("productDto")) {
            model.addAttribute("productDto", new ProductDto());
        }
        model.addAttribute("products", productsService.getAll());
        return "productPage";
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String addProduct(@Valid ProductDto product, BindingResult result, Model model, Locale locale) {
        if (result.hasErrors()){
            model.addAttribute("productDto", product);
        } else {
            productsService.addProduct(product);
            model.addAttribute("productDto", new ProductDto());
        }

        return getProductPage(model, locale);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public List<ProductDto> deleteProduct(@RequestBody ProductDto product) {
        productsService.deleteProduct(product.getId());
        return productsService.getAll();
    }
}
