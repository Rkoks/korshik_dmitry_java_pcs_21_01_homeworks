package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.List;

import static ru.rkoks.dto.AccountDto.from;

@RequiredArgsConstructor
@Service
public class AccountsServiceImpl implements AccountsService {
    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> getAllAccounts() {

        return from(accountsRepository.findAllByStateOrderById(Account.State.CONFIRMED));
    }

    @Override
    public void deleteAccount(Long accountId) {
        Account account = accountsRepository.getById(accountId);
        account.setState(Account.State.DELETED);
        accountsRepository.save(account);
    }
}
