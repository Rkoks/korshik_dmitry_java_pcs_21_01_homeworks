package ru.rkoks.services;

import ru.rkoks.dto.ProductDto;

import java.util.List;

public interface ProductsService {
    List<ProductDto> getAll();

    ProductDto addProduct(ProductDto productDto);

    void deleteProduct(Long id);
}
