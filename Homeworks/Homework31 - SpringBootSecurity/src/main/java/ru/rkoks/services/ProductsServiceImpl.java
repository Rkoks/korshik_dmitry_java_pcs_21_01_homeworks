package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.ProductDto;
import ru.rkoks.models.Product;
import ru.rkoks.repositories.ProductsRepository;

import java.util.List;

import static ru.rkoks.dto.ProductDto.from;
import static ru.rkoks.models.Currency.fromString;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> getAll() {
        return from(productsRepository.findAllByIsDeletedIsNullOrderById());
    }

    @Override
    public ProductDto addProduct(ProductDto product) {
        Product newProduct = Product.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(fromString(product.getPrice()))
                .count(product.getCount())
                .build();
        productsRepository.save(newProduct);
        return from(newProduct);
    }

    @Override
    public void deleteProduct(Long id) {
        Product product = productsRepository.getById(id);
        product.setIsDeleted(true);
        productsRepository.save(product);
    }
}
