package ru.rkoks.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.rkoks.repositories.AccountsRepository;
import ru.rkoks.security.details.AccountUserDetails;

@Service
@RequiredArgsConstructor
public class AccountUserDetailsService implements UserDetailsService {
    private final AccountsRepository accountsRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        Optional<Account> accountOptional = accountsRepository.findByEmail(email);
//        Account account = accountOptional.orElseThrow(() -> new UsernameNotFoundException("User not found"));
//        AccountUserDetails accountUserDetails = new AccountUserDetails(account);
//        return accountUserDetails;
        // то же, что и выше, но в одну строчку
        return new AccountUserDetails(accountsRepository.findByEmail(email).
                orElseThrow(
                        () -> new UsernameNotFoundException("User not found")));
    }
}
