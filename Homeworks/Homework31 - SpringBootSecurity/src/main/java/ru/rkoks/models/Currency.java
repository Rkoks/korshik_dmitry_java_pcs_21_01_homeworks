package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.web.servlet.LocaleResolver;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Currency {
    private static CurrencyName locale = CurrencyName.USD;

    public enum CurrencyName {
        RUB(7290L), USD(100L);
        //значение курса валют до 1/100;
        private Long rateCount;

        public Long getRateCount() {
            return rateCount;
        }

        CurrencyName(Long value) {
            rateCount = value;
        }

    }

    @Id
    @GeneratedValue
    private Long id;
    private Long ceiling;
    private Long floor;
    private CurrencyName name;

    private Currency(Long ceiling, Long floor, CurrencyName name) {
        this.ceiling = ceiling;
        this.floor = floor;
        this.name = name;
    }

    public static Currency fromString(String currency) {
        if (currency.matches("((\\d*(,|.)\\d{1,2})|(\\d*))( \\w{3})?")) {
            String[] parts = new String[3];

            int spaceIndex = 0;
            if (currency.contains(" ")) {
                spaceIndex = currency.lastIndexOf(' ');
            } else {
                spaceIndex = currency.length();
            }

            int sep = 0;
            if (currency.contains(",")) {
                sep = currency.indexOf(",");
            } else if (currency.contains(".")) {
                sep = currency.indexOf(".");
            }

            if (sep != 0) {
                parts[0] = currency.substring(0, sep);
                parts[1] = currency.substring(sep + 1, spaceIndex).trim();
            } else {
                parts[0] = currency.substring(0, spaceIndex);
                parts[1] = null;
            }
            if (spaceIndex != currency.length()) {
                parts[2] = currency.substring(spaceIndex + 1);
            } else {
                parts[2] = CurrencyName.RUB.name();
            }

            return fromArray(parts);
        }

        return new Currency(0L, 0L, CurrencyName.RUB);
    }

    private static Currency fromArray(String[] arr) {
        Long ceiling = Long.parseLong(arr[0]);
        Long floor;
        if (arr[1] == null) {
            floor = 0L;
        } else if (arr[1].length() == 1) {
            floor = Long.parseLong(arr[1]) * 10;
        } else {
            floor = Long.parseLong(arr[1]);
        }

        CurrencyName name = null;
        for (CurrencyName cur : CurrencyName.values()) {
            if (cur.name().equalsIgnoreCase(arr[2])) {
                name = cur;
                break;
            }
        }

        return new Currency(ceiling, floor, name);
    }

    @Override
    public String toString() {
        if (locale == this.name) {
            return String.format("%,d", ceiling) + "," + String.format("%02d", floor) + " " + name.name();
        } else {
            Currency currency = exchangeCurrency(this, locale);
            return String.format("%,d", currency.ceiling) + "," + String.format("%02d", currency.floor) + " " + currency.name.name();
        }
    }

    public static Currency exchangeCurrency(Currency amount, CurrencyName to) {
        if (amount.name == to) {
            return amount;
        } else {
            Long currentAmount = amount.ceiling * 100 + amount.floor;
            Long newAmount = currentAmount * to.rateCount / amount.name.rateCount;
            Long newCeiling = newAmount / 100;
            Long newFloor = newAmount % 100;
            return new Currency(newCeiling, newFloor, to);
        }
    }
    public static void setLocale(String locale) {
        if (locale.equalsIgnoreCase("EN")) {
            Currency.locale = CurrencyName.USD;
        } else {
            Currency.locale = CurrencyName.RUB;
        }
    }
}
