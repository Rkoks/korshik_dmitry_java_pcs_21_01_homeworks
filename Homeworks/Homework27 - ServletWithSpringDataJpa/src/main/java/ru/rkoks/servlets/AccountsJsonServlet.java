package ru.rkoks.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.dto.SignUpForm;
import ru.rkoks.services.AccountsService;
import ru.rkoks.services.SignUpService;

import java.io.IOException;
import java.util.List;

@WebServlet("/accounts/json")
public class AccountsJsonServlet extends HttpServlet {
    private AccountsService accountsService;
    private SignUpService signUpService;
    private ObjectMapper objectMapper;

    /**
     * Инициализируем сервис работы с данными
     *
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");

        accountsService = context.getBean(AccountsService.class);
        signUpService = context.getBean(SignUpService.class);
        objectMapper = context.getBean(ObjectMapper.class);
    }

    // находим аккаунты, email которых содержит полученное в request значение
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<AccountDto> accounts = accountsService.searchByEmail(request.getParameter("email"));

        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(accounts);

        response.getWriter().println(json);
    }

    //добавляем аккаунт через админку
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String body = request.getReader().readLine();
        SignUpForm signUpForm = objectMapper.readValue(body, SignUpForm.class);
        signUpService.signUp(signUpForm);

        response.getWriter().println(objectMapper.writeValueAsString(accountsService.getAll()));
    }
}
