package ru.rkoks.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.services.ProfileService;
import ru.rkoks.services.ProfileServiceImpl;

import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private ProfileService profileService;

    /**
     * Инициализируем сервис работы с данными
     *
     * @param config конфигурационный файл для доступа к контексту сервлетов
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        profileService = context.getBean(ProfileService.class);
    }

    //получаем информацию о текущем пользователе и передаём на сервлет отображения профиля
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        AccountDto accountDto =
                profileService.getProfileData((Long) session.getAttribute(ProfileServiceImpl.DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME));

        request.setAttribute("accountData", accountDto);
        request.getRequestDispatcher("jsp/profile.jsp").forward(request, response);
    }

    //выполняем выход пользователя из системы и перенаправляем на страницу входа
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(true);
        session.invalidate();
        response.sendRedirect("signIn");
    }
}
