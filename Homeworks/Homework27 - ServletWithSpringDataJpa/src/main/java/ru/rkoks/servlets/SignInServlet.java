package ru.rkoks.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.rkoks.dto.SignInForm;
import ru.rkoks.filters.AuthenticationFilter;
import ru.rkoks.services.ProfileServiceImpl;
import ru.rkoks.services.SignInService;

import java.io.IOException;
import java.util.Optional;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignInService signInService;


    /**
     * вызывается при первом обращении к сервлету
     * Инициализируем необходимые объекты
     */
    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        signInService = context.getBean(SignInService.class);
    }

    /**
     * Перенаправляет запрос на сервлет signIn.jsp форма входа в систему
     *
     * @param request  запрос
     * @param response ответ
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/signIn.jsp").forward(request, response);

    }

    //обрабатываем запрос аутентификации пользователя
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SignInForm form = SignInForm.builder()
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();

        Optional<Long> accountId = signInService.doAuthenticate(form);
        if (accountId.isPresent()) {
            HttpSession session = request.getSession(true);
            session.setAttribute(AuthenticationFilter.DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME, true);
            session.setAttribute(ProfileServiceImpl.DEFAULT_ACCOUNT_ID_ATTRIBUTE_NAME, accountId.get());

            response.sendRedirect("profile");
        } else {

            response.sendRedirect("signIn?error");
        }
    }
}
