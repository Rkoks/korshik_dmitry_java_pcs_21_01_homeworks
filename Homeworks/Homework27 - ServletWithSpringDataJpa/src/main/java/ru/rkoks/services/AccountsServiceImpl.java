package ru.rkoks.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.AccountDto;
import ru.rkoks.repositories.AccountsRepository;

import java.util.List;

import static ru.rkoks.dto.AccountDto.from;

@Service
public class AccountsServiceImpl implements AccountsService {
    private final AccountsRepository accountsRepository;

    @Autowired
    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Получает из репозитория всех зарегистрированных пользователей
     *
     * @return список найденных пользователей с доступной для клиента информацией
     */
    @Override
    public List<AccountDto> getAll() {
        return from(accountsRepository.findAll());
    }

    /**
     * Производит поиск пользователей в репозитории по email
     *
     * @param email строка для поиска по совпадению
     * @return список найденных пользователей с доступной для клиента информацией
     */
    @Override
    public List<AccountDto> searchByEmail(String email) {
        return from(accountsRepository.findByEmailContainingIgnoreCase(email));
    }
}
