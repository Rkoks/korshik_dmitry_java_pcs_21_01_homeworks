package ru.rkoks.services;

import ru.rkoks.dto.SignInForm;

import java.util.Optional;

public interface SignInService {

    Optional<Long> doAuthenticate(SignInForm form);
}
