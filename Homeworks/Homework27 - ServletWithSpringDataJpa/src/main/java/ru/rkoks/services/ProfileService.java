package ru.rkoks.services;

import ru.rkoks.dto.AccountDto;

public interface ProfileService {
    AccountDto getProfileData(Long id);

}
