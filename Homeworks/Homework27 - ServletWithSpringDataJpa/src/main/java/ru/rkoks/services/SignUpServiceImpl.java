package ru.rkoks.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.SignUpForm;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.Locale;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public SignUpServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Регистрируем аккаунт в репозитории по полученным из формы данным
     *
     * @param form объект формы созданной в SignUpServlet
     */
    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail().toLowerCase(Locale.ROOT))
                .password(form.getPassword())
                .build();

        accountsRepository.save(account);

    }
}
