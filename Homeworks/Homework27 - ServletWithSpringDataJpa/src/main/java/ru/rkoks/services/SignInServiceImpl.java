package ru.rkoks.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.SignInForm;
import ru.rkoks.models.Account;
import ru.rkoks.repositories.AccountsRepository;

import java.util.Optional;

@Service
public class SignInServiceImpl implements SignInService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    /**
     * Производим аутентификацию пользователя
     *
     * @param form логин и пароль, введённые пользователем
     * @return идентификатор аккаунта в случае корректно введённых данных
     */
    @Override
    public Optional<Long> doAuthenticate(SignInForm form) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(form.getEmail());

        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            if (account.getPassword().equals(form.getPassword())) {
                return Optional.of(account.getId());
            }
        }

        return Optional.empty();
    }
}
