package ru.rkoks.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rkoks.dto.FileDto;
import ru.rkoks.models.FileInfo;
import ru.rkoks.repositories.AccountsRepository;
import ru.rkoks.repositories.FileInfoRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ru.rkoks.dto.FileDto.from;

@Service
public class FilesServiceImpl implements FilesService {
    private final FileInfoRepository fileInfoRepository;
    private final AccountsRepository accountsRepository;

    @Value("${storage.path}")
    private String storagePath;

    @Autowired
    public FilesServiceImpl(FileInfoRepository fileInfoRepository, AccountsRepository accountsRepository) {
        this.fileInfoRepository = fileInfoRepository;
        this.accountsRepository = accountsRepository;
    }

    /**
     * Загружаем файл на сервер
     *
     * @param form данные получаемые от клиента при загрузке файла
     */
    @Override
    public void upload(FileDto form) {
        String fileName = form.getOriginalFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID() + extension)
                .description(form.getDescription())
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .uploader(accountsRepository.getById(form.getUploaderId()))
                .build();

        fileInfoRepository.save(fileInfo);

        if (storagePath != null) {
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFileName()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    /**
     * устанавливаем директорию хранилища файлов
     *
     * @param path путь к хранилищу в файловой системе
     */
    @Override
    public void setStoragePath(String path) {
        this.storagePath = path;
    }

    /**
     * Достаём файл из репозитория по его имени в хранилище
     *
     * @param storageFileName UUID имя файла
     * @return информацию для клиента о файле
     */
    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByStorageFileName(storageFileName);

        if (fileInfoOptional.isPresent()) {
            return from(fileInfoOptional.get());
        }
        return null;
    }

    /**
     * Скачиваем необходимый файл из хранилища в <code>OutputStream</code>
     *
     * @param file   информация о запрашиваемом для скачивания файле
     * @param output исходящий поток для скачивания файла клиентом
     */
    @Override
    public void writeFile(FileDto file, OutputStream output) {
        try {
            Files.copy(Paths.get(storagePath + file.getStorageFileName()), output);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Производим поиск информации о файлах в репозитории по запрашиваемому имени (части имени)
     * @param filename имя файла (часть имени) для поиска по репозиторию
     * @return список с информацией о найденных файлах
     */
    /**
     * Производим поиск информации о файлах пользователя в репозитории по запрашиваемому имени (части имени)
     *
     * @param uploaderId       идентификатор владельца файла
     * @param originalFileName имя файла для поиска
     * @return список файлов, загруженных пользователем и содержащих в своём названии <code>originalFileName</code>
     */
    @Override
    public List<FileDto> searchByOriginalFileName(Long uploaderId, String originalFileName) {

        return from(fileInfoRepository.
                findByUploader_IdAndOriginalFileNameContainingIgnoreCaseOrderByOriginalFileName(
                        uploaderId, originalFileName));
    }
}
