package ru.rkoks.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "file_info")
/**
 * Модель файлов
 */
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String originalFileName;
    private String storageFileName;
    private Long size;
    private String mimeType;
    private String description;

    @ManyToOne
    @JoinColumn(name = "uploader_id")
    private Account uploader;

}
