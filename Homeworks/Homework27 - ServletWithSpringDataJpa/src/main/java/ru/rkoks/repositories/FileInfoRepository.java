package ru.rkoks.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rkoks.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
    Optional<FileInfo> findByStorageFileName(String storageFileName);

    List<FileInfo> findByUploader_IdAndOriginalFileNameContainingIgnoreCaseOrderByOriginalFileName(
            Long uploaderId, String originalFileName);
}
