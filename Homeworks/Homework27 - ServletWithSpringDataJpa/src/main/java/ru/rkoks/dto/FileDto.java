package ru.rkoks.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rkoks.models.FileInfo;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Long size;
    private String storageFileName;
    private String mimeType;
    private InputStream fileStream;
    private String description;
    private String originalFileName;
    private Long uploaderId;

    public static FileDto from(FileInfo fileInfo) {
        return FileDto.builder()
                .size(fileInfo.getSize())
                .storageFileName(fileInfo.getStorageFileName())
                .mimeType(fileInfo.getMimeType())
                .description(fileInfo.getDescription())
                .originalFileName(fileInfo.getOriginalFileName())
                .uploaderId(fileInfo.getUploader().getId())
                .build();
    }

    public static List<FileDto> from(List<FileInfo> fileInfos) {
        return fileInfos.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
