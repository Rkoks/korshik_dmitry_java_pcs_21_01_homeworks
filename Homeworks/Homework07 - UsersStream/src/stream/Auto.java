package stream;

public class Auto {
    private int number;
    private String model;
    private String color;
    private int mileage;
    private long cost;

    public Auto(int number, String model, String color, int mileage, long cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.cost = cost;
    }

    public int getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public long getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "number=" + number +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", cost=" + cost +
                "}\n";
    }
}
