package stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AutosRepository {
    private final String filename;
    private List<Auto> autos;

    public AutosRepository(String filename) {
        this.filename = filename;
    }

    /**
     * хранит функцию в виде лямбды для парсинга строки в объект Auto
     */
    private final static Function<String, Auto> autoMapFunc = line -> {
        String[] parts = line.split("\\|");

        int number = Integer.parseInt(parts[0]);
        String model = parts[1];
        String color = parts[2];
        int mileage = Integer.parseInt(parts[3]);
        long cost = Long.parseLong(parts[4]);

        return new Auto(number, model, color, mileage, cost);
    };

    /**
     * загружает список автомобилей из файла
     */
    public void loadAutos() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            autos = reader.lines().map(autoMapFunc).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Auto> getAutos() {
        return autos;
    }
}
