package stream;

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        AutosRepository autosRepo = new AutosRepository("autos.txt");
        autosRepo.loadAutos();
        List<Auto> autos = autosRepo.getAutos();

        System.out.println("1) Номера чёрных автомобилей или с нулевым пробегом:");
        autos.stream()
                .filter((auto -> auto.getColor().equals("чёрный") || auto.getMileage() == 0))
                .mapToInt(Auto::getNumber)
                .forEach(System.out::println);

        System.out.println("2) Количество уникальных моделей стоимостью от 700 до 800 тысяч:");
        System.out.println(autos.stream()
                .filter(auto -> auto.getCost() >= 700000 && auto.getCost() <= 800000)
                .map(Auto::getModel)
                .distinct()
                .count());

        System.out.println("3) Цвет автомобиля с уникальной стоимостью:");
        autos.stream()
                .min(Comparator.comparingLong(Auto::getCost))
                .ifPresent(auto -> System.out.println(auto.getColor()));

        System.out.println("4) Средняя стоимость Camry:");
        autos.stream()
                .filter(auto -> auto.getModel().equals("Camry"))
                .mapToLong(Auto::getCost)
                .average()
                .ifPresent(System.out::println);

    }
}
