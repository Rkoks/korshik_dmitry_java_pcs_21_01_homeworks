import java.math.BigInteger;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class Summator {
    private final int count;
    private BigInteger result;
    private final Object monitor;
    AtomicBoolean isDone;

    public Summator(int count) {
        this.count = count;
        this.monitor = new Object();
    }


    //TODO заставить метод возвращать значение после окончания всех вычислений

    /**
     * Возвращает BigInteger сумму от 1 до <code>number</code>
     *
     * @param number сколько чисел необходимо сложить
     * @return BigInteger сумму чисел до <code>number</code>
     */
    public BigInteger sum(BigInteger number) {
        result = BigInteger.ZERO;

        BigInteger part = number.divide(BigInteger.valueOf(count));
        BigInteger mod = number.mod(BigInteger.valueOf(count));

        threadManager(part, mod);

        return result;
    }

    /**
     * Делит задачу суммирования на потоки
     *
     * @param part сколько цифр в одной части - задаче для одного потока
     * @param mod  остаток из-за некратности заданного числа и количества потоков
     */
    private void threadManager(BigInteger part, BigInteger mod) {
        isDone = new AtomicBoolean(false);

        Semaphore semaphore = new Semaphore(10);
        CyclicBarrier barrier = new CyclicBarrier(count, () -> isDone.set(true));

        for (int i = 0; i < count; i++) {
            final BigInteger from;
            if (i != 0) {
                from = BigInteger.ONE.add(mod)
                        .add(part.multiply(BigInteger.valueOf(i)));
            } else {
                from = BigInteger.ONE;
            }

            final BigInteger to = BigInteger.ZERO
                    .add(mod)
                    .add(part.multiply(BigInteger.valueOf(i + 1)));

            Runnable task = () -> {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }

                BigInteger res = partSum(from, to);

                synchronized (result) {
                    result = result.add(res);
                }

                semaphore.release();
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    throw new IllegalStateException(e);
                }
            };

            new Thread(task).start();
        }

        waitResults();
    }

    /**
     * Сумму чисел от <code>from</code> до <code>to</code>
     *
     * @param from нижняя граница чисел для суммирования
     * @param to   верхняя граница чисел для суммирования
     * @return BigInteger сумма всех чисел от <code>from</code> до <code>to</code>
     */
    private BigInteger partSum(BigInteger from, BigInteger to) {
        BigInteger result = BigInteger.ZERO;

        BigInteger current = BigInteger.ZERO.add(from);
        while (current.compareTo(to) <= 0) {
            result = result.add(current);
            current = current.add(BigInteger.ONE);
        }

        return result;
    }

    private void waitResults(){
        while (!isDone.get()) {
            synchronized (monitor) {
                try {
                    monitor.wait(10);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
