import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Summator summator = new Summator(5);

        Scanner sc = new Scanner(System.in);
        long input = sc.nextLong();

        System.out.println(summator.sum(BigInteger.valueOf(input)).toString());

    }
}
